from collections import defaultdict
from enum import Enum
from typing import Iterable, Dict, Set, Optional, TYPE_CHECKING
from logging import getLogger
from pathlib import Path

from openpyxl import load_workbook
from openpyxl.cell.cell import Cell
from xlsxwriter import Workbook

from ..document import Document
from ..entity import Entity, Regest
from ..config import CONFIG as _CONFIG
from .result import SearchResultList

if TYPE_CHECKING:  # avoid circular dependency
    from ..index import ResultHead


LOGGER = getLogger(__name__)


class Correctness(Enum):
    FULL = 2
    PARTIAL = 1
    NONE = 0

    @classmethod
    def from_symbol(cls, symbol: str) -> 'Correctness':
        for value in cls:
            if str(value) == symbol:
                return value
        raise ValueError('Unexpected symbol: {}'.format(symbol))

    def __int__(self) -> int:
        return self.value

    def __str__(self) -> str:
        return str(int(self))

    @classmethod
    def list_symbols(cls) -> str:
        return f'{min(map(int, cls))}–{max(map(int, cls))}'


class Completeness(Enum):
    TOO_SMALL_WINDOW = '<'
    TOO_LARGE_WINDOW = '>'
    PARTIAL_OVERLAP = 'N'
    EXACT_OVERLAP = 'A'

    @classmethod
    def from_symbol(cls, symbol: Optional[str]) -> Optional['Completeness']:
        if symbol is None:
            return None
        for value in cls:
            if str(value) == symbol:
                return value
        raise ValueError('Unexpected symbol: {}'.format(symbol))

    def __str__(self) -> str:
        return self.value

    @classmethod
    def list_symbols(cls) -> str:
        return ', '.join(str(value) for value in cls)


class Annotation:
    def __init__(self, correctness: Correctness, completeness: Optional[Completeness],
                 note: Optional[str]):
        if completeness is None and correctness != Correctness.NONE:
            completeness = Completeness.EXACT_OVERLAP
        self.correctness = correctness
        self.completeness = completeness
        self.note = note


def _get_cell_value(cell: Cell) -> Optional[str]:
    if cell.value is None or not str(cell.value).strip():
        return None
    if isinstance(cell.value, float):
        return str(int(cell.value))
    return str(cell.value)


class Annotations:
    CONFIG = _CONFIG['search.Annotations']
    ROOT_PATH = Path(CONFIG['root_path'])
    NUMBER_OF_REGESTS = CONFIG.getint('number_of_regests')
    TOPN = CONFIG.getint('number_of_results')

    def __init__(self, filename: str, entities: Iterable[Entity],
                 documents: Dict[str, Document]) -> None:
        self.annotations: Dict[Entity, Dict['ResultHead', Annotation]] = defaultdict(lambda: dict())

        workbook = load_workbook(self.ROOT_PATH / filename)
        for entity in entities:
            worksheet = workbook[str(entity)[:31]]
            rows = iter(worksheet.iter_rows())
            for row in rows:
                cell = row[0]
                if _get_cell_value(cell) == 'Dokument':
                    break
            for row in rows:
                cell_values = map(_get_cell_value, row)
                document_name, left_context, match, right_context, correctness_symbol, completeness_symbol, note, *_ = cell_values
                if document_name is None:
                    break  # We have reached the final row
                document = documents[document_name]
                snippet = (left_context, match, right_context)
                snippet = tuple(map(lambda cell_value: '' if cell_value is None else cell_value, snippet))
                position = document.find_snippet(snippet)
                if position is None:
                    raise ValueError(f'Snippet {snippet} not found in document {document}')
                result_head = (document, position)
                assert correctness_symbol is not None
                correctness = Correctness.from_symbol(correctness_symbol)
                completeness = Completeness.from_symbol(completeness_symbol)
                annotation = Annotation(correctness, completeness, note)
                self.annotations[entity][result_head] = annotation

    @classmethod
    def create_annotation_template(cls, filename: str, entities: Iterable[Entity],
                                   entities_to_regests: Dict[Entity, Set[Regest]],
                                   result_lists: Iterable[SearchResultList],
                                   annotations: Optional['Annotations'] = None) -> None:
        with Workbook(cls.ROOT_PATH / filename) as workbook:

            # Declare formats
            default_format = workbook.add_format()

            bold_format = workbook.add_format({'bold': True})

            nonrelevant_format = workbook.add_format({'bg_color': '#f4cccc'})
            partially_relevant_format = workbook.add_format({'bg_color': '#fff2cc'})
            relevant_format = workbook.add_format({'bg_color': '#d9ead3'})

            inadequate_subset_format = workbook.add_format({'bg_color': '#f4cccc'})
            inadequate_superset_format = workbook.add_format({'bg_color': '#f4cccc'})
            inadequate_overlap_format = workbook.add_format({'bg_color': '#f4cccc'})
            adequate_format = workbook.add_format({'bg_color': '#d9ead3'})

            left_alignment_format = workbook.add_format({'align': 'left'})
            bold_and_centering_format = workbook.add_format({'align': 'center', 'bold': True})
            right_alignment_format = workbook.add_format({'align': 'right'})

            # Create worksheets for individual entities
            for entity in entities:
                worksheet = workbook.add_worksheet(str(entity)[:31])

                # Set column width
                worksheet.set_column('A:A', 10)
                worksheet.set_column('B:B', 50)
                worksheet.set_column('C:C', 25)
                worksheet.set_column('D:D', 50)
                worksheet.set_column('E:E', 15)
                worksheet.set_column('F:F', 20)
                worksheet.set_column('G:G', 50)

                # Add entity and hyperlinks to regests
                worksheet.write('A1', 'Entita:', bold_format)
                worksheet.write('B1', str(entity))
                worksheet.write('A2', 'Regesty:', bold_format)
                worksheet.write('C1', 'Tento list si zamlouvá:', bold_format)
                worksheet.write_comment('D1', 'Uveďte své jméno, aby ostatní '
                                        'věděli, že tento list budete anotovat vy.')

                regests = sorted(entities_to_regests[entity])
                for row_number, regest in enumerate(regests):
                    row_number += 1
                    if row_number >= cls.NUMBER_OF_REGESTS:
                        worksheet.write(row_number, 1, '...')
                        break
                    worksheet.write_url(row_number, 1, regest.document_url, string=str(regest))

                first_context_row = 3 + row_number

                # Add headers
                worksheet.write(first_context_row - 1, 0, 'Dokument', bold_format)
                worksheet.write(first_context_row - 1, 1, 'Levý kontext', bold_format)
                worksheet.write(first_context_row - 1, 2, 'Výskyt', bold_format)
                worksheet.write(first_context_row - 1, 3, 'Pravý kontext', bold_format)
                worksheet.write(first_context_row - 1, 4, f'Správnost ({Correctness.list_symbols()})', bold_format)
                worksheet.write_comment(first_context_row - 1, 4, f'Na škále {Correctness.list_symbols()} uveďte, '
                                        f'jak správný je nalezený výskyt entity.\n\n{Correctness.NONE}: vůbec se '
                                        'nejedná o jmennou entitu, nebo se jedná o jmennou '
                                        'entitu jiného typu (např. Ota Veliký a Praha).\n\n'
                                        f'{Correctness.PARTIAL}: jedná se o jinou jmennou entitu stejného typu (např. '
                                        f'Ota Veliký a Petr Vok).\n\n{Correctness.FULL}: Jedná se o stejnou jmennou '
                                        'entitu při zanedbání jazyka a slovního tvaru (např. Ota '
                                        'Veliký a Otu Velikého nebo Otto I. der Große).')
                worksheet.write(first_context_row - 1, 5, f'Úplnost ({Completeness.list_symbols()})', bold_format)
                worksheet.write_comment(first_context_row - 1, 5, 'Pokud je správnost výskytu 1 '
                                        'nebo 2, uveďte, jak úplný je výskyt jmenné entity.\n\n'
                                        f'{Completeness.TOO_SMALL_WINDOW}: Část jmenné entity se nachází v levém '
                                        f'nebo pravém kontextu mimo výskyt.\n\n{Completeness.TOO_LARGE_WINDOW}: '
                                        'Výskyt zahrnuje kromě celé jmenné entity i její okolí.\n\n'
                                        f'{Completeness.PARTIAL_OVERLAP}: Ve výskytu chybí části '
                                        'jmenné entity a naopak přebývá její okolí.\n\n'
                                        f'{Completeness.EXACT_OVERLAP}: Výskyt zahrnuje právě celou jmennou entitu.')
                worksheet.write(first_context_row - 1, 6, 'Poznámky', bold_format)
                worksheet.write_comment(first_context_row - 1, 6, 'Pokud si nejste svou anotací '
                                        'jisti, popiště, co vám činilo potíže. Používejte prosím '
                                        'celé věty a jděte do takové úrovně detailu, aby bylo možné '
                                        'sporné anotace předat bez dalších vysvětlivek historikům.')

                # List results
                result_heads = set([] if annotations is None else annotations.annotations[entity].keys())
                for result_list in list(result_lists):
                    result_heads |= {
                        result_head
                        for result_head, similarity
                        in result_list[entity][:cls.TOPN]
                    }
                result_heads = sorted(result_heads)

                for row_number, result_head in enumerate(result_heads):
                    document, position = result_head
                    row_number += first_context_row
                    snippet_url = document.get_snippet_url(position)
                    left_context, match, right_context = document.get_snippet(position)

                    worksheet.write_url(row_number, 0, snippet_url, string=repr(document))
                    worksheet.write(row_number, 1, left_context, right_alignment_format)
                    worksheet.write(row_number, 2, match, bold_and_centering_format)
                    worksheet.write(row_number, 3, right_context, left_alignment_format)

                    try:
                        annotation = annotations.annotations[entity][result_head]  # pytype: disable=attribute-error
                        worksheet.write(row_number, 4, int(annotation.correctness))
                        if annotation.completeness is not None:
                            worksheet.write(row_number, 5, str(annotation.completeness))
                        if annotation.note is not None:
                            worksheet.write(row_number, 6, annotation.note)
                    except (AttributeError, KeyError):
                        pass

                # Set conditional formatting for correctness
                blank_conditional_format = {'type': 'blanks', 'stop_if_true': True, 'format': default_format}
                nonrelevant_conditional_format = {'type': 'cell', 'criteria': '==',
                                                  'value': int(Correctness.NONE),
                                                  'format': nonrelevant_format}
                partially_relevant_conditional_format = {'type': 'cell', 'criteria': '==',
                                                         'value': int(Correctness.PARTIAL),
                                                         'format': partially_relevant_format}
                relevant_conditional_format = {'type': 'cell', 'criteria': '==',
                                               'value': int(Correctness.FULL),
                                               'format': relevant_format}

                relevance_format_range = [first_context_row, 4, first_context_row + len(result_heads) - 1, 4]

                worksheet.conditional_format(*relevance_format_range, blank_conditional_format)
                worksheet.conditional_format(*relevance_format_range, nonrelevant_conditional_format)
                worksheet.conditional_format(*relevance_format_range, partially_relevant_conditional_format)
                worksheet.conditional_format(*relevance_format_range, relevant_conditional_format)

                # Set conditional formatting for completeness
                inadequate_subset_conditional_format = {'type': 'cell', 'criteria': '==',
                                                        'value': f'"{Completeness.TOO_SMALL_WINDOW}"',
                                                        'format': inadequate_subset_format}
                inadequate_superset_conditional_format = {'type': 'cell', 'criteria': '==',
                                                          'value': f'"{Completeness.TOO_LARGE_WINDOW}"',
                                                          'format': inadequate_superset_format}
                inadequate_overlap_conditional_format = {'type': 'cell', 'criteria': '==',
                                                         'value': f'"{Completeness.PARTIAL_OVERLAP}"',
                                                         'format': inadequate_overlap_format}
                adequate_conditional_format = {'type': 'cell', 'criteria': '==',
                                               'value': f'"{Completeness.EXACT_OVERLAP}"',
                                               'format': adequate_format}

                adequacy_format_range = [first_context_row, 5, first_context_row + len(result_heads) - 1, 5]

                worksheet.conditional_format(*adequacy_format_range, blank_conditional_format)
                worksheet.conditional_format(*adequacy_format_range, inadequate_subset_conditional_format)
                worksheet.conditional_format(*adequacy_format_range, inadequate_superset_conditional_format)
                worksheet.conditional_format(*adequacy_format_range, inadequate_overlap_conditional_format)
                worksheet.conditional_format(*adequacy_format_range, adequate_conditional_format)
