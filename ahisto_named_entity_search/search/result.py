from __future__ import annotations

from datetime import timedelta
from collections import defaultdict
from itertools import islice, chain, combinations
import json
from logging import getLogger
from statistics import mean
from typing import Dict, List, Tuple, Optional, Iterable, Iterator, Union, TYPE_CHECKING
from pathlib import Path

from docx import Document as DocxDocument
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_COLOR_INDEX, WD_UNDERLINE
from docx.opc.constants import RELATIONSHIP_TYPE
from docx.oxml.ns import qn
from docx.oxml.shared import OxmlElement
from docx.shared import RGBColor
from docx.text.paragraph import Paragraph
from docx.text.run import Run
from IPython.display import display, Markdown
from markdown import markdown as to_html
from markdown_strings import esc_format as escape, link, header, bold, italics
from more_itertools import zip_equal
from humanize import naturaldelta
import regex

from ..document import Document, Documents, Sentence
from ..entity import Entity, NerTag
from ..config import CONFIG as _CONFIG

if TYPE_CHECKING:  # avoid circular dependency
    from .evaluation import NerEvaluationResult, NerConfusionMatrix
    from ..index import Results, Result, Index
    from ..recognition import NerModel
    from ..document import WindowPosition


LOGGER = getLogger(__name__)


Duration = timedelta
IndexName = str

SerializedResults = List[List]
EntityRepr = str

AllResults = Tuple[Dict[Entity, 'Results'], Duration, IndexName]
AllSerializedResults = List

Word = str

BioNerTag = str
BioNerTags = str

TaggedSentenceFormat = str


def strip_suffix(bio_ner_tag: BioNerTag) -> str:
    if bio_ner_tag == 'O':
        prefix = ''
    else:
        index = bio_ner_tag.index('-') + 1
        prefix = bio_ner_tag[:index]
    return prefix


def strip_prefix(bio_ner_tag: BioNerTag) -> NerTag:
    if bio_ner_tag == 'O':
        ner_tag = bio_ner_tag
    else:
        prefix = strip_suffix(bio_ner_tag)
        ner_tag = bio_ner_tag[len(prefix):]
    return ner_tag


def split_to_prefix_and_suffix(bio_ner_tag: BioNerTag) -> Tuple[str, NerTag]:
    prefix = strip_suffix(bio_ner_tag)
    suffix = strip_prefix(bio_ner_tag)
    return prefix, suffix


class TaggedSentence:
    CONFIG = _CONFIG['search.TaggedSentence']
    ROOT_PATH = Path(CONFIG['root_path'])
    REGEST_SCHEMA_URL = CONFIG['regest_schema_url']

    def __init__(self, sentence: Sentence, ner_tags: BioNerTags,
                 could_be_both_maybe_iterable: Optional[Iterable[bool]] = None):
        sentence = regex.sub(r'\n+', ' ', sentence)
        assert '\n' not in sentence
        assert '\n' not in ner_tags

        num_tokens = len(sentence.split())
        if could_be_both_maybe_iterable is None:
            could_be_both_iterable = [False] * num_tokens
        else:
            could_be_both_iterable = could_be_both_maybe_iterable

        # Prevent I-X tag right after *-Y or O tag if X != Y
        ner_tags_list = []
        previous_ner_tag: Optional[BioNerTag] = None
        for ner_tag in ner_tags.split():
            if strip_suffix(ner_tag) == 'I-' \
                    and (previous_ner_tag is None
                         or strip_prefix(ner_tag) != strip_prefix(previous_ner_tag)):
                ner_tag = f'B-{strip_prefix(ner_tag)}'
            ner_tags_list.append(ner_tag)
            previous_ner_tag = ner_tag
        ner_tags = ' '.join(ner_tags_list)

        # Merge punctuation with previous words
        sentence_list, ner_tags_list, could_be_both_list = [], [], []
        previous_ner_tag: Optional[BioNerTag] = None
        previous_could_be_both: bool = False
        num_folded_ner_tags_list = []
        for word, ner_tag, could_be_both in zip_equal(sentence.split(), ner_tags.split(),
                                                      could_be_both_iterable):
            assert not (ner_tag == 'O' and could_be_both)
            if regex.fullmatch(r'\p{Punct}+', word) and previous_ner_tag is not None:
                sentence_list[-1] += word
                num_folded_ner_tags_list[-1] += 1
                if ner_tag != 'O' and previous_ner_tag == 'O':
                    ner_tags_list[-1] = ner_tag
                    previous_ner_tag = ner_tag
                if could_be_both and not previous_could_be_both:
                    could_be_both_list[-1] = could_be_both
                    previous_could_be_both = could_be_both
            else:
                num_folded_ner_tags_list.append(1)
                sentence_list.append(word)
                ner_tags_list.append(ner_tag)
                could_be_both_list.append(could_be_both)
                previous_ner_tag = ner_tag
                previous_could_be_both = could_be_both

        assert len(sentence_list) == len(ner_tags_list)
        assert len(could_be_both_list) == len(sentence_list)
        assert len(num_folded_ner_tags_list) == len(sentence_list)

        sentence = ' '.join(sentence_list)
        ner_tags = ' '.join(ner_tags_list)

        self.sentence_tuple = tuple(sentence.split())
        self.ner_tags_tuple = tuple(ner_tags.split())
        self.could_be_both = tuple(could_be_both_list)
        self.num_folded_ner_tags_list = tuple(num_folded_ner_tags_list)

    @property
    def possible_hypotheses(self) -> Iterable[TaggedSentence]:

        def extract_could_be_both_windows() -> List['WindowPosition']:
            could_be_both_windows: List['WindowPosition'] = []
            start_position = None
            current_suffix = None
            for position, (could_be_both, ner_tag) in enumerate(zip_equal(self.could_be_both, self.ner_tags_tuple)):
                if start_position is None and could_be_both:
                    start_position = position
                    current_suffix = strip_prefix(ner_tag)
                if start_position is not None:
                    if not could_be_both:
                        window = (start_position, position - 1)
                        could_be_both_windows.append(window)
                        start_position = None
                        current_suffix = None
                    elif current_suffix != strip_prefix(ner_tag):
                        window = (start_position, position - 1)
                        could_be_both_windows.append(window)
                        start_position = position
                        current_suffix = strip_prefix(ner_tag)
            if start_position is not None:
                window = (start_position, position)
                could_be_both_windows.append(window)
            return could_be_both_windows

        def all_combinations(array):
            return chain(*(list(combinations(array, i + 1)) for i in range(len(array))))

        yield TaggedSentence(self.sentence, self.ner_tags)

        for could_be_both_windows in all_combinations(extract_could_be_both_windows()):
            ner_tags = list(self.ner_tags_tuple)
            for window in could_be_both_windows:
                current_suffix = None
                for position in range(window[0], window[1] + 1):
                    ner_tag = ner_tags[position]
                    prefix, suffix = split_to_prefix_and_suffix(ner_tag)
                    assert suffix != 'O'
                    if position > window[0]:
                        assert current_suffix == suffix

                    # Flip the NER tag
                    if suffix == 'PER':
                        updated_suffix = 'LOC'
                    elif suffix == 'LOC':
                        updated_suffix = 'PER'
                    else:
                        raise ValueError(f'Unknown NER tag: {ner_tag}')

                    if position == window[0]:
                        updated_prefix = 'B-'
                    else:
                        updated_prefix = 'I-'

                    ner_tags[position] = f'{updated_prefix}{updated_suffix}'

                    if position == window[0]:
                        current_suffix = suffix

            tagged_sentence = TaggedSentence(self.sentence, ' '.join(ner_tags))
            yield tagged_sentence

    @property
    def sentence(self) -> Sentence:
        sentence = ' '.join(self.sentence_tuple)
        return sentence

    @property
    def ner_tags(self) -> BioNerTags:
        ner_tags = ' '.join(self.ner_tags_tuple)
        return ner_tags

    @property
    def unfolded_ner_tags_tuple(self) -> Tuple[BioNerTag]:
        ner_tags_list = []
        for num_ner_tags, ner_tag in zip_equal(self.num_folded_ner_tags_list, self.ner_tags_tuple):
            prefix, suffix = split_to_prefix_and_suffix(ner_tag)
            if prefix == 'B-':
                ner_tags = [ner_tag] + (num_ner_tags - 1) * [f'I-{suffix}']
            else:
                ner_tags = num_ner_tags * [ner_tag]
            ner_tags_list.extend(ner_tags)
        return tuple(ner_tags_list)

    @property
    def unfolded_ner_tags(self) -> BioNerTags:
        ner_tags = ' '.join(self.unfolded_ner_tags_tuple)
        return ner_tags

    @property
    def density(self) -> float:
        density = sum(1 if ner_tag != 'O' else 0 for ner_tag in self.ner_tags_tuple) / len(self)
        return density

    @classmethod
    def save(cls, basename: str, tagged_sentences: Iterable['TaggedSentence'],
             also_save_docx: bool = False) -> None:
        cls._save_txt(basename, tagged_sentences)
        if also_save_docx:
            cls._save_docx(basename, tagged_sentences)

    @classmethod
    def load(cls, basename: str) -> Iterable['TaggedSentence']:
        try:
            for tagged_sentence in cls._load_docx(basename):
                yield tagged_sentence
        except FileNotFoundError:
            for tagged_sentence in cls._load_txt(basename):
                yield tagged_sentence

    @classmethod
    def _save_txt(cls, basename: str, tagged_sentences: Iterable['TaggedSentence']) -> None:
        sentences_filename = cls._get_txt_sentences_filename(basename)
        ner_tags_filename = cls._get_txt_ner_tags_filename(basename)
        with sentences_filename.open('wt', encoding='utf8') as sf, \
                ner_tags_filename.open('wt', encoding='utf8') as ntf:
            for tagged_sentence in tagged_sentences:
                print(tagged_sentence.sentence, file=sf)
                print(tagged_sentence.ner_tags, file=ntf)
        LOGGER.debug(f'Saved {sentences_filename} and {ner_tags_filename}')

    @classmethod
    def _save_docx(cls, basename: str, tagged_sentences: Iterable['TaggedSentence']) -> None:
        document = DocxDocument()

        # Define character styles for different NER tags
        per_style = document.styles.add_style('PER', WD_STYLE_TYPE.CHARACTER)
        loc_style = document.styles.add_style('LOC', WD_STYLE_TYPE.CHARACTER)
        loc_in_per_style = document.styles.add_style('LOC in PER', WD_STYLE_TYPE.CHARACTER)
        per_in_loc_style = document.styles.add_style('PER in LOC', WD_STYLE_TYPE.CHARACTER)
        hyperlink_style = document.styles.add_style('Hyperlink', WD_STYLE_TYPE.CHARACTER)

        per_style.font.highlight_color = WD_COLOR_INDEX.YELLOW
        loc_style.font.highlight_color = WD_COLOR_INDEX.TURQUOISE
        loc_in_per_style.font.highlight_color = WD_COLOR_INDEX.YELLOW
        loc_in_per_style.font.underline = WD_UNDERLINE.SINGLE
        per_in_loc_style.font.highlight_color = WD_COLOR_INDEX.TURQUOISE
        per_in_loc_style.font.underline = WD_UNDERLINE.SINGLE
        hyperlink_style.font.color.rgb = RGBColor(17, 85, 204)

        # Add instructions for assessors
        document.add_paragraph('Instrukce pro anotátory:')

        list_item = document.add_paragraph(style='List Bullet')  # Instruction 1
        list_item.add_run('Zvýrazněte chybející jmenné entity.\n').bold = True
        list_item.add_run('Např.: „')
        list_item.add_run('Sigmunds').style = per_style
        list_item.add_run(' Verhandl. mit ')
        list_item.add_run('Rokycana').style = per_style
        list_item.add_run('“ místo „')
        list_item.add_run('Sigmunds').style = per_style
        list_item.add_run(' Verhandl. mit Rokycana“')

        list_item = document.add_paragraph(style='List Bullet')  # Instruction 2
        list_item.add_run('Odstraňte zvýraznění u slov, která nejsou entity.\n').bold = True
        list_item.add_run('Např.: „vor eyn halb schock strow“ místo „vor eyn halb schock ')
        list_item.add_run('strow').style = loc_style
        list_item.add_run('“')

        list_item = document.add_paragraph(style='List Bullet')  # Instruction 3
        list_item.add_run('Opravte zvýraznění ').bold = True
        list_item_run = list_item.add_run('místních názvů')
        list_item_run.bold = True
        list_item_run.style = loc_style
        list_item.add_run(' označených jako ').bold = True
        list_item_run = list_item.add_run('osoby')
        list_item_run.bold = True
        list_item_run.style = per_style
        list_item.add_run(' a obráceně.\n').bold = True
        list_item.add_run('Např.: „')
        list_item.add_run('Damnov,').style = loc_style
        list_item.add_run(' ')
        list_item.add_run('Pavlovice,').style = loc_style
        list_item.add_run(' ')
        list_item.add_run('Velikou ves').style = loc_style
        list_item.add_run('“ místo „')
        list_item.add_run('Damnov,').style = loc_style
        list_item.add_run(' ')
        list_item.add_run('Pavlovice,').style = per_style
        list_item.add_run(' ')
        list_item.add_run('Velikou ves').style = loc_style
        list_item.add_run('“')

        list_item = document.add_paragraph(style='List Bullet')  # Instruction 4
        list_item.add_run('Pokud dvě jmenné entity sousedí, odstraňte zvýraznění z mezery mezi entitami.\n').bold = True
        list_item.add_run('Např.: „')
        list_item.add_run('Sigmund').style = per_style
        list_item.add_run(' ')
        list_item.add_run('Oldřichovi z Rožmberka').style = per_style
        list_item.add_run('“ místo „')
        list_item.add_run('Sigmund Oldřichovi z Rožmberka').style = per_style
        list_item.add_run('“')

        def add_hyperlink(paragraph: Paragraph, text: str, url: str,
                          bold: bool = True, color: Optional[str] = '1155cc'):
            part = paragraph.part
            r_id = part.relate_to(url, RELATIONSHIP_TYPE.HYPERLINK, is_external=True)
            hyperlink = OxmlElement('w:hyperlink')
            hyperlink.set(qn('r:id'), r_id, )

            new_run = OxmlElement('w:r')
            rPr = OxmlElement('w:rPr')

            if color is not None:
                c = OxmlElement('w:color')
                c.set(qn('w:val'), color)
                rPr.append(c)

            if bold:
                b = OxmlElement('w:b')
                b.set(qn('w:val'), 'True')
                rPr.append(b)

            new_run.append(rPr)
            new_run.text = text
            hyperlink.append(new_run)
            paragraph._p.append(hyperlink)

        list_item = document.add_paragraph(style='List Bullet')  # Instruction 5
        list_item.add_run('Opravte zvýraznění ').bold = True
        list_item_run = list_item.add_run('jmen osob')
        list_item_run.bold = True
        list_item_run.style = per_style
        list_item.add_run(' tak, aby zahrnovala ').bold = True
        list_item_run = list_item.add_run('místní tituly')
        list_item_run.bold = True
        list_item_run.style = loc_in_per_style
        list_item.add_run('.\n').bold = True
        list_item.add_run('Např.: „')
        list_item.add_run('Blažek z ').style = per_style
        list_item.add_run('Kralup').style = loc_in_per_style
        list_item.add_run('“ místo „')
        list_item.add_run('Blažek z Kralup').style = per_style
        list_item.add_run('“ nebo „')
        list_item.add_run('Blažek').style = per_style
        list_item.add_run(' z ')
        list_item.add_run('Kralup').style = loc_style
        list_item.add_run('“\n')
        list_item.add_run('Toto odpovídá ').bold = True
        add_hyperlink(list_item, 'instrukcím pro autory regestů', cls.REGEST_SCHEMA_URL)
        list_item.add_run(' (sekce ').bold = True
        list_item_run = list_item.add_run('Vlastní text regestu')
        list_item_run.bold = True
        list_item_run.italic = True
        list_item.add_run(').').bold = True

        list_item = document.add_paragraph(style='List Bullet')  # Instruction 6
        list_item.add_run('Opravte zvýraznění ').bold = True
        list_item_run = list_item.add_run('místních názvů')
        list_item_run.bold = True
        list_item_run.style = loc_style
        list_item.add_run(' tak, aby zahrnovaly ').bold = True
        list_item_run = list_item.add_run('jména osob')
        list_item_run.bold = True
        list_item_run.style = per_in_loc_style
        list_item.add_run('.\n').bold = True
        list_item.add_run('Např.: „')
        list_item.add_run('Kostel ').style = loc_style
        list_item.add_run('sv. Martina').style = per_in_loc_style
        list_item.add_run('“ místo „')
        list_item.add_run('Kostel sv. Martina').style = loc_style
        list_item.add_run('“ nebo „')
        list_item.add_run('Kostel').style = loc_style
        list_item.add_run(' ')
        list_item.add_run('sv. Martina').style = per_style
        list_item.add_run('“')

        document.add_paragraph('Doplňující informace pro anotátory:')

        list_item = document.add_paragraph(style='List Bullet')  # Instruction 7
        list_item.add_run('Není důležité, jestli zvýrazníte interpunkci okolo jmenných entit.\n').bold = True
        list_item.add_run('Např.: „')
        list_item.add_run('Jindřichovi z ').style = per_style
        list_item.add_run('Drahova').style = loc_in_per_style
        list_item.add_run('.–“ je totéž jako „')
        list_item.add_run('Jindřichovi z ').style = per_style
        list_item.add_run('Drahova.–').style = loc_in_per_style
        list_item.add_run('“')

        list_item = document.add_paragraph(style='List Bullet')  # Instruction 8
        list_item.add_run('Zvýrazněte i jmenné entity, které obsahují chyby. Chyby ').bold = True
        list_item_run = list_item.add_run('neopravujte')
        list_item_run.bold = True
        list_item_run.italic = True
        list_item.add_run('.\n').bold = True
        list_item.add_run('Např.: „')
        list_item.add_run('Viléπovi').style = per_style
        list_item.add_run(' a bratru jeho ')
        list_item.add_run('Jañkov1').style = per_style
        list_item.add_run('“ místo „Viléπovi a bratru jeho Jañkov1“ (chybějící zvýraznění) a místo „')
        list_item.add_run('Vikéřovi').style = per_style
        list_item.add_run(' a bratru jeho ')
        list_item.add_run('Jankovi').style = per_style
        list_item.add_run('“ (nadbytečné opravy chyb v textu)')

        def add_horizontal_rule(paragraph: Paragraph) -> None:
            p = paragraph._p  # p is the <w:p> XML element
            pPr = p.get_or_add_pPr()
            pBdr = OxmlElement('w:pBdr')
            pPr.insert_element_before(
                pBdr, 'w:shd', 'w:tabs', 'w:suppressAutoHyphens', 'w:kinsoku',
                'w:wordWrap', 'w:overflowPunct', 'w:topLinePunct',
                'w:autoSpaceDE', 'w:autoSpaceDN', 'w:bidi', 'w:adjustRightInd',
                'w:snapToGrid', 'w:spacing', 'w:ind', 'w:contextualSpacing',
                'w:mirrorIndents', 'w:suppressOverlap', 'w:jc',
                'w:textDirection', 'w:textAlignment', 'w:textboxTightWrap',
                'w:outlineLvl', 'w:divId', 'w:cnfStyle', 'w:rPr', 'w:sectPr',
                'w:pPrChange',
            )
            bottom = OxmlElement('w:bottom')
            bottom.set(qn('w:val'), 'single')
            bottom.set(qn('w:sz'), '6')
            bottom.set(qn('w:space'), '1')
            bottom.set(qn('w:color'), 'auto')
            pBdr.append(bottom)

        horizontal_rule = document.add_paragraph()
        add_horizontal_rule(horizontal_rule)

        # Create paragraphs for tagged sentences
        for tagged_sentence in tagged_sentences:
            paragraph = document.add_paragraph(style='List Number')
            previous_ner_tag: Optional[BioNerTag] = None
            for (word, ner_tag), could_be_both in zip_equal(tagged_sentence, tagged_sentence.could_be_both):

                def color_run(run: Run, ner_tag: BioNerTag) -> None:
                    if ner_tag == 'O':
                        pass
                    elif strip_prefix(ner_tag) == 'PER':
                        if could_be_both:
                            run.style = loc_in_per_style
                        else:
                            run.style = per_style
                    elif strip_prefix(ner_tag) == 'LOC':
                        if could_be_both:
                            run.style = per_in_loc_style
                        else:
                            run.style = loc_style
                    else:
                        raise ValueError(f'Unknown tag {ner_tag}')

                if previous_ner_tag is not None:
                    space_run = paragraph.add_run(' ')
                    if previous_ner_tag != 'O' and strip_suffix(ner_tag) == 'I-' and \
                            strip_prefix(ner_tag) == strip_prefix(previous_ner_tag):
                        color_run(space_run, ner_tag)
                previous_ner_tag = ner_tag

                word_run = paragraph.add_run(word)
                color_run(word_run, ner_tag)

        # Save the created document
        document_filename = cls._get_docx_filename(basename)
        document.save(document_filename)
        LOGGER.debug(f'Saved {document_filename}')

    @classmethod
    def _load_txt(cls, basename: str) -> Iterable['TaggedSentence']:
        sentences_filename = cls._get_txt_sentences_filename(basename)
        ner_tags_filename = cls._get_txt_ner_tags_filename(basename)
        yield from cls._load_txt_(sentences_filename, ner_tags_filename)

    @classmethod
    def _load_txt_(cls, sentences_filename: Path, ner_tags_filename: Path) -> Iterable['TaggedSentence']:
        with sentences_filename.open('rt', encoding='utf8') as sf, \
                ner_tags_filename.open('rt', encoding='utf8') as ntf:
            for sentence, ner_tags in zip_equal(sf, ntf):
                sentence = sentence.rstrip('\r\n')
                ner_tags = ner_tags.rstrip('\r\n')
                tagged_sentence = TaggedSentence(sentence, ner_tags)
                yield tagged_sentence
        LOGGER.debug(f'Loaded {sentences_filename} and {ner_tags_filename}')

    @classmethod
    def _load_docx(cls, basename: str) -> Iterable['TaggedSentence']:
        document_filename = cls._get_docx_filename(basename)
        document = DocxDocument(document_filename)

        first_paragraph_index, = [
            index
            for index, paragraph
            in enumerate(document.paragraphs)
            if not paragraph.text
        ]
        first_paragraph_index += 1
        paragraphs = document.paragraphs[first_paragraph_index:]

        for paragraph in paragraphs:
            current_ner_tag = 'O'
            sentence_list = []
            ner_tags_list = []
            could_be_both_list = []
            for run in paragraph.runs:
                sentence = run.text.strip()
                num_ner_tags = len(sentence.split())
                color = run.font.highlight_color
                underline = run.font.underline
                assert color in (None, WD_COLOR_INDEX.YELLOW, WD_COLOR_INDEX.TURQUOISE)
                assert underline in (None, False, WD_UNDERLINE.SINGLE), underline
                if color is None:
                    next_ner_tag = 'O'
                    ner_tags_sublist = [next_ner_tag] * num_ner_tags
                    could_be_both_sublist = [False] * num_ner_tags
                else:
                    if num_ner_tags == 0:
                        continue
                    next_ner_tag = 'PER' if color == WD_COLOR_INDEX.YELLOW else 'LOC'
                    ner_tags_sublist = [f'I-{next_ner_tag}'] * num_ner_tags
                    if current_ner_tag != next_ner_tag:
                        ner_tags_sublist[0] = f'B-{next_ner_tag}'
                    could_be_both = True if underline == WD_UNDERLINE.SINGLE else False
                    could_be_both_sublist = [could_be_both] * num_ner_tags
                sentence_list.append(sentence)
                ner_tags_list.extend(ner_tags_sublist)
                could_be_both_list.extend(could_be_both_sublist)
                current_ner_tag = next_ner_tag
            sentence = ' '.join(sentence_list)
            ner_tags = ' '.join(ner_tags_list)
            yield TaggedSentence(sentence, ner_tags, could_be_both_list)

        LOGGER.debug(f'Loaded {document_filename}')

    @staticmethod
    def deduplicate(tagged_sentences: Iterable['TaggedSentence']) -> Iterable['TaggedSentence']:
        all_ner_tag_lists = defaultdict(lambda: list())
        for tagged_sentence in tagged_sentences:
            all_ner_tag_lists[tagged_sentence.sentence].append(tagged_sentence.ner_tags)
        for sentence, ner_tags_list in all_ner_tag_lists.items():
            combined_ner_tags = None
            for ner_tags in ner_tags_list:
                ner_tags = ner_tags.split()
                if combined_ner_tags is None:
                    combined_ner_tags = ner_tags
                else:
                    assert len(ner_tags) == len(combined_ner_tags)
                    for index, ner_tag in enumerate(ner_tags):
                        if ner_tag != 'O':
                            combined_ner_tags[index] = ner_tag
            assert combined_ner_tags is not None
            tagged_sentence = TaggedSentence(sentence, ' '.join(combined_ner_tags))
            yield tagged_sentence

    @classmethod
    def _get_docx_filename(cls, basename: str) -> Path:
        filename = (cls.ROOT_PATH / basename).with_suffix('.docx')
        return filename

    @classmethod
    def _get_txt_sentences_filename(cls, basename: str) -> Path:
        filename = (cls.ROOT_PATH / basename).with_suffix('.sentences.txt')
        return filename

    @classmethod
    def _get_txt_ner_tags_filename(cls, basename: str) -> Path:
        filename = (cls.ROOT_PATH / basename).with_suffix('.ner_tags.txt')
        return filename

    @staticmethod
    def evaluate(model: 'NerModel', ground_truth) -> 'NerEvaluationResult':
        ground_truth = list(ground_truth)
        sentences = [tagged_sentence.sentence for tagged_sentence in ground_truth]
        predictions = model.tag_sentences(sentences)

        from .evaluation import NerEvaluationResult

        result = NerEvaluationResult.from_tagged_sentences(ground_truth, predictions)
        return result

    @staticmethod
    def confusion_matrix(model: 'NerModel', ground_truth, *args, **kwargs) -> 'NerConfusionMatrix':
        ground_truth = list(ground_truth)
        sentences = [tagged_sentence.sentence for tagged_sentence in ground_truth]
        predictions = model.tag_sentences(sentences)

        from .evaluation import NerConfusionMatrix

        result = NerConfusionMatrix.from_tagged_sentences(ground_truth, predictions, *args, **kwargs)
        return result

    def __str__(self) -> str:
        return self.sentence

    def __repr__(self) -> str:
        return str(self)

    def _ipython_display_(self) -> None:
        display(self.markdown)

    @property
    def markdown(self) -> Markdown:
        formatted_sentence_list = []
        for word, ner_tag in self:
            if ner_tag == 'O':
                formatted_word = word
            elif strip_prefix(ner_tag) == 'PER':
                formatted_word = bold(escape(word))
            elif strip_prefix(ner_tag) == 'LOC':
                formatted_word = italics(escape(word))
            else:
                raise ValueError(f'Unknown tag {ner_tag}')
            formatted_sentence_list.append(formatted_word)
        formatted_sentence = ' '.join(formatted_sentence_list)
        return Markdown(formatted_sentence)

    def __len__(self) -> int:
        return len(self.sentence_tuple)

    def __iter__(self) -> Iterator[Tuple[Word, BioNerTag]]:
        yield from zip_equal(self.sentence_tuple, self.ner_tags_tuple)

    def __radd__(self, other) -> 'TaggedSentence':
        if other == 0:
            return self
        else:
            return self.__add__(other)

    def __add__(self, other) -> 'TaggedSentence':
        if not isinstance(other, TaggedSentence):
            raise NotImplementedError

        sentence_list, ner_tags_list = [], []
        previous_ner_tag: Optional[BioNerTag] = None
        for (left_word, left_ner_tag), (right_word, right_ner_tag) in zip_equal(self, other):
            assert left_word == right_word
            word = left_word

            candidate_ner_tags = [right_ner_tag, left_ner_tag]
            for candidate_ner_tag in candidate_ner_tags:
                if candidate_ner_tag == 'O':
                    continue
                if strip_suffix(candidate_ner_tag) == 'I-' \
                        and (previous_ner_tag is None
                             or strip_prefix(candidate_ner_tag) != strip_prefix(previous_ner_tag)):
                    continue  # Prevent I-X tag right after *-Y or O tag if X != Y
                ner_tag = candidate_ner_tag
                break
            else:
                ner_tag = 'O'

            sentence_list.append(word)
            ner_tags_list.append(ner_tag)
            previous_ner_tag = ner_tag

        sentence = ' '.join(sentence_list)
        ner_tags = ' '.join(ner_tags_list)
        tagged_sentence = TaggedSentence(sentence, ner_tags)
        return tagged_sentence

    def __eq__(self, other) -> bool:
        if isinstance(other, TaggedSentence):
            return self.sentence == other.sentence
        elif isinstance(other, Sentence):
            return self.sentence == other
        raise NotImplementedError

    def __hash__(self) -> int:
        return hash(self.sentence)


def serialize(all_results: AllResults) -> AllSerializedResults:
    results, duration, index_name = all_results
    serialized_results = dict()
    for entity, result in results.items():
        entity_repr = repr(entity)
        serialized_results[entity_repr] = []
        for (document, (start, end)), similarity in result:
            test_snippet = document[start:end]
            serialized_result = [document.filename, start, end, test_snippet, similarity]
            serialized_results[entity_repr].append(serialized_result)
    serialized_duration = duration.total_seconds()
    all_serialized_results = [serialized_results, serialized_duration, index_name]
    return all_serialized_results


def deserialize(all_serialized_results: AllSerializedResults,
                entities: Optional[Iterable[Entity]]) -> AllResults:
    results = defaultdict(lambda: list())
    serialized_results, serialized_duration, index_name = all_serialized_results
    for entity_repr, serialized_result in serialized_results.items():
        entity = Entity.from_repr(entity_repr)
        for filename, start, end, test_snippet, similarity in serialized_result:
            document = Document(filename)
            assert document[start:end] == test_snippet
            result = ((document, (start, end)), similarity)
            results[entity].append(result)
        from ..index import sort_results
        results[entity] = sort_results(results[entity])
    if entities is not None:
        results = {entity: results[entity] for entity in entities}
    duration = timedelta(seconds=serialized_duration)
    all_results = (results, duration, index_name)
    return all_results


class SearchResultList:
    CONFIG = _CONFIG['search.SearchResultList']
    RESULTS_TOPN = CONFIG.getint('number_of_results')
    SENTENCES_TOPN = None if CONFIG['number_of_sentences'] == 'all sentences' else CONFIG.getint('number_of_sentences')
    ROOT_PATH = Path(CONFIG['root_path'])

    def __init__(self, results: Dict[Entity, 'Results'], duration: Duration,
                 index: Union['Index', str]):
        self.results = dict(results)
        self.duration = duration
        self.index_name = str(index)

    def __getitem__(self, entity_or_entities: Union[Iterable[Entity], Entity]) -> Union['Results', 'SearchResultList']:
        if isinstance(entity_or_entities, Entity):
            entity = entity_or_entities
            result = self.results[entity]
            return result
        else:
            entities = entity_or_entities
            results = {entity: self[entity] for entity in entities}
            duration = self.duration * len(entities) / len(self)
            sliced_result_list = SearchResultList(results, duration, self.index_name)
            return sliced_result_list

    def __len__(self) -> int:
        return len(self.results)

    def __iter__(self) -> Iterator[Entity]:
        return iter(self.results)

    def filter_and_map_entities(self, entities: Dict[Entity, Entity]) -> 'SearchResultList':
        updated_results = dict()
        for entity in self:
            if entity not in entities:
                continue
            mapped_entity = entities[entity]
            updated_results[mapped_entity] = self[entity]
        return SearchResultList(updated_results, self.duration, self.index_name)

    def get_tagged_sentences(self, only_relevant: bool, cross_page_boundaries: bool,
                             documents: Optional[Documents]) -> Iterable[TaggedSentence]:
        if only_relevant and cross_page_boundaries:
            assert documents is not None
            documents = {
                basename: document
                for basename, document
                in documents.items()
                if document.is_relevant
            }
        for entity in self:
            for result in self[entity][:self.SENTENCES_TOPN]:
                if TYPE_CHECKING:
                    assert isinstance(result, Result)
                result_head, _ = result
                document, position = result_head
                if only_relevant and not document.is_relevant:
                    continue

                snippet = document.get_sentence(position, cross_page_boundaries, documents)
                if snippet is None:
                    continue

                left_context, match, right_context = snippet
                left_context_tokens = left_context.split()
                match_tokens = match.split()
                right_context_tokens = right_context.split()

                left_context_and_match_tokens = f'{left_context}{match}'.split()
                match_and_right_context_tokens = f'{match}{right_context}'.split()
                assert len(left_context_and_match_tokens) in (
                    len(left_context_tokens) + len(match_tokens),
                    len(left_context_tokens) + len(match_tokens) - 1,
                )
                assert len(match_and_right_context_tokens) in (
                    len(match_tokens) + len(right_context_tokens),
                    len(match_tokens) + len(right_context_tokens) - 1,
                )

                is_left_context_separate = (
                    len(left_context_and_match_tokens) == len(left_context_tokens) + len(match_tokens)
                )
                is_right_context_separate = (
                    len(match_and_right_context_tokens) == len(match_tokens) + len(right_context_tokens)
                )

                def get_bio_ner_tags(ner_tag: NerTag) -> Iterable[BioNerTag]:
                    assert ner_tag != 'O'
                    head_ner_tag, tail_ner_tag = f'B-{ner_tag}', f'I-{ner_tag}'
                    yield head_ner_tag
                    while True:
                        yield tail_ner_tag

                ner_tags_list = (
                    (len(left_context_tokens) - 1) * ['O'] +
                    (['O'] if is_left_context_separate and left_context_tokens else []) +
                    list(islice(get_bio_ner_tags(entity.ner_tag), len(match_tokens))) +
                    (len(right_context_tokens) - 1) * ['O'] +
                    (['O'] if is_right_context_separate and right_context_tokens else [])
                )
                ner_tags = ' '.join(ner_tags_list)

                sentence = f'{left_context}{match}{right_context}'

                tagged_sentence = TaggedSentence(sentence, ner_tags)
                yield tagged_sentence

    @property
    def all_results(self) -> AllResults:
        return (self.results, self.duration, self.index_name)

    @classmethod
    def _get_json_filename(cls, basename: str) -> Path:
        filename = (cls.ROOT_PATH / basename).with_suffix('.json')
        return filename

    @classmethod
    def _get_html_filename(cls, basename: str) -> Path:
        filename = (cls.ROOT_PATH / basename).with_suffix('.html')
        return filename

    def save(self, basename: str) -> None:
        self._save_json(basename)
        self._save_html(basename)

    def _save_json(self, basename: str) -> None:
        filename = self.__class__._get_json_filename(basename)
        with filename.open('wt') as f:
            json.dump(serialize(self.all_results), f, indent=4, sort_keys=True)
        LOGGER.debug('Saved {}'.format(filename))

    def _save_html(self, basename: str) -> None:
        filename = self.__class__._get_html_filename(basename)
        with filename.open('wt', encoding='utf8') as f:
            print(self.html, file=f)
        LOGGER.debug('Saved {}'.format(filename))

    @classmethod
    def load(cls, basename: str, entities: Optional[Iterable[Entity]] = None) -> 'SearchResultList':
        filename = cls._get_json_filename(basename)
        with filename.open('rt') as f:
            all_results = deserialize(json.load(f), entities)
        LOGGER.debug('Loaded {}'.format(filename))
        results = SearchResultList(*all_results)
        results._save_html(basename)
        return results

    def __str__(self) -> str:
        lengths = list(map(len, self.results.values()))
        summary = 'Retrieved {} results for {} entities ({:.2f} on average, {} at minimum) in {} using {}.'
        summary = summary.format(sum(lengths), len(self), mean(lengths), min(lengths), naturaldelta(self.duration), self.index_name)
        return summary

    def __repr__(self) -> str:
        return str(self)

    def _ipython_display_(self) -> None:
        display(self.markdown)

    def __radd__(self, other) -> 'SearchResultList':
        if other == 0:
            return self
        else:
            return self.__add__(other)

    def __add__(self, other) -> 'SearchResultList':
        if not isinstance(other, SearchResultList):
            raise NotImplementedError
        assert not (self.results.keys() & other.results.keys())
        results = {**self.results, **other.results}
        duration = self.duration + other.duration
        index_name = 'Combined'
        combined_result_list = SearchResultList(results, duration, index_name)
        return combined_result_list

    @property
    def markdown(self) -> Markdown:
        blocks = [str(self)]
        for entity in self:
            result = self[entity][:self.RESULTS_TOPN]
            if TYPE_CHECKING:
                assert isinstance(result, Results)
            top_line = 'Top {} result{} for the entity {}:'
            top_line = top_line.format(len(result), 's' if len(result) > 1 else '', bold(escape(entity)))
            items = []
            for result_number, ((document, position), similarity) in enumerate(result):
                snippet = document.get_markdown_snippet(position).data
                document_name = 'Document {}'.format(escape(repr(document)))
                document_link = link(document_name, document.get_snippet_url(position))
                document_link = bold('{}:'.format(document_link))
                score = italics('(score: {:.2f})'.format(similarity))
                item = '{}. {} {} {}'.format(result_number+1, document_link, snippet, score)
                items.append(item)
            interblock_separator = header('<a href="#{0}" id="{0}">{1}</a>'.format(entity, self.index_name), 1)
            block = '{}\n\n{}\n\n{}'.format(interblock_separator, top_line, '\n'.join(items))
            blocks.append(block)
        document = '\n\n'.join(blocks)
        return Markdown(document)

    @property
    def html(self) -> str:
        title = 'Named entity search results for {}'.format(self.index_name)
        body = to_html(self.markdown.data)
        return '<!DOCTYPE html>\n<meta charset="utf-8">\n<title>{}</title>\n{}'.format(title, body)
