from abc import ABC, abstractmethod
from collections import Counter
from logging import getLogger
from typing import Callable, Iterable, TYPE_CHECKING, List, Tuple, Optional

from more_itertools import zip_equal
from sklearn.metrics import precision_score, recall_score, ConfusionMatrixDisplay

from ..config import CONFIG as _CONFIG
from ..entity import Entity
from .result import SearchResultList
from .annotation import Annotations, Annotation, Correctness, Completeness
from .result import TaggedSentence, strip_prefix, strip_suffix

if TYPE_CHECKING:  # avoid circular dependency
    from ..document import WindowPosition
    from ..index import Result
    from ..entity import NerTag
    from .result import BioNerTag


LOGGER = getLogger(__name__)


RelevanceCriterion = Callable[[Annotation], bool]

Precision = float
Recall = float
FScore = float


class EvaluationResult(ABC):
    CONFIG = _CONFIG['search.EvaluationResult']
    BETA = CONFIG.getfloat('f_score_beta')

    @property
    @abstractmethod
    def precision(self) -> Precision:
        pass

    @property
    @abstractmethod
    def recall(self) -> Recall:
        pass

    @property
    def f_score(self) -> FScore:
        beta_squared = self.BETA ** 2
        numerator = (1 + beta_squared) * self.precision * self.recall
        denominator = (beta_squared * self.precision) + self.recall
        f_score = numerator / denominator if denominator > 0.0 else 0.0
        return f_score


class NerConfusionMatrix(EvaluationResult):
    def __init__(self, y_true: Iterable['BioNerTag'], y_pred: Iterable['BioNerTag'],
                 displayed_labels: Optional[Iterable['BioNerTag']] = None):
        self.y_true = list(y_true)
        self.y_pred = list(y_pred)

        if displayed_labels is None:
            self.labels = [label for label, count in Counter(self.y_true).most_common()]
        else:
            self.labels = list(displayed_labels)

    @classmethod
    def from_tagged_sentences(
            cls, ground_truth: Iterable[TaggedSentence],
            predictions: Iterable[TaggedSentence],
            use_bio_tags: bool = True,
            displayed_labels: Optional[Iterable['BioNerTag']] = None) -> 'NerConfusionMatrix':

        def from_tagged_sentence(
                ground_truth_tagged_sentence: TaggedSentence,
                prediction_tagged_sentence: TaggedSentence) -> 'NerConfusionMatrix':

            assert len(ground_truth_tagged_sentence) == len(prediction_tagged_sentence)

            y_true = ground_truth_tagged_sentence.ner_tags_tuple
            y_pred = prediction_tagged_sentence.ner_tags_tuple

            if not use_bio_tags:
                y_true = [strip_prefix(y) for y in y_true]
                y_pred = [strip_prefix(y) for y in y_pred]

            return cls(y_true, y_pred)

        y_true, y_pred = [], []

        for ground_truth_tagged_sentence, prediction_tagged_sentence in zip_equal(
                ground_truth, predictions):

            results = []
            for ground_truth_tagged_sentence_hypothesis in ground_truth_tagged_sentence.possible_hypotheses:
                result = from_tagged_sentence(ground_truth_tagged_sentence_hypothesis,
                                              prediction_tagged_sentence)
                results.append(result)

            best_result = max(results, key=lambda result: result.f_score)

            y_true.extend(best_result.y_true)
            y_pred.extend(best_result.y_pred)

        result = cls(y_true, y_pred, displayed_labels=displayed_labels)
        return result

    @property
    def precision(self) -> Precision:
        return precision_score(self.y_true, self.y_pred, average='micro')

    @property
    def recall(self) -> Precision:
        return recall_score(self.y_true, self.y_pred, average='micro')

    def display(self) -> ConfusionMatrixDisplay:
        return ConfusionMatrixDisplay.from_predictions(self.y_true, self.y_pred, labels=self.labels, normalize='true')

    def _ipython_display_(self) -> None:
        self.display()


class PartialNerEvaluationResult(EvaluationResult):
    def __init__(self, true_positives: int, false_positives: int, false_negatives: int):
        self.true_positives = true_positives
        self.false_positives = false_positives
        self.false_negatives = false_negatives

    @property
    def precision(self) -> Precision:
        if self.false_positives == 0:
            return 1.0
        return self.true_positives / (self.true_positives + self.false_positives)

    @property
    def recall(self) -> Recall:
        if self.false_negatives == 0:
            return 1.0
        return self.true_positives / (self.true_positives + self.false_negatives)


class NerEvaluationResult(EvaluationResult):
    CONFIG = _CONFIG['search.NerEvaluationResult']
    PREFERRED_TYPE = CONFIG['preferred_evaluation_type']

    def __init__(self,
                 strict_result_for_precision: PartialNerEvaluationResult,
                 strict_result_for_recall: PartialNerEvaluationResult,
                 fuzzy_result_for_precision: PartialNerEvaluationResult,
                 fuzzy_result_for_recall: PartialNerEvaluationResult):
        self.strict_result_for_precision = strict_result_for_precision
        self.strict_result_for_recall = strict_result_for_recall
        self.fuzzy_result_for_precision = fuzzy_result_for_precision
        self.fuzzy_result_for_recall = fuzzy_result_for_recall

    @property
    def _preferred_result_for_precision(self) -> PartialNerEvaluationResult:
        if self.PREFERRED_TYPE == 'strict':
            return self.strict_result_for_precision
        elif self.PREFERRED_TYPE == 'fuzzy':
            return self.fuzzy_result_for_precision
        else:
            raise ValueError(f'Unknown preferred evaluation type: {self.PREFERRED_TYPE}')

    @property
    def _preferred_result_for_recall(self) -> PartialNerEvaluationResult:
        if self.PREFERRED_TYPE == 'strict':
            return self.strict_result_for_recall
        elif self.PREFERRED_TYPE == 'fuzzy':
            return self.fuzzy_result_for_recall
        else:
            raise ValueError(f'Unknown preferred evaluation type: {self.PREFERRED_TYPE}')

    @property
    def precision(self) -> Precision:
        return self._preferred_result_for_precision.precision

    @property
    def recall(self) -> Recall:
        return self._preferred_result_for_recall.recall

    @classmethod
    def from_tagged_sentences(
            cls, ground_truth: Iterable[TaggedSentence],
            predictions: Iterable[TaggedSentence]) -> 'NerEvaluationResult':

        def from_tagged_sentence(
                ground_truth_tagged_sentence: TaggedSentence,
                prediction_tagged_sentence: TaggedSentence) -> Tuple[PartialNerEvaluationResult,
                                                                     PartialNerEvaluationResult]:

            assert len(ground_truth_tagged_sentence) == len(prediction_tagged_sentence)

            def extract_named_entity_positions(
                    tagged_sentence: TaggedSentence) -> List[Tuple['WindowPosition', 'NerTag']]:
                named_entity_positions: List[Tuple['WindowPosition', 'NerTag']] = []
                current_named_entity = None
                for position, ner_tag in enumerate((*tagged_sentence.ner_tags_tuple, 'O')):
                    if current_named_entity is not None and strip_suffix(ner_tag) != 'I-':
                        window_position = (current_named_entity[0], position - 1)
                        named_entity_positions.append((window_position, current_named_entity[1]))
                    if strip_suffix(ner_tag) == 'B-':
                        current_named_entity = (position, strip_prefix(ner_tag))
                    if ner_tag == 'O':
                        current_named_entity = None
                if current_named_entity is not None:
                    window_position = (current_named_entity[0], position)
                    named_entity_positions.append((window_position, current_named_entity[1]))
                return named_entity_positions

            def overlaps(first: 'WindowPosition', second: 'WindowPosition') -> bool:
                first, second = sorted([first, second])
                first_start, first_stop = first
                second_start, second_stop = second
                overlaps = first_start <= second_stop and second_start <= first_stop
                return overlaps

            strict_tp, strict_fp, strict_fn = 0, 0, 0
            fuzzy_tp, fuzzy_fp, fuzzy_fn = 0, 0, 0

            ground_truth_positions = extract_named_entity_positions(ground_truth_tagged_sentence)
            prediction_positions = extract_named_entity_positions(prediction_tagged_sentence)

            for ground_truth_position_and_type in ground_truth_positions:
                ground_truth_position, ground_truth_type = ground_truth_position_and_type
                for prediction_position_and_type in prediction_positions:
                    if ground_truth_position_and_type == prediction_position_and_type:
                        strict_tp += 1
                        break
                else:
                    strict_fn += 1
                for prediction_position_and_type in prediction_positions:
                    prediction_position, prediction_type = prediction_position_and_type
                    if ground_truth_type == prediction_type:
                        if overlaps(ground_truth_position, prediction_position):
                            fuzzy_tp += 1
                            break
                else:
                    fuzzy_fn += 1

            for prediction_position_and_type in prediction_positions:
                prediction_position, prediction_type = prediction_position_and_type
                for ground_truth_position_and_type in ground_truth_positions:
                    if ground_truth_position_and_type == prediction_position_and_type:
                        break
                else:
                    strict_fp += 1
                for ground_truth_position_and_type in ground_truth_positions:
                    ground_truth_position, ground_truth_type = ground_truth_position_and_type
                    if ground_truth_type == prediction_type:
                        if overlaps(ground_truth_position, prediction_position):
                            break
                else:
                    fuzzy_fp += 1

            strict_result = PartialNerEvaluationResult(strict_tp, strict_fp, strict_fn)
            fuzzy_result = PartialNerEvaluationResult(fuzzy_tp, fuzzy_fp, fuzzy_fn)
            return (strict_result, fuzzy_result)

        strict_tp_for_precision, strict_fp_for_precision, strict_fn_for_precision = 0, 0, 0
        strict_tp_for_recall, strict_fp_for_recall, strict_fn_for_recall = 0, 0, 0
        fuzzy_tp_for_precision, fuzzy_fp_for_precision, fuzzy_fn_for_precision = 0, 0, 0
        fuzzy_tp_for_recall, fuzzy_fp_for_recall, fuzzy_fn_for_recall = 0, 0, 0

        for ground_truth_tagged_sentence, prediction_tagged_sentence in zip_equal(
                ground_truth, predictions):

            strict_results = []
            fuzzy_results = []
            for ground_truth_tagged_sentence_hypothesis in ground_truth_tagged_sentence.possible_hypotheses:
                strict_result, fuzzy_result = from_tagged_sentence(ground_truth_tagged_sentence_hypothesis,
                                                                   prediction_tagged_sentence)
                strict_results.append(strict_result)
                fuzzy_results.append(fuzzy_result)

            best_strict_result_for_precision = max(strict_results, key=lambda result: result.precision)
            best_strict_result_for_recall = max(strict_results, key=lambda result: result.recall)
            best_fuzzy_result_for_precision = max(fuzzy_results, key=lambda result: result.precision)
            best_fuzzy_result_for_recall = max(fuzzy_results, key=lambda result: result.recall)

            strict_tp_for_precision += best_strict_result_for_precision.true_positives
            strict_fp_for_precision += best_strict_result_for_precision.false_positives
            strict_fn_for_precision += best_strict_result_for_precision.false_negatives

            strict_tp_for_recall += best_strict_result_for_recall.true_positives
            strict_fp_for_recall += best_strict_result_for_recall.false_positives
            strict_fn_for_recall += best_strict_result_for_recall.false_negatives

            fuzzy_tp_for_precision += best_fuzzy_result_for_precision.true_positives
            fuzzy_fp_for_precision += best_fuzzy_result_for_precision.false_positives
            fuzzy_fn_for_precision += best_fuzzy_result_for_precision.false_negatives

            fuzzy_tp_for_recall += best_fuzzy_result_for_recall.true_positives
            fuzzy_fp_for_recall += best_fuzzy_result_for_recall.false_positives
            fuzzy_fn_for_recall += best_fuzzy_result_for_recall.false_negatives

        strict_result_for_precision = PartialNerEvaluationResult(
            strict_tp_for_precision, strict_fp_for_precision, strict_fn_for_precision)
        strict_result_for_recall = PartialNerEvaluationResult(
            strict_tp_for_recall, strict_fp_for_recall, strict_fn_for_recall)
        fuzzy_result_for_precision = PartialNerEvaluationResult(
            fuzzy_tp_for_precision, fuzzy_fp_for_precision, fuzzy_fn_for_precision)
        fuzzy_result_for_recall = PartialNerEvaluationResult(
            fuzzy_tp_for_recall, fuzzy_fp_for_recall, fuzzy_fn_for_recall)

        result = cls(strict_result_for_precision,
                     strict_result_for_recall,
                     fuzzy_result_for_precision,
                     fuzzy_result_for_recall)

        return result

    def __str__(self) -> str:
        summary = (
            'The results achieved Precision of '
            f'{100.0 * self.strict_result_for_precision.precision:.2f}--{100.0 * self.fuzzy_result_for_precision.precision:.2f}%'
            ', Recall of '
            f'{100.0 * self.strict_result_for_recall.recall:.2f}--{100.0 * self.fuzzy_result_for_recall.recall:.2f}%.'
        )
        return summary

    def __repr__(self) -> str:
        return str(self)


def default_relevance_criterion(annotation: Annotation) -> bool:
    if annotation.correctness == Correctness.FULL and annotation.completeness == Completeness.EXACT_OVERLAP:
        return True  # perfect match
    elif annotation.correctness == Correctness.PARTIAL and annotation.completeness == Completeness.EXACT_OVERLAP:
        return True  # different entity
    elif annotation.note == 'navíc pouze interpunknce':
        return True  # punctuation around entity
    return False


class IrEvaluationResult(EvaluationResult):
    CONFIG = _CONFIG['search.IrEvaluationResult']
    TOPN = CONFIG.getint('number_of_results')

    def __init__(self, result_list: SearchResultList, annotations: Annotations,
                 entities: Iterable[Entity],
                 relevance_criterion: RelevanceCriterion = default_relevance_criterion):
        self.result_list = result_list
        self.annotations = annotations.annotations
        self.entities = list(entities)
        self.relevance_criterion = relevance_criterion

    @property
    def number_of_total_relevant_results(self) -> int:
        number_of_total_relevant_results = 0
        for entity in self.entities:
            for annotation in self.annotations[entity].values():
                if self.relevance_criterion(annotation):
                    number_of_total_relevant_results += 1
        return number_of_total_relevant_results

    @property
    def number_of_found_relevant_results(self) -> int:
        number_of_found_relevant_results = 0
        for entity in self.entities:
            results = self._get_topn_results(entity)
            for (result_head, _) in results:
                if result_head not in self.annotations[entity]:
                    continue
                annotation = self.annotations[entity][result_head]
                if self.relevance_criterion(annotation):
                    number_of_found_relevant_results += 1
        return number_of_found_relevant_results

    @property
    def number_of_found_unannotated_results(self) -> int:
        number_of_found_unannotated_results = 0
        for entity in self.entities:
            results = self._get_topn_results(entity)
            for (result_head, _) in results:
                if result_head not in self.annotations[entity]:
                    number_of_found_unannotated_results += 1
        return number_of_found_unannotated_results

    @property
    def number_of_found_results(self) -> int:
        number_of_found_results = 0
        for entity in self.entities:
            results = self._get_topn_results(entity)
            for (result_head, _) in results:
                number_of_found_results += 1
        return number_of_found_results

    def _get_topn_results(self, entity: Entity) -> Iterable['Result']:
        for result in self.result_list[entity][:self.TOPN]:
            yield result

    @property
    def precision(self) -> Precision:
        if self.number_of_found_unannotated_results > 0:
            message = '{} results have not been annotated; treating them as non-relevant'.format(
                self.number_of_found_unannotated_results)
            LOGGER.warning(message)
        if self.number_of_found_results == 0:
            if self.number_of_total_relevant_results > 0:
                return 0.0
            else:
                raise ValueError('There are no results in the result list and no known relevant results')
        precision = 1.0 * self.number_of_found_relevant_results / self.number_of_found_results
        return precision

    @property
    def recall(self) -> Recall:
        if self.number_of_found_unannotated_results > 0:
            message = '{} results have not been annotated; treating them as non-relevant'.format(
                self.number_of_found_unannotated_results)
            LOGGER.warning(message)
        if self.number_of_total_relevant_results == 0:
            return 1.0
        recall = 1.0 * self.number_of_found_relevant_results / self.number_of_total_relevant_results
        return recall

    def __str__(self) -> str:
        summary = 'The results achieved Precision@{topn} of {:.2f}%, Recall@{topn} of {:.2f}%, and F@{topn}-score of {:.2f}%.'
        summary = summary.format(100.0 * self.precision, 100.0 * self.recall, 100.0 * self.f_score, topn=self.TOPN)
        return summary

    def __repr__(self) -> str:
        return str(self)
