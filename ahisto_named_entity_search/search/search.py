from collections import defaultdict
from datetime import datetime
from logging import getLogger
from typing import Optional, Iterable, Tuple, Dict

from tqdm import tqdm

from ..config import CONFIG as _CONFIG
from ..entity import Entity
from ..index import Index, Result, ResultTail, Results, sort_results
from ..util import NestableForkingPool
from .result import SearchResultList


CONFIG = _CONFIG['search.Search']
LOGGER = getLogger(__name__)


_INDEX: Optional[Index] = None  # global variable used for sharing state between forked processes


def _search_helper(entity: Entity) -> Tuple[Entity, Results]:
    results = _INDEX.search(entity)
    return entity, results


class Search:
    TOPN = CONFIG.getint('number_of_results')
    NUM_WORKERS = None if CONFIG['number_of_workers'] == 'all cpus' else CONFIG.getint('number_of_workers')
    SIMILARITY_THRESHOLD = CONFIG.getfloat('similarity_threshold')

    def __init__(self, index: Index, num_workers: Optional[int] = None):
        self.index = index
        self.num_workers = num_workers if num_workers is not None else self.NUM_WORKERS

    def search(self, entities: Iterable[Entity]) -> SearchResultList:
        start_time = datetime.now()
        if self.num_workers is not None and self.num_workers <= 1:
            results = self._singleprocess_search(entities)
        else:
            results = self._multiprocess_search(entities)
        finish_time = datetime.now()
        duration = finish_time - start_time
        return SearchResultList(results, duration, self.index)

    def _singleprocess_search(self, entities: Iterable[Entity]) -> Dict[Entity, Results]:
        global _INDEX
        _INDEX = self.index
        entities = list(entities)
        all_results = dict()
        iterable = map(_search_helper, entities)
        iterable = tqdm(iterable, total=len(entities), desc='Searching windows ({})'.format(self.index))
        for entity, results in iterable:
            all_results[entity] = self._process_results(entity, results)
        _INDEX = None
        return all_results

    def _multiprocess_search(self, entities: Iterable[Entity]) -> Dict[Entity, Results]:
        global _INDEX
        _INDEX = self.index
        entities = list(entities)
        all_results = dict()
        with NestableForkingPool(self.num_workers) as pool:
            iterable = pool.imap(_search_helper, entities)
            iterable = tqdm(iterable, total=len(entities), desc='Searching windows ({})'.format(self.index))
            for entity, results in iterable:
                all_results[entity] = self._process_results(entity, results)
        _INDEX = None
        return all_results

    def _process_results(self, entity: Entity, results: Iterable[Result]) -> Results:
        results = self._threshold_results(results)
        results = self._aggregate_results(results)
        if len(results) < self.TOPN:
            message = 'Retrieved less than {} results ({}) for entity {}'
            message = message.format(self.TOPN, len(results), entity)
            LOGGER.warning(message)
        results = results[:self.TOPN]
        return results

    def _threshold_results(self, results: Iterable[Result]) -> Results:
        thresholded_results = []
        for result in results:
            result_head, similarity = result
            if similarity <= self.SIMILARITY_THRESHOLD:
                continue
            thresholded_results.append(result)
        return thresholded_results

    def _aggregate_results(self, results: Iterable[Result]) -> Results:
        documents = defaultdict(lambda: list())
        for (document, window_position), similarity in sort_results(results):
            result_tail = (window_position, similarity)
            documents[document].append(result_tail)
            if len(documents) >= self.TOPN:
                break

        def sort_key(result_tail: ResultTail):
            (start, end), similarity = result_tail
            window_size = end - start
            return similarity, window_size, -start, -end

        aggregate_results = []
        for document, result_tails in documents.items():
            result_tail = max(result_tails, key=sort_key)
            window_position, similarity = result_tail
            result_head = (document, window_position)
            result = (result_head, similarity)
            aggregate_results.append(result)

        return sort_results(aggregate_results)
