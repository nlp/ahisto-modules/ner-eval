import os
import sys
from typing import TextIO, List
from pathlib import Path

import click

from .config import CONFIG as _CONFIG


CONFIG = _CONFIG['cli']


@click.group()
@click.option('--model-source',
              help=('Whether the model name specifies a direct path/online name (external) '
                    'or a name of a local model trained using this library (internal)'),
              type=click.Choice(['external', 'internal']),
              default=CONFIG['model_source'],
              required=False)
@click.option('--model-name',
              help='The name of the model that will be used to infer NER tags for the input sentences',
              default=CONFIG['model_name'],
              required=False)
@click.pass_context
def cli(context: click.Context, model_source: str, model_name: str) -> None:
    # Silence cometml warnings
    if 'COMET_API_KEY' not in os.environ:
        os.environ['COMET_API_KEY'] = 'Placeholder API key'

    # Load NER model
    from .recognition import NerModel

    if model_source == 'internal':
        model = NerModel.load(model_name)
    elif model_source == 'external':
        model = NerModel(model_name)
    else:
        raise ValueError(f'Unknown model source "{model_source}", expected "internal" or "external"')

    context.ensure_object(dict)
    context.obj['model'] = model


@cli.command()
@click.argument('input-file',
                type=click.File('r', encoding='utf8'),
                default=sys.stdin,
                required=False)
@click.argument('output-file',
                type=click.File('w', encoding='utf8', lazy=False),
                default=sys.stdout,
                required=False)
@click.pass_context
def tag_sentences(context: click.Context, input_file: TextIO, output_file: TextIO) -> None:
    """For every input line with a sentence, produce an output line of NER tags for each word in the sentence."""

    # Extract lines as sentences
    sentences = (line.rstrip('\r\n') for line in input_file)

    # Tag the extracted sentences
    model = context.obj['model']
    tagged_sentences = model.tag_sentences(sentences)

    # Write NER tags for each tagged sentence on a separate line
    for tagged_sentence in tagged_sentences:
        print(tagged_sentence.unfolded_ner_tags, file=output_file)


@cli.command()
@click.argument('input-file',
                type=click.File('r', encoding='utf8'),
                default=sys.stdin,
                required=False)
@click.argument('output-file',
                type=click.File('w', encoding='utf8', lazy=False),
                default=sys.stdout,
                required=False)
@click.pass_context
def tag_texts(context: click.Context, input_file: TextIO, output_file: TextIO) -> None:
    """For every input line with a text, produce an output line of NER tags for each word in the text."""

    from .util import map_texts_as_sentences
    from .search import BioNerTag

    # Extract lines as texts
    texts = [line.rstrip('\r\n') for line in input_file]

    # Produce tags for all sentences
    model = context.obj['model']

    def rejoin_sentences(text, tagged_sentences) -> List[BioNerTag]:
        ner_tags = []
        for sentence_prefix, _, tagged_sentence in tagged_sentences:
            # With no space between sentences, remove last NER tag from the previous sentence
            if ner_tags and not sentence_prefix and tagged_sentence.unfolded_ner_tags_tuple:
                ner_tags.pop()
            prefix_ner_tags = ['O'] * len(sentence_prefix.split())
            sentence_ner_tags = tagged_sentence.unfolded_ner_tags_tuple
            ner_tags.extend(prefix_ner_tags)
            ner_tags.extend(sentence_ner_tags)
        num_words = len(text.split())
        assert num_words == len(ner_tags)
        return ner_tags

    all_ner_tags = map_texts_as_sentences(model.tag_sentences, rejoin_sentences, texts)
    all_ner_tags = list(all_ner_tags)
    assert len(all_ner_tags) == len(texts)

    # Write NER tags for each text on a separate line
    for ner_tags in all_ner_tags:
        print(' '.join(ner_tags), file=output_file)


@cli.command()
@click.argument('input-texts-file',
                type=click.Path(exists=True, file_okay=True, dir_okay=False, readable=True),
                required=True)
@click.argument('input-ner-tags-file',
                type=click.Path(exists=True, file_okay=True, dir_okay=False, readable=True),
                required=True)
@click.argument('output-file',
                type=click.File('w', encoding='utf8', lazy=False),
                default=sys.stdout,
                required=False)
def classify_texts(input_texts_file: Path, input_ner_tags_file: Path, output_file: TextIO) -> None:
    """For every input line with a text, produce an output line with a prediction whether the text comes from index."""

    from .search import TaggedSentence
    from .classification import ClassificationModel

    # Extract tagged sentences as texts
    tagged_sentences = TaggedSentence._load_txt_(Path(input_texts_file), Path(input_ner_tags_file))
    tagged_sentences = list(tagged_sentences)

    # Predict whether the texts come from index
    model = ClassificationModel()
    are_texts_from_index = model.are_texts_from_index(tagged_sentences)
    are_texts_from_index = list(are_texts_from_index)
    assert len(tagged_sentences) == len(are_texts_from_index)

    # Write prediction for each text on a separate line
    for is_text_from_index in are_texts_from_index:
        print(is_text_from_index, file=output_file)
