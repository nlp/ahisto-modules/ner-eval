from collections.abc import Sequence
import gc
import multiprocessing.pool
from typing import Set, List, Tuple, Iterable, TypeVar, Callable
from functools import lru_cache
import sys
import os

from more_itertools import zip_equal
import regex
from regex import sub
from edit_distance import SequenceMatcher
from gensim.utils import deaccent
import torch

from .config import CONFIG as _CONFIG


CONFIG = _CONFIG['util']

TOKEN = regex.compile(r'\w+')


def cache(user_function: Callable) -> Callable:
    maxsize = CONFIG.getint('cache_maxsize')
    return lru_cache(maxsize=maxsize)(user_function)


def collect_garbage_from_torch() -> None:
    gc.collect()
    torch.cuda.empty_cache()


def suppress_stderr(func):
    def wrapper(*args, **kwargs):
        stderr_tmp = sys.stderr
        null = open(os.devnull, 'w')
        sys.stderr = null
        try:
            result = func(*args, **kwargs)
            sys.stderr = stderr_tmp
            return result
        except:  # noqa:E722
            sys.stderr = stderr_tmp
            raise
    return wrapper


def lossless_strip(text: str) -> Tuple[str, str, str]:
    strip_regex = r'^(?P<left_stripped_bits>\P{L}*)(?P<body>.*?)(?P<right_stripped_bits>\P{L}*)$'
    match = regex.match(strip_regex, text)
    assert match
    left_stripped_bits = match.group('left_stripped_bits')
    body = match.group('body')
    right_stripped_bits = match.group('right_stripped_bits')
    return (left_stripped_bits, body, right_stripped_bits)


def strip(text: str) -> str:
    left_stripped_bits, body, right_stripped_bits = lossless_strip(text)
    return body


def normalize_ocr(text: str) -> str:
    text = sub(r'-\s*\n\s*', '', text)
    return ' '.join(text.split())


def tokenize(text: str) -> List[str]:
    tokens = [match.group() for match in TOKEN.finditer(text)]
    return tokens


def preprocess(text: str) -> List[str]:
    text = normalize_ocr(text)
    text = deaccent(text)
    tokens = tokenize(text)
    return tokens


def normalize(text: str) -> str:
    return ' '.join(preprocess(text))


def error_rate(first: Sequence, second: Sequence) -> float:
    max_distance = max(len(first), len(second))
    if max_distance == 0.0:
        return 1.0
    distance = SequenceMatcher(a=first, b=second).distance()
    error_rate = 1.0 * distance / max_distance
    return error_rate


Text = str
Sentence = str
SentencePrefix = str


def extract_sentences(text: Text,
                      include_prefix: bool,
                      include_suffix: bool) -> Iterable[Tuple[SentencePrefix, Sentence]]:
    prefix_regex = r'\P{L}\p{Ll}+\.\P{L}+'
    match_regex = r'((?P<sentence_head>\p{Lu}\p{Ll}*\P{L}.*?)'
    suffix_regex = r'(?=(?P<sentence_tail>\P{L}\p{Ll}+\.)\P{L}+\p{Lu}\p{Ll}*(?:\P{L}|$)))'
    sentence_regex = f'{prefix_regex}{match_regex}{suffix_regex}'

    matches = list(regex.finditer(sentence_regex, text))
    for match_number, match in enumerate(matches):
        match_number = match_number + 1

        sentence_head = match.group('sentence_head')
        sentence_tail = match.group('sentence_tail')
        sentence = f'{sentence_head}{sentence_tail}'
        sentence = regex.sub(r'\n+', ' ', sentence)

        if match_number == 1:
            if include_prefix:
                sentence_prefix = text[:match.start('sentence_head') + 1]
                sentence_prefix = regex.sub(r'\S*$', '', sentence_prefix)
                yield ('', sentence_prefix)
                yield (' ', sentence)
            else:
                yield ('', sentence)
        else:
            previous_match = matches[match_number - 2]
            previous_match_end = previous_match.end('sentence_tail')
            match_start = match.start('sentence_head')
            sentence_prefix = text[previous_match_end:match_start + 1]
            sentence_prefix = regex.sub(r'^\S*|\S*$', '', sentence_prefix)
            yield (sentence_prefix, sentence)

        if include_suffix and match_number == len(matches):
            match_end = match.end('sentence_tail')
            sentence_suffix = text[match_end:]
            sentence_suffix = regex.sub(r'^\S*', '', sentence_suffix)
            yield (regex.sub(r'^(\s*).*', r'\1', sentence_suffix),
                   regex.sub(r'^\s*(.*)', r'\1', sentence_suffix))

    if (include_prefix or include_suffix) and not matches:
        yield ('', text)


def extract_ngrams(text: str, minn: int, maxn: int) -> Set[str]:
    ngrams = set()
    maxn = min(maxn, len(text))
    for i in range(minn, maxn+1):
        for j in range(len(text)-i+1):
            ngram = text[j:j+i]
            ngrams.add(ngram)
    return ngrams


T1, T2 = TypeVar('T1'), TypeVar('T2')


def map_texts_as_sentences(
        transform_sentences: Callable[[Iterable[Sentence]], Iterable[T1]],
        rejoin_sentences: Callable[[Text, Iterable[Tuple[SentencePrefix, Sentence, T1]]], T2],
        texts: Iterable[Text]) -> Iterable[T2]:

    # Extract sentences from the raw texts
    num_sentences_in_texts, all_sentence_prefixes, all_sentences = [], [], []
    for text in texts:
        sentences = extract_sentences(text, include_prefix=True, include_suffix=True)
        sentences = list(sentences)
        for sentence_prefix, sentence in sentences:
            all_sentence_prefixes.append(sentence_prefix)
            all_sentences.append(sentence)
        num_sentences_in_text = len(sentences)
        num_sentences_in_texts.append(num_sentences_in_text)

    # Transform the extracted sentences
    all_transformed_sentences = transform_sentences(all_sentences)
    all_transformed_sentences = list(all_transformed_sentences)

    # Produce tags for all sentences
    sentence_index = 0
    for text, num_sentences_in_text in zip_equal(texts, num_sentences_in_texts):
        sentence_indexes = slice(sentence_index, sentence_index + num_sentences_in_text)
        sentence_prefixes = all_sentence_prefixes[sentence_indexes]
        sentences = all_sentences[sentence_indexes]
        transformed_sentences = all_transformed_sentences[sentence_indexes]
        sentence_index += num_sentences_in_text
        rejoined_sentences = rejoin_sentences(text, zip_equal(sentence_prefixes, sentences, transformed_sentences))
        yield rejoined_sentences


def jaccard_index(first: Set, second: Set) -> float:
    union = len(first | second)
    if union == 0:
        return 1.0
    intersection = len(first & second)
    similarity = 1.0 * intersection / (union if union > 0.0 else 1.0)
    return similarity


class NoDaemonProcess(multiprocessing.Process):
    @property
    def daemon(self):
        return False

    @daemon.setter
    def daemon(self, value):
        pass


class NoDaemonForkingContext(type(multiprocessing.get_context('fork'))):
    Process = NoDaemonProcess


class NestableForkingPool(multiprocessing.pool.Pool):
    def __init__(self, *args, **kwargs):
        kwargs['context'] = NoDaemonForkingContext()
        super().__init__(*args, **kwargs)
