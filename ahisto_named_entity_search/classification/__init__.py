from .model import (
    ClassificationModel,
)


__all__ = [
    'ClassificationModel',
]
