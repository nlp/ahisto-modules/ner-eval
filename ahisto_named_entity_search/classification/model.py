from typing import Optional, List, Iterable
from pathlib import Path
import pickle

from ..config import CONFIG as _CONFIG
from ..search import TaggedSentence


class ClassificationModel:
    CONFIG = _CONFIG['classification.ClassificationModel']
    MODEL = Path(__file__).parent / CONFIG['model']

    def __init__(self):
        with self.MODEL.open('rb') as f:
            self.model = pickle.load(f)

    def is_text_from_index(self, text: TaggedSentence) -> Optional[bool]:
        is_text_from_index, = self.are_texts_from_index([text])

    def are_texts_from_index(self, texts: Iterable[TaggedSentence]) -> Iterable[Optional[bool]]:
        texts = list(texts)
        holes, samples = set(), []
        for text_number, text in enumerate(texts):
            sample = get_text_features(text)
            if sample is None:
                holes.add(text_number)
            else:
                samples.append(sample)
        assert len(texts) == len(samples) + len(holes)
        predictions = self.model.predict(samples)
        assert len(samples) == len(predictions)
        predictions_iter = iter(predictions)
        num_predictions_read = 0
        for text_number in range(len(texts)):
            if text_number in holes:
                yield None
            else:
                num_predictions_read += 1
                prediction = next(predictions_iter)
                is_text_from_index = prediction > 0
                yield is_text_from_index
        assert num_predictions_read == len(predictions)


def get_text_features(text: TaggedSentence) -> Optional[List[float]]:
    num_all_tokens = len(text)
    if num_all_tokens == 0:
        return None
    num_entity_tokens, num_nonword_tokens, num_punctuation_tokens, num_numeric_tokens = 0, 0, 0, 0
    for word, ner_tag in text:
        if ner_tag != 'O':
            num_entity_tokens += 1
        if not str(word).isalpha():
            num_nonword_tokens += 1
        if not str(word).isalnum():
            num_punctuation_tokens += 1
        if str(word).isnumeric():
            num_numeric_tokens += 1
    num_nonentity_tokens = num_all_tokens - num_entity_tokens
    sample = [
        num_entity_tokens,
        num_nonentity_tokens,
        num_all_tokens,
        num_entity_tokens / num_all_tokens,
        num_nonword_tokens / num_all_tokens,
        num_punctuation_tokens / num_all_tokens,
        num_numeric_tokens / num_all_tokens,
    ]
    return sample
