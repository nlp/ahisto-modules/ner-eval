from itertools import repeat, chain
from logging import getLogger
from typing import Iterable, Tuple, TYPE_CHECKING

from regex import finditer

from ..config import CONFIG as _CONFIG
from ..entity import Entity

if TYPE_CHECKING:  # avoid circular dependency
    from .document import Document


CONFIG = _CONFIG['document.SlidingWindow']
LOGGER = getLogger(__name__)


WindowPosition = Tuple[int, int]


class SlidingWindow:
    MINIMUM_LENGTH = CONFIG.getint('minimum_length')
    MAXIMUM_LENGTH = CONFIG.getint('maximum_length')
    MINIMUM_MAXIMUM_LENGTH = CONFIG.getint('minimum_maximum_length')
    MAXIMUM_MINIMUM_LENGTH = CONFIG.getint('maximum_minimum_length')
    MINIMUM_LENGTH_RATIO = CONFIG.getfloat('minimum_length_ratio')
    MAXIMUM_LENGTH_RATIO = CONFIG.getfloat('maximum_length_ratio')
    MINIMUM_STEP = CONFIG.getint('minimum_step')
    STEP_MAXIMUM_LENGTH_RATIO = CONFIG.getfloat('step_maximum_length_ratio')

    def __init__(self, entity: Entity):
        self.entity = str(entity)

        self.minimum_length = int(round(self.MINIMUM_LENGTH_RATIO * len(self.entity)))
        self.minimum_length = max(self.MINIMUM_LENGTH, self.minimum_length)
        self.minimum_length = min(self.MAXIMUM_MINIMUM_LENGTH, self.minimum_length)

        self.maximum_length = int(round(self.MAXIMUM_LENGTH_RATIO * len(self.entity)))
        self.maximum_length = max(self.minimum_length, self.maximum_length, self.MINIMUM_MAXIMUM_LENGTH)
        self.maximum_length = min(self.MAXIMUM_LENGTH, self.maximum_length)

        self.minimum_step = min(self.MINIMUM_STEP, len(self.entity))
        self.minimum_step = min(self.minimum_step, int(round(self.STEP_MAXIMUM_LENGTH_RATIO * len(self.entity))))

        message = 'Entity {}: window length {}--{}, minimum step {}'
        message = message.format(self.entity, self.minimum_length, self.maximum_length, self.minimum_step)
        LOGGER.debug(message)

    def iterate(self, document: 'Document') -> Iterable[WindowPosition]:
        text = str(document)

        positions = [0] + [match.start() for match in finditer(r'\P{L}', text)] + [len(text) - 1]
        positions = sorted(set(positions))

        previous_start = None
        for start_index, (start, is_last_start) in enumerate(chain(
                    zip(positions[:-2], repeat(False)),
                    zip(positions[-2:-1], repeat(True)),
                )):
            is_first_start = previous_start is None
            start_step = self.minimum_step if is_first_start or is_last_start else start - previous_start
            if start_step >= self.minimum_step:
                previous_end = None
                for end, is_last_end in chain(
                            zip(positions[start_index + 1:-1], repeat(False)),
                            zip(positions[-1:], repeat(True)),
                        ):
                    is_first_end = previous_end is None
                    end_step = self.minimum_step if is_first_end or is_last_end else end - previous_end
                    if end_step >= self.minimum_step:
                        length = end - start + 2
                        if length < self.minimum_length or length > self.maximum_length:
                            continue
                        yield (start, end + 1)
                    previous_end = end
            previous_start = start
