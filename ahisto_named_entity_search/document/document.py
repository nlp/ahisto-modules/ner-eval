from functools import total_ordering
from collections import defaultdict
import json
from logging import getLogger
from math import floor
from pathlib import Path
from typing import Any, Callable, List, Dict, Optional, Tuple, Iterable, Set, TYPE_CHECKING
from urllib.parse import quote_plus
from random import Random

from IPython.display import display, HTML, Markdown
from markdown_strings import esc_format as escape, bold, link
import regex
from tqdm import tqdm

from ..util import normalize_ocr, cache, tokenize, strip, lossless_strip, extract_sentences
from ..config import CONFIG as _CONFIG

if TYPE_CHECKING:  # avoid circular dependency
    from .window import WindowPosition


CONFIG = _CONFIG['document.Document']
LOGGER = getLogger(__name__)


LeftContext = str
Match = str
RightContext = str
Snippet = Tuple[LeftContext, Match, RightContext]
Sentence = str

BookId = int
Page = int

Documents = Dict[str, 'Document']


@total_ordering
class Document:
    ROOT_PATH = Path(CONFIG['root_path'])
    SENTENCES_PATH = Path(CONFIG['sentences_path'])
    RELEVANT_PAGES_PATH = Path(CONFIG['relevant_pages_path'])
    RELEVANT_PAGES: Optional[Dict[BookId, Set[Page]]] = None
    SNIPPET_SIZE = CONFIG.getint('snippet_size')
    DOCUMENT_URL = CONFIG['document_url']
    IMAGE_URL = CONFIG['image_url']
    MAXIMUM_ERRORS = CONFIG.getint('maximum_errors')
    MAXIMUM_REGEX_TOKENS = CONFIG.getint('maximum_regex_tokens')

    def __init__(self, filename: str):
        self.filename = filename
        self.basename = str(Path(filename).with_suffix(''))
        self.book_id, self.page_id = map(int, self.basename.split('/'))
        self.document_url = self.DOCUMENT_URL.format(book_id=self.book_id, page_id=self.page_id)
        self.image_url = self.IMAGE_URL.format(book_id=self.book_id, page_id=self.page_id)
        self.text_filename = self.ROOT_PATH / Path(self.filename).with_suffix('.txt')

    @property
    def relevant_pages(self) -> Dict[BookId, Set[Page]]:
        if self.__class__.RELEVANT_PAGES is None:
            self.__class__.RELEVANT_PAGES = {
                int(book_id): set(pages)
                for book_id, pages
                in json.load(self.RELEVANT_PAGES_PATH.open('rt')).items()
            }
        return self.RELEVANT_PAGES

    def get_previous_page(self, documents: Documents) -> Optional['Document']:
        basename = f'{self.book_id}/{self.page_id - 1}'
        if basename not in documents:
            return None
        document = documents[basename]
        return document

    def get_next_page(self, documents: Documents) -> Optional['Document']:
        basename = f'{self.book_id}/{self.page_id + 1}'
        if basename not in documents:
            return None
        document = documents[basename]
        return document

    @staticmethod
    def get_basename(book_id: int, page_id: int) -> str:
        basename = f'{book_id}/{page_id}'
        return basename

    def get_snippet_url(self, position: 'WindowPosition') -> str:
        start, end = position
        snippet = self[start:end]
        tokens = tokenize(strip(snippet))
        value = quote_plus(' '.join(tokens))
        snippet_url = '{}&search={}'.format(self.document_url, value)
        return snippet_url

    @classmethod
    def save_sentences(cls, basename: str, *args, random_seed: int = 42, **kwargs) -> None:
        filename = cls._get_sentences_filename(basename)
        sentences = list(cls.get_sentences(*args, **kwargs))
        Random(random_seed).shuffle(sentences)
        with filename.open('wt', encoding='utf8') as f:
            for sentence in sentences:
                assert '\n' not in sentence
                print(sentence, file=f)
        LOGGER.debug(f'Saved {filename}')

    @classmethod
    def load_sentences(cls, basename: str) -> Iterable[Sentence]:
        filename = cls._get_sentences_filename(basename)
        with filename.open('rt', encoding='utf8') as f:
            for sentence in f:
                sentence = sentence.rstrip('\r\n')
                yield sentence
        LOGGER.debug(f'Loaded {filename}')

    @staticmethod
    def get_sentences(documents: Documents, only_relevant: bool,
                      cross_page_boundaries: bool) -> Iterable[Sentence]:
        books = defaultdict(lambda: list())
        for document in sorted(documents.values()):
            if only_relevant and not document.is_relevant:
                continue
            books[document.book_id].append(document)

        for book_id, pages in sorted(books.items()):
            page_texts = []
            previous_page = None

            def get_sentences_from_accumulated_texts() -> Iterable[Sentence]:
                accumulated_text = ' '.join(page_texts)
                sentences = extract_sentences(
                    accumulated_text, include_prefix=False, include_suffix=False)
                for _, sentence in sentences:
                    yield sentence

            for page in pages:
                if previous_page is None or cross_page_boundaries and previous_page.page_id == page.page_id - 1:
                    page_texts.append(page.text)
                else:
                    for sentence in get_sentences_from_accumulated_texts():
                        yield sentence
                    page_texts = [page.text]
                previous_page = page
            if page_texts:
                for sentence in get_sentences_from_accumulated_texts():
                    yield sentence

    def get_sentence(self, position: 'WindowPosition', cross_page_boundaries: bool,
                     documents: Optional[Documents]) -> Optional[Snippet]:
        if cross_page_boundaries:
            assert documents is not None

        start, end = position
        assert start >= 0 and start <= len(self)
        assert end >= 0 and end <= len(self) and end >= start

        match = self[start:end]
        left_stripped_bits, match, right_stripped_bits = lossless_strip(match)

        full_left_context = f'{self[:start]}{left_stripped_bits}'
        left_context_regex = r'.*\P{L}\p{Ll}+\.\P{L}+(?P<left_context>\p{Lu}\p{Ll}*\P{L}.*)'
        left_context_match = regex.match(left_context_regex, full_left_context)
        if left_context_match is None:
            if not cross_page_boundaries:
                return None
            previous_page = self.get_previous_page(documents)
            if previous_page is None:
                left_context = full_left_context
            else:
                full_left_context = f'{previous_page.text} {full_left_context}'
                left_context_match = regex.match(left_context_regex, full_left_context)
                if left_context_match is None:
                    return None
                left_context = left_context_match.group('left_context')
        else:
            left_context = left_context_match.group('left_context')

        full_right_context = f'{right_stripped_bits}{self[end:]}'
        right_context_regex = r'(?P<right_context>.*?\P{L}\p{Ll}+\.)\P{L}+\p{Lu}\p{Ll}*(?:\P{L}|$)'
        right_context_match = regex.match(right_context_regex, full_right_context)
        if right_context_match is None:
            if not cross_page_boundaries:
                return None
            next_page = self.get_next_page(documents)
            if next_page is None:
                right_context = full_right_context
            else:
                full_right_context = f'{full_right_context} {next_page.text}'
                right_context_match = regex.match(right_context_regex, full_right_context)
                if right_context_match is None:
                    return None
                right_context = right_context_match.group('right_context')
        else:
            right_context = right_context_match.group('right_context')

        snippet = (left_context, match, right_context)
        return snippet

    def get_snippet(self, position: 'WindowPosition') -> Snippet:
        start, end = position
        assert start >= 0 and start <= len(self)
        assert end >= 0 and end <= len(self) and end >= start
        snippet_size = end - start
        if snippet_size < self.SNIPPET_SIZE:
            effective_start = max(0, start - floor(0.5 * (self.SNIPPET_SIZE - snippet_size)))
            snippet_size = end - effective_start
            effective_end = min(len(self), end + self.SNIPPET_SIZE - snippet_size)
        else:
            effective_start, effective_end = start, end

        left_context = self[effective_start:start]
        match = self[start:end]
        right_context = self[end:effective_end]

        if start != 0:
            left_context = '...{}'.format(left_context)
        if end != len(str(self)):
            right_context = '{}...'.format(right_context)

        snippet = (left_context, match, right_context)
        return snippet

    def find_snippet(self, snippet: Snippet) -> Optional['WindowPosition']:
        position = self._find_exact_snippet(snippet)
        if position is None:
            position = self._find_tokenized_snippet(snippet)
        return position

    def _find_exact_snippet(self, snippet: Snippet) -> Optional['WindowPosition']:
        left_context, match, right_context = snippet
        left_context = left_context.lstrip('...')
        right_context = right_context.rstrip('...')
        context = '{}{}{}'.format(left_context, match, right_context)

        if context not in str(self):
            return None

        start = str(self).index(context) + len(left_context)
        end = start + len(match)

        position = (start, end)
        return position

    def _find_tokenized_snippet(self, snippet: Snippet) -> Optional['WindowPosition']:
        left_context, match, right_context = snippet

        def create_full_match_regex(text: str, token_filter: Optional[Callable[[List[str]], List[str]]]) -> str:
            tokens = tokenize(text)
            token_regexes = map(regex.escape, tokens)
            if token_filter is not None:
                token_regexes = list(token_regexes)
                token_regexes = token_filter(token_regexes)
            full_match_regex = r'\W*'.join(token_regexes)
            return full_match_regex

        left_context_regex = create_full_match_regex(left_context, lambda tokens: tokens[-self.MAXIMUM_REGEX_TOKENS:])
        match_regex = create_full_match_regex(match, None)
        right_context_regex = create_full_match_regex(right_context, lambda tokens: tokens[:self.MAXIMUM_REGEX_TOKENS])

        def _create_page_boundary_regex(text: str) -> Optional[str]:
            tokens = tokenize(text)
            if len(tokens) == 0:
                return None
            maximum_regex_tokens = min(len(tokens), self.MAXIMUM_REGEX_TOKENS)
            page_boundary_regex = f'(\\w+\\W*){{0,{maximum_regex_tokens}}}'
            return page_boundary_regex

        def create_page_beginning_regex(text: str) -> Optional[str]:
            page_boundary_regex = _create_page_boundary_regex(text)
            if page_boundary_regex is None:
                return None
            page_beginning_regex = f'^\\W*{page_boundary_regex}'
            return page_beginning_regex

        def create_page_end_regex(text: str) -> Optional[str]:
            page_boundary_regex = _create_page_boundary_regex(text)
            if page_boundary_regex is None:
                return None
            page_beginning_regex = f'{page_boundary_regex}\\W*$'
            return page_beginning_regex

        left_context_page_beginning_regex = create_page_beginning_regex(left_context)
        right_context_page_end_regex = create_page_end_regex(right_context)

        if left_context_page_beginning_regex is not None:
            left_context_regex = f'({left_context_page_beginning_regex}|{left_context_regex})'
        if right_context_page_end_regex is not None:
            right_context_regex = f'({right_context_page_end_regex}|{right_context_regex})'

        context_regex = f'{left_context_regex}\\W*(?<result>{match_regex})\\W*{right_context_regex}'
        match = regex.search(context_regex, str(self))

        if match is None and self.MAXIMUM_ERRORS > 0:
            context_regex = f'(?b)({context_regex}){{e<={self.MAXIMUM_ERRORS}}}'
            match = regex.search(context_regex, str(self))

        if match is None:
            return None

        start = match.start('result')
        end = match.end('result')

        position = (start, end)
        return position

    def get_markdown_snippet(self, position: 'WindowPosition') -> Markdown:
        left_context, match, right_context = self.get_snippet(position)
        snippet = '{}{}{}'.format(escape(left_context), bold(escape(match)), escape(right_context))
        return Markdown(snippet)

    @property
    @cache
    def text(self) -> str:
        with self.text_filename.open('rt') as f:
            text = f.read()
        return normalize_ocr(text)

    @property
    def is_relevant(self) -> bool:
        is_relevant = (
            self.book_id in self.relevant_pages and
            self.page_id in self.relevant_pages[self.book_id]
        )
        return is_relevant

    @classmethod
    @cache
    def get_filenames(cls, only_relevant: bool = False) -> Tuple[str]:
        num_total, num_relevant = 0, 0
        filenames = []
        input_filenames = cls.ROOT_PATH.glob('*/*.txt')
        if only_relevant:
            input_filenames = tqdm(list(input_filenames), desc='Filtering documents')
        for filename in input_filenames:
            filename = str(filename.relative_to(cls.ROOT_PATH))
            document = Document(filename)
            num_total += 1
            if only_relevant and not document.is_relevant:
                continue
            num_relevant += 1
            filenames.append(filename)
        message = 'Found {} relevant documents out of all {} ({:.2f}%).'
        message = message.format(num_relevant, num_total, 100.0 * num_relevant / num_total)
        if only_relevant:
            LOGGER.info(message)
        return tuple(sorted(filenames))

    @classmethod
    def _get_sentences_filename(cls, basename: str) -> Path:
        filename = (cls.SENTENCES_PATH / basename).with_suffix('.txt')
        return filename

    def __len__(self) -> int:
        return len(str(self))

    def __getitem__(self, key: Any) -> str:
        return str(self)[key]

    def __hash__(self):
        return hash(self.basename)

    def __eq__(self, other) -> bool:
        if isinstance(other, Document):
            return self.basename == other.basename
        return NotImplemented

    def __lt__(self, other) -> bool:
        if isinstance(other, Document):
            return self.basename < other.basename
        return NotImplemented

    def __str__(self) -> str:
        return self.text

    def __repr__(self) -> str:
        return self.basename

    def _ipython_display_(self) -> None:
        document_name = 'Document {}'.format(escape(self.basename))
        document_link = link(document_name, self.document_url)
        document_link = bold('{}:'.format(document_link))
        display(
            Markdown('{} {}'.format(document_link, escape(str(self)))),
            HTML('<img src="{}" width="500" />'.format(self.image_url)),
        )


def document_factory(filename: str) -> Tuple[str, Document]:
    document = Document(filename)
    return (document.basename, document)


def load_documents() -> Documents:
    filenames = Document.get_filenames()
    documents = {
        basename: document
        for basename, document
        in map(document_factory, tqdm(filenames, desc='Loading documents'))
    }
    return documents
