from abc import ABC, abstractmethod
from collections import defaultdict
import json
from logging import getLogger
from typing import List, Tuple, Iterable, Optional, Dict, Any, TYPE_CHECKING
from heapq import heappush, heappushpop
from math import ceil
from urllib.parse import quote_plus
from urllib.request import urlopen

from more_itertools import zip_equal
import numpy as np
from rank_bm25 import BM25Okapi
import regex

from ..config import CONFIG as _CONFIG
from ..document import Document, Match, Snippet, SlidingWindow, WindowPosition
from ..entity import Entity
from .scorer import (
    CharacterScorer,
    Scorer,
    Similarity,
    CharacterJaccardSimilarityScorer,
    WordJaccardSimilarityScorer,
    CharacterEditSimilarityScorer,
    WordEditSimilarityScorer,
    BERTScorer,
    SentenceBERTSimilarityScorer,
)
from ..util import NestableForkingPool, preprocess, tokenize

if TYPE_CHECKING:  # avoid circular dependency
    from ..search import SearchResultList


LOGGER = getLogger(__name__)


_ENTITY_REGEXES: Optional[Tuple['Pattern', 'NumberOfErrors']] = None  # global variable used for sharing state between forked processes


Pattern = Any
NumberOfErrors = int
ResultHead = Tuple[Document, WindowPosition]
ResultTail = Tuple[WindowPosition, Similarity]
Result = Tuple[ResultHead, Similarity]

ResultHeads = List[ResultHead]
Results = List[Result]

Score = float
Rank = Optional[int]
Ranks = Iterable[Rank]


def sort_result_key(item: Result) -> Any:
    result_head, similarity = item
    return similarity, result_head


def sort_results(results: Iterable[Result]) -> Results:
    return sorted(results, key=sort_result_key, reverse=True)


class Index(ABC):
    CONFIG = _CONFIG['index.Index']
    TOPN = CONFIG.getint('number_of_results')

    @abstractmethod
    def _search(self, entity: Entity) -> Iterable[Result]:
        pass

    def search(self, entity: Entity) -> Results:
        results = self._search(entity)
        return sort_results(results)[:self.TOPN]

    def __repr__(self) -> str:
        return self.__class__.__name__

    def __str__(self) -> str:
        return repr(self)


class JaccardSimilarityIndex(Index):
    def __init__(self, documents: Iterable[Document]):
        self.documents = list(documents)

    @abstractmethod
    def _get_scorer(self, entity: Entity) -> CharacterScorer:
        pass

    def _search(self, entity: Entity) -> Iterable[Result]:
        queue = []
        window = SlidingWindow(entity)
        scorer = self._get_scorer(entity)
        for document in self.documents:
            result_heads = [(document, (start, end)) for start, end in window.iterate(document)]
            texts = (document[start:end] for document, (start, end) in result_heads)
            similarities = scorer.score(texts)
            for result in zip_equal(result_heads, similarities):
                priority = sort_result_key(result)
                item = (priority, result)
                if len(queue) == self.TOPN:
                    heappushpop(queue, item)
                else:
                    assert len(queue) < self.TOPN
                    heappush(queue, item)
        results = (result for (priority, result) in queue)
        return results


class CharacterJaccardSimilarityIndex(JaccardSimilarityIndex):
    def __init__(self, documents: Iterable[Document]):
        super().__init__(documents)

    def _get_scorer(self, entity: Entity) -> CharacterScorer:
        return CharacterJaccardSimilarityScorer(entity)


class WordJaccardSimilarityIndex(JaccardSimilarityIndex):
    def __init__(self, documents: Iterable[Document]):
        super().__init__(documents)

    def _get_scorer(self, entity: Entity) -> CharacterScorer:
        return WordJaccardSimilarityScorer(entity)


def _fuzzy_regex_search_helper(document: Document) -> List[Tuple[ResultHead, NumberOfErrors]]:
    text = str(document)
    results = []
    for entity_regex, num_deletions in _ENTITY_REGEXES:
        for match in entity_regex.finditer(text):
            start, end = match.span('result')
            num_errors = sum(match.fuzzy_counts) + num_deletions
            result_head = (document, (start, end))
            result = (result_head, num_errors)
            results.append(result)
    return results


class FuzzyRegexIndex(Index):
    CONFIG = _CONFIG['index.FuzzyRegexIndex']
    TOPN = CONFIG.getint('number_of_results')
    MAXIMUM_ERRORS = CONFIG.getint('maximum_errors')
    MAXIMUM_ERROR_RATIO = CONFIG.getfloat('maximum_error_ratio')
    MAXIMUM_UNSHORTENED_SIZE = CONFIG.getint('maximum_unshortened_size')
    MINIMUM_ERRORS_FOR_MULTIPROCESSING = CONFIG.getint('minimum_errors_for_multiprocessing')
    NUM_WORKERS = None if CONFIG['number_of_workers'] == 'all cpus' else CONFIG.getint('number_of_workers')
    CHUNK_SIZE = CONFIG.getint('chunk_size')
    SHORTENED_SIZE = CONFIG.getint('shortened_size')

    def __init__(self, documents: Iterable[Document]):
        self.documents = list(documents)

    def _make_regex(self, text: str) -> Tuple[regex.Pattern, NumberOfErrors]:
        num_errors = min(self.MAXIMUM_ERRORS, int(ceil(len(text) * self.MAXIMUM_ERROR_RATIO)))
        text_regex = regex.escape(text)
        text_regex = '(?e)(?<result>(^|\\P{{L}}){}(\\P{{L}}|$)){{e<={}}}'.format(text_regex, num_errors)
        return regex.compile(text_regex), num_errors

    def _make_regexes(self, entity: Entity) -> Tuple[List[Tuple[regex.Pattern, NumberOfErrors]], NumberOfErrors]:
        unshortened_entity_regex, num_errors = self._make_regex(str(entity))
        entity_regexes = [(unshortened_entity_regex, 0)]
        if len(entity) > self.MAXIMUM_UNSHORTENED_SIZE:
            left_shortened_entity_regex, _ = self._make_regex(str(entity)[:self.SHORTENED_SIZE])
            num_deletions = max(0, len(entity) - self.SHORTENED_SIZE)
            entity_regexes.append((left_shortened_entity_regex, num_deletions))
        return entity_regexes, num_errors

    def _get_similarity(self, entity: Entity, num_errors: NumberOfErrors) -> float:
        similarity = 1.0 - 1.0 * num_errors / (len(entity) + 3)
        return similarity

    def _singleprocess_search(self, entity: Entity) -> Dict[ResultHead, Similarity]:
        global _ENTITY_REGEXES
        _ENTITY_REGEXES, _ = self._make_regexes(entity)
        all_results = dict()
        for results in map(_fuzzy_regex_search_helper, self.documents):
            for result_head, num_errors in results:
                similarity = self._get_similarity(entity, num_errors)
                all_results[result_head] = similarity
        _ENTITY_REGEXES = None
        return all_results

    def _multiprocess_search(self, entity: Entity) -> Dict[ResultHead, Similarity]:
        global _ENTITY_REGEXES
        _ENTITY_REGEXES, _ = self._make_regexes(entity)
        all_results = dict()
        with NestableForkingPool(self.NUM_WORKERS) as pool:
            for results in pool.imap_unordered(_fuzzy_regex_search_helper, self.documents,
                                               chunksize=self.CHUNK_SIZE):
                for result_head, num_errors in results:
                    similarity = self._get_similarity(entity, num_errors)
                    all_results[result_head] = similarity
        _ENTITY_REGEXES = None
        return all_results

    def _search(self, entity: Entity) -> Iterable[Result]:
        _, num_errors = self._make_regexes(entity)
        if num_errors >= self.MINIMUM_ERRORS_FOR_MULTIPROCESSING:
            results = self._multiprocess_search(entity)
        else:
            results = self._singleprocess_search(entity)
        return results.items()


class OkapiBM25Index(Index):
    CONFIG = _CONFIG['index.OkapiBM25Index']
    TOPN = CONFIG.getint('number_of_results')

    def __init__(self, documents: Iterable[Document]):
        self.documents = list(documents)

    def _collect_result_heads(self, entity: Entity) -> ResultHeads:
        query = set(preprocess(str(entity)))
        result_heads = list()
        window = SlidingWindow(entity)
        for document in self.documents:
            for start, end in window.iterate(document):
                result_head = (document, (start, end))
                text = set(preprocess(document[start:end]))
                if query & text:
                    result_heads.append(result_head)
        return result_heads

    def _search(self, entity: Entity) -> Iterable[Result]:
        result_heads = self._collect_result_heads(entity)
        if not result_heads:
            return []
        index = BM25Okapi(preprocess(document[start:end]) for document, (start, end) in result_heads)
        query = preprocess(str(entity))
        similarities = index.get_scores(query)
        top_indices = np.argsort(similarities)[::-1][:self.TOPN]
        results = ((result_heads[top_index], similarities[top_index]) for top_index in top_indices)
        return results


class RemoteManateeIndex(Index):
    CONFIG = _CONFIG['index.RemoteManateeIndex']
    URL = CONFIG['url']

    def __init__(self, documents: Iterable[Document]):
        self.documents = {
            document.basename: document
            for document
            in documents
        }

    def _search(self, entity: Entity) -> Iterable[Result]:
        scorer = CharacterEditSimilarityScorer(entity)

        result_heads = self._get_result_heads(entity)
        result_heads = list(result_heads)

        def get_match(result_head: ResultHead) -> Match:
            document, position = result_head
            snippet = document.get_snippet(position)
            _, match, __ = snippet
            return match

        matches = map(get_match, result_heads)
        similarities = scorer.score(matches)

        results = zip_equal(result_heads, similarities)

        return results

    def _get_result_heads(self, entity: Entity) -> Iterable[ResultHead]:
        for document, snippet in self._get_documents_and_snippets(entity):
            position = document.find_snippet(snippet)
            if position is None:
                continue
            result_head = (document, position)
            yield result_head

    def _get_documents_and_snippets(self, entity: Entity) -> Iterable[Tuple[Document, Snippet]]:
        query = self._get_query(entity)
        url = self.URL.format(query=query)
        with urlopen(url) as f:
            books = json.load(f)
        if isinstance(books, dict):
            if 'error' not in books:
                raise ValueError(f'{self} returned a dict without the `error` key: {books}')
            error = books['error']
            if error == 'nothing found':
                return []
            else:
                raise ValueError(f'{self} returned an error: {error}')
        for book in books:
            pages, book_id = book['hits'], int(book['id'])
            for page in pages:
                left_context, match, right_context = f'...{page["left"]}', page['word'], f'{page["right"]}...'
                page_id = int(page['page.n'])
                basename = Document.get_basename(book_id, page_id)
                document = self.documents[basename]
                snippet = (left_context, match, right_context)
                yield (document, snippet)

    def _get_query(self, entity: Entity) -> str:
        tokens = tokenize(str(entity))
        query_elements = []
        for token in tokens:
            token = regex.sub(r'\\', r'\\\\', token)
            token = regex.sub(r'"', r'\\"', token)
            query_element = f'[word@lemma="{token}"]'
            query_elements.append(query_element)
        query = ' '.join(query_elements)
        query = quote_plus(query)
        return query


class ExpensiveIndex(Index):
    def __init__(self, candidates: Iterable['SearchResultList']):
        self.candidates = list(candidates)

    @abstractmethod
    def _get_scorer(self, entity: Entity) -> Scorer:
        pass

    def _search(self, entity: Entity) -> Iterable[Result]:
        result_heads = set()
        scorer = self._get_scorer(entity)
        for candidate_result_list in self.candidates:
            candidate_results = candidate_result_list[entity]
            for result_head, _ in candidate_results:
                if result_head in result_heads:
                    continue
                result_heads.add(result_head)
        result_heads = sorted(result_heads)
        texts = (document[start:end] for document, (start, end) in result_heads)
        similarities = scorer.score(texts)
        assert len(result_heads) == len(similarities)
        results = dict()
        for result in zip(result_heads, similarities):
            result_head, similarity = result
            results[result_head] = similarity
        return results.items()


class CharacterEditSimilarityIndex(ExpensiveIndex):
    def __init__(self, candidates: Iterable['SearchResultList']):
        super().__init__(candidates)

    def _get_scorer(self, entity: Entity) -> CharacterScorer:
        return CharacterEditSimilarityScorer(entity)


class WordEditSimilarityIndex(ExpensiveIndex):
    def __init__(self, candidates: Iterable['SearchResultList']):
        super().__init__(candidates)

    def _get_scorer(self, entity: Entity) -> CharacterScorer:
        return WordEditSimilarityScorer(entity)


class BERTScoreIndex(ExpensiveIndex):
    def __init__(self, candidates: Iterable['SearchResultList']):
        super().__init__(candidates)

    def _get_scorer(self, entity: Entity) -> Scorer:
        return BERTScorer(entity)


class SentenceBERTSimilarityIndex(ExpensiveIndex):
    def __init__(self, candidates: Iterable['SearchResultList']):
        super().__init__(candidates)

    def _get_scorer(self, entity: Entity) -> Scorer:
        return SentenceBERTSimilarityScorer(entity)


class RankFusionIndex(Index):
    def __init__(self, candidates: Iterable['SearchResultList']):
        self.candidates = list(candidates)

    @abstractmethod
    def _get_score(self, ranks: Ranks) -> Score:
        pass

    def _search(self, entity: Entity) -> Iterable[Result]:
        result_ranks = defaultdict(lambda: [None] * len(self.candidates))
        for candidate_index, candidate_result_list in enumerate(self.candidates):
            candidate_results = candidate_result_list[entity]
            for rank, (result_head, _) in enumerate(candidate_results):
                result_ranks[result_head][candidate_index] = rank + 1
        for result_head, ranks in result_ranks.items():
            similarity = self._get_score(ranks)
            result = (result_head, similarity)
            yield result


class ReciprocalRankFusionIndex(RankFusionIndex):
    CONFIG = _CONFIG['index.ReciprocalRankFusionIndex']
    K = CONFIG.getint('k')

    def __init__(self, candidates: Iterable['SearchResultList']):
        super().__init__(candidates)

    def _get_score(self, ranks: Ranks) -> Score:
        score = sum(
            1.0 / (rank + self.K)
            for rank
            in ranks
            if rank is not None
        )
        return score


class ConcatenatedIndex(RankFusionIndex):
    def __init__(self, candidates: Iterable['SearchResultList']):
        super().__init__(candidates)
        self.max_rank = max(
            max(
                len(result_list)
                for result_list
                in candidate.results.values()
            )
            for candidate
            in self.candidates
        )

    def _get_score(self, ranks: Ranks) -> Score:
        concatenated_rank = 1
        for rank_number, rank in enumerate(reversed(list(ranks))):
            zero_based_rank = (rank - 1) if rank is not None else self.max_rank
            concatenated_rank += zero_based_rank * (self.max_rank + 1)**rank_number
        score = 1.0 / concatenated_rank
        return score
