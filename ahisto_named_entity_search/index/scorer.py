from abc import ABC, abstractmethod
from typing import List, Set, Iterable, Union

from more_itertools import chunked
from bert_score import BERTScorer as _BERTScorer
from sentence_transformers import SentenceTransformer
from sentence_transformers.util import dot_score
from torch import Tensor

from ..entity import Entity
from ..util import (
    normalize_ocr,
    normalize,
    preprocess,
    error_rate,
    jaccard_index,
    extract_ngrams,
    suppress_stderr,
)
from ..config import CONFIG as _CONFIG


Similarity = float


class Scorer(ABC):
    @abstractmethod
    def score(self, texts: Iterable[str]) -> List[Similarity]:
        pass

    def __str__(self) -> str:
        return self.__class__.__name__

    def __repr__(self) -> str:
        return str(self)


class CharacterScorer(Scorer):
    @abstractmethod
    def character_score(self, text: str) -> Similarity:
        pass

    def score(self, texts: Iterable[str]) -> List[Similarity]:
        return list(map(self.character_score, texts))


class TokenizedScorer(CharacterScorer):
    @abstractmethod
    def token_score(self, text: List[str]) -> Similarity:
        pass

    def character_score(self, text: str) -> Similarity:
        tokens = preprocess(text)
        return self.token_score(tokens)


class CharacterEditSimilarityScorer(CharacterScorer):
    def __init__(self, entity: Entity):
        self.entity = str(entity)

    def character_score(self, text: str) -> Similarity:
        similarity = 1.0 - error_rate(self.entity, text)
        return similarity


class WordEditSimilarityScorer(TokenizedScorer):
    def __init__(self, entity: Entity):
        self.entity = preprocess(str(entity))

    def token_score(self, text: List[str]) -> Similarity:
        similarity = 1.0 - error_rate(self.entity, text)
        return similarity


class CharacterJaccardSimilarityScorer(CharacterScorer):
    CONFIG = _CONFIG['index.CharacterJaccardSimilarityScorer']
    MINN = CONFIG.getint('min_ngram_length')
    MAXN = CONFIG.getint('max_ngram_length')

    def __init__(self, entity: Entity):
        entity = ' {} '.format(normalize(str(entity)))
        self.entity = self._extract_ngrams(entity)

    def _extract_ngrams(self, text: str) -> Set[str]:
        return extract_ngrams(text, self.MINN, self.MAXN)

    def character_score(self, text: str) -> Similarity:
        text = ' {} '.format(normalize(text))
        return jaccard_index(self.entity, self._extract_ngrams(text))


class WordJaccardSimilarityScorer(TokenizedScorer):
    def __init__(self, entity: Entity):
        self.entity = set(preprocess(str(entity)))

    def token_score(self, text: List[str]) -> Similarity:
        return jaccard_index(self.entity, set(text))


class BERTScorer(Scorer):
    CONFIG = _CONFIG['index.BERTScorer']
    MODEL = CONFIG['model']
    BATCH_SIZE = CONFIG.getint('batch_size')

    def __init__(self, entity: Entity):
        self.entity = str(entity)
        self.scorer = _BERTScorer(model_type=self.MODEL)

    @suppress_stderr
    def score(self, texts: Iterable[str]) -> List[Similarity]:
        texts = map(normalize_ocr, texts)
        similarities = []
        for text_batch in chunked(texts, self.BATCH_SIZE):
            entities = len(text_batch) * [self.entity]
            *_, batch_similarities = self.scorer.score(entities, text_batch)
            similarities.extend(batch_similarities.detach().cpu().tolist())
        return similarities


class SentenceBERTSimilarityScorer(Scorer):
    CONFIG = _CONFIG['index.SentenceBERTSimilarityScorer']
    MODEL = CONFIG['model']
    BATCH_SIZE = CONFIG.getint('batch_size')

    def __init__(self, entity: Entity):
        self.model = SentenceTransformer(self.MODEL)
        self.entity = self._encode(str(entity))

    def _encode(self, texts: Union[str, List[str]]) -> Tensor:
        return self.model.encode(texts, convert_to_tensor=True, normalize_embeddings=True,
                                 batch_size=self.BATCH_SIZE, show_progress_bar=False)

    def score(self, texts: Iterable[str]) -> List[Similarity]:
        texts = list(map(normalize_ocr, texts))
        similarities = dot_score(self.entity, self._encode(texts))
        return similarities.detach().cpu().tolist()[0]
