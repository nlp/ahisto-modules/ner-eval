from __future__ import annotations

from collections import defaultdict
from contextlib import contextmanager
from logging import getLogger
from pathlib import Path
from typing import Tuple, List, Optional, Iterable, Dict
import re
import shutil

import comet_ml  # noqa: F401
from adaptor.adapter import Adapter
from adaptor.objectives.MLM import MaskedLanguageModeling
from adaptor.lang_module import LangModule
from adaptor.utils import StoppingStrategy, AdaptationArguments
from more_itertools import zip_equal
from transformers import AutoModelForTokenClassification, AutoTokenizer, pipeline
import torch

from ..config import CONFIG as _CONFIG
from ..document import Document, Sentence
from ..search import TaggedSentence, BioNerTags, BioNerTag, strip_prefix, strip_suffix
from ..util import collect_garbage_from_torch
from .schedule import get_schedule
from .evaluator import AggregateMeanFScoreEvaluator, FScore, LabelMap, Label, MeanFScoreEvaluator
from .objective import BIOTokenWeightedClassification, LabelWeights


LOGGER = getLogger(__name__)


BioNerTagScore = float

EvaluationResult = Dict[AggregateMeanFScoreEvaluator, FScore]


class NerModel:
    CONFIG = _CONFIG['recognition.NerModel']
    ROOT_PATH = Path(CONFIG['root_path'])
    BASE_MODEL = CONFIG['base_model']
    BATCH_SIZE = CONFIG.getint('batch_size')
    GRADIENT_ACCUMULATION_STEPS = CONFIG.getint('gradient_accumulation_steps')
    LEARNING_RATE = CONFIG.getfloat('learning_rate')
    EVAL_STEPS = CONFIG.getint('evaluate_every_n_steps')
    SAVE_STEPS = CONFIG.getint('save_every_n_steps')
    LOGGING_STEPS = CONFIG.getint('log_every_n_steps')
    NUM_TRAIN_EPOCHS = CONFIG.getint('number_of_training_epochs')
    SCHEDULE_NAME = CONFIG['schedule']
    NUM_VALIDATION_SAMPLES = CONFIG.getint('number_of_validation_samples')
    STOPPING_PATIENCE = CONFIG.getint('stopping_patience')
    LABELS: Iterable[Label] = ('B-PER', 'I-PER', 'B-LOC', 'I-LOC', 'O')
    EXTRA_LABELS: Iterable[Label] = ('B-PER-or-LOC', 'I-PER-or-LOC')
    MAX_STEPS = CONFIG.getint('max_steps') if CONFIG['max_steps'] != 'none' else -1
    WARMUP_RATIO = CONFIG.getfloat('warmup_ratio')

    def __init__(self, model_name_or_basename: str, labels: Iterable[Label] = LABELS):
        self.model_name_or_basename = model_name_or_basename
        self.labels = tuple(labels)

    @property
    def tokenizer(self) -> AutoTokenizer:
        tokenizer = AutoTokenizer.from_pretrained(self.model_name_or_basename)
        return tokenizer

    @property
    @contextmanager
    def model(self) -> AutoModelForTokenClassification:
        model = AutoModelForTokenClassification.from_pretrained(self.model_name_or_basename)
        yield model
        collect_garbage_from_torch()

    def __str__(self) -> str:
        return self.model_name_or_basename

    def __repr__(self) -> str:
        return '{}: {}'.format(self.__class__.__name__, self)

    @staticmethod
    def tag_sentences_with_multiple_models(ner_models: Iterable['NerModel'],
                                           sentences: Iterable[Sentence],
                                           allowed_labels: Iterable[Label] = LABELS) -> Iterable[TaggedSentence]:
        sentences = list(sentences)

        if not sentences:
            return

        all_entities_list = [[] for _ in sentences]
        for ner_model in ner_models:
            with ner_model.model as model:
                ner_recognizer = pipeline('ner', model=model, tokenizer=ner_model.tokenizer,
                                          device=0 if torch.cuda.is_available() else -1)
                tagged_sentences = ner_recognizer(sentences)
                del ner_recognizer
            for all_entities, entities in zip_equal(all_entities_list, tagged_sentences):
                all_entities.extend(entities)

        for sentence, all_entities in zip_equal(sentences, all_entities_list):
            words_list: List[re.Match] = list(re.finditer(r'\S+', sentence))
            words_dict: Dict[int, re.Match] = dict()
            ner_tags_dict: Dict[re.Match, Dict[BioNerTag, BioNerTagScore]] = \
                defaultdict(lambda: defaultdict(lambda: 0.0))

            for word in words_list:
                for index in range(word.start(), word.end() + 1):
                    words_dict[index] = word

            for entity in all_entities:
                if entity['entity'] not in allowed_labels:
                    continue
                entity_matches_a_word = False
                for index in range(entity['start'], entity['end'] + 1):
                    if index not in words_dict:
                        continue
                    entity_matches_a_word = True
                    word = words_dict[index]
                    ner_tag = entity['entity']
                    ner_tag_score = (entity['end'] - entity['start']) * entity['score']
                    ner_tags_dict[word][ner_tag] += ner_tag_score
                assert entity_matches_a_word, f'Failed to match {entity} to a word in {sentence}'

            ner_tags_list: List[BioNerTag] = []
            previous_ner_tag: Optional[BioNerTag] = None
            for word in words_list:
                ner_tag_scores = ner_tags_dict[word]
                if ner_tag_scores:
                    ner_tag, _ = max(ner_tag_scores.items(), key=lambda item: (item[1], item[0]))

                    # Prevent I-X tag right after *-Y or O tag if X != Y
                    if strip_suffix(ner_tag) == 'I-' \
                            and (previous_ner_tag is None
                                 or strip_prefix(ner_tag) != strip_prefix(previous_ner_tag)):

                        # Replace the offending I-X tag with the most likely tag out of I-Y, B-X, and O
                        alternative_ner_tags = []
                        if previous_ner_tag is not None and previous_ner_tag != 'O':
                            alternative_ner_tags.append(f'I-{strip_prefix(previous_ner_tag)}')
                        alternative_ner_tags.append(f'B-{strip_prefix(ner_tag)}')

                        best_alternative_ner_tag: Optional[BioNerTag] = None
                        best_alternative_ner_tag_score: float = float('-inf')
                        for alternative_ner_tag in alternative_ner_tags:
                            if alternative_ner_tag not in ner_tag_scores:
                                continue
                            alternative_ner_tag_score = ner_tag_scores[alternative_ner_tag]
                            if alternative_ner_tag_score > best_alternative_ner_tag_score:
                                best_alternative_ner_tag = alternative_ner_tag
                                best_alternative_ner_tag_score = alternative_ner_tag_score
                        if best_alternative_ner_tag is not None:
                            ner_tag = best_alternative_ner_tag
                        else:
                            ner_tag = 'O'
                else:
                    ner_tag = 'O'
                ner_tags_list.append(ner_tag)
                previous_ner_tag = ner_tag

            ner_tags = ' '.join(ner_tags_list)
            tagged_sentence = TaggedSentence(sentence, ner_tags)

            yield tagged_sentence

    def tag_sentences(self, sentences: Iterable[Sentence]) -> Iterable[TaggedSentence]:
        tagged_sentences = self.__class__.tag_sentences_with_multiple_models([self], sentences)
        return tagged_sentences

    def test(self, *args, **kwargs) -> EvaluationResult:
        evaluation_results = self._test(*args, **kwargs)
        collect_garbage_from_torch()
        return evaluation_results

    def _test(self, test_tagged_sentence_basename: str) -> EvaluationResult:
        lang_module = LangModule(self.model_name_or_basename)

        ner_test_texts, ner_test_labels = load_ner_dataset(test_tagged_sentence_basename)
        ner_test_label_weights = get_label_weights(self.labels, ner_test_labels)
        ner_evaluators = list(get_evaluators(self.labels, self.EXTRA_LABELS))
        ner_objective = BIOTokenWeightedClassification(
            ner_test_label_weights,
            lang_module,
            batch_size=1,
            texts_or_path=['placeholder text'],
            labels_or_path=[' '.join(self.labels)],
            val_texts_or_path=ner_test_texts,
            val_labels_or_path=ner_test_labels,
            val_evaluators=ner_evaluators)
        adaptation_arguments = AdaptationArguments(
            output_dir='.',
            stopping_strategy=StoppingStrategy.FIRST_OBJECTIVE_CONVERGED,
            evaluation_strategy='steps',
        )
        schedule = get_schedule('sequential', [ner_objective], adaptation_arguments)
        adapter = Adapter(lang_module, schedule, adaptation_arguments)  # noqa: F841

        test_result = ner_objective.per_objective_log("eval")
        evaluation_results = {
            ner_evaluator: test_result[f'eval_{ner_objective}_{ner_evaluator}']
            for ner_evaluator
            in ner_evaluators
        }

        return evaluation_results

    @classmethod
    def _train_and_save(cls, model_checkpoint_pathname: Path,
                        training_sentence_basename: str, validation_sentence_basename: str,
                        training_tagged_sentence_basename: str,
                        validation_tagged_sentence_basename: str,
                        use_label_weighting: bool = True, **kwargs) -> Adapter:

        def resolve_option(kwarg_name, default_value):
            return kwargs[kwarg_name] if kwarg_name in kwargs else default_value

        schedule_name = resolve_option('schedule_name', cls.SCHEDULE_NAME)
        base_model = resolve_option('base_model', cls.BASE_MODEL)
        batch_size = resolve_option('batch_size', cls.BATCH_SIZE)
        gradient_accumulation_steps = resolve_option('gradient_accumulation_steps',
                                                     cls.GRADIENT_ACCUMULATION_STEPS)
        learning_rate = resolve_option('learning_rate', cls.LEARNING_RATE)
        number_of_training_epochs = resolve_option('number_of_training_epochs', cls.NUM_TRAIN_EPOCHS)
        max_steps = resolve_option('max_steps', cls.MAX_STEPS)
        warmup_ratio = resolve_option('warmup_ratio', cls.WARMUP_RATIO)

        # Load base model
        lang_module = LangModule(base_model)

        # Set up masked language modeling (MLM) training
        mlm_training_texts = list(Document.load_sentences(training_sentence_basename))
        mlm_validation_texts = list(Document.load_sentences(validation_sentence_basename))
        mlm_validation_texts = mlm_validation_texts[:cls.NUM_VALIDATION_SAMPLES]

        mlm_objective = MaskedLanguageModeling(lang_module,
                                               batch_size=batch_size,
                                               texts_or_path=mlm_training_texts,
                                               val_texts_or_path=mlm_validation_texts)

        ner_training_texts, ner_training_labels = load_ner_dataset(training_tagged_sentence_basename)
        ner_training_label_weights = get_label_weights(cls.LABELS, ner_training_labels)
        ner_validation_texts, ner_validation_labels = load_ner_dataset(validation_tagged_sentence_basename)
        ner_validation_texts = ner_validation_texts[:cls.NUM_VALIDATION_SAMPLES]
        ner_validation_labels = ner_validation_labels[:cls.NUM_VALIDATION_SAMPLES]

        if not use_label_weighting:
            ner_training_label_weights = {label: 1.0 for label in ner_training_label_weights}

        ner_evaluators = get_evaluators(cls.LABELS)
        ner_evaluators = filter(lambda evaluator: evaluator.determines_convergence, ner_evaluators)
        ner_evaluators = list(ner_evaluators)

        ner_objective = BIOTokenWeightedClassification(
            ner_training_label_weights,
            lang_module,
            batch_size=batch_size,
            texts_or_path=ner_training_texts,
            labels_or_path=ner_training_labels,
            val_texts_or_path=ner_validation_texts,
            val_labels_or_path=ner_validation_labels,
            val_evaluators=ner_evaluators)

        # Train MLM and NER in parallel until convergence on validation
        shutil.rmtree(model_checkpoint_pathname, ignore_errors=True)
        adaptation_arguments = AdaptationArguments(
            output_dir=str(model_checkpoint_pathname),
            stopping_strategy=StoppingStrategy.FIRST_OBJECTIVE_CONVERGED,
            evaluation_strategy='steps',
            eval_steps=cls.EVAL_STEPS,
            stopping_patience=cls.STOPPING_PATIENCE,
            save_strategy='steps',
            save_steps=cls.SAVE_STEPS,
            save_total_limit=1,
            learning_rate=learning_rate,
            logging_strategy='steps',
            logging_steps=cls.LOGGING_STEPS,
            do_train=True,
            do_eval=True,
            gradient_accumulation_steps=gradient_accumulation_steps,
            num_train_epochs=number_of_training_epochs,
            max_steps=max_steps,
            warmup_ratio=warmup_ratio,
            fp16=True,
            fp16_full_eval=True,
        )

        schedule = get_schedule(schedule_name, [mlm_objective, ner_objective], adaptation_arguments)

        adapter = Adapter(lang_module, schedule, adaptation_arguments)
        return adapter

    @classmethod
    def train_and_save(cls, model_checkpoint_basename: str, model_basename: str, *args,
                       remove_checkpoints: bool = True, **kwargs) -> 'NerModel':

        # Train NER model
        model_checkpoint_pathname = cls.ROOT_PATH / model_checkpoint_basename
        adapter = cls._train_and_save(model_checkpoint_pathname, *args, **kwargs)
        collect_garbage_from_torch()
        adapter.train()

        # Remove checkpoints
        if remove_checkpoints:
            shutil.rmtree(model_checkpoint_pathname, ignore_errors=True)

        # Save NER model
        model_pathname = cls.ROOT_PATH / model_basename
        shutil.rmtree(model_pathname, ignore_errors=True)
        adapter.save_model(str(model_pathname))

        return cls.load(model_basename)

    @classmethod
    def load(cls, model_basename: str) -> 'NerModel':
        model_pathname = cls.ROOT_PATH / model_basename
        model_pathname = model_pathname / 'BIOTokenWeightedClassification'
        model_name_or_basename = str(model_pathname)
        return cls(model_name_or_basename)


def load_ner_dataset(tagged_sentence_basename: str) -> Tuple[List[Sentence], List[BioNerTags]]:
    ner_texts, all_ner_tags = [], []
    for tagged_sentence in TaggedSentence.load(tagged_sentence_basename):
        ner_texts.append(tagged_sentence.sentence)
        all_ner_tags.append(tagged_sentence.ner_tags)
    return ner_texts, all_ner_tags


def get_evaluators(labels: Iterable[Label],
                   extra_labels: Optional[Iterable[Label]] = None) -> Iterable[AggregateMeanFScoreEvaluator]:
    labels = sorted(labels)
    if extra_labels is not None:
        labels += sorted(extra_labels)

    label_map: LabelMap = {
        line_id: line_id_index
        for line_id_index, line_id
        in enumerate(labels)
    }

    for beta in sorted(AggregateMeanFScoreEvaluator.BETAS) + [None]:
        for group_name in sorted(AggregateMeanFScoreEvaluator.GROUPS.keys()) + [None]:
            group_names = [group_name] if group_name is not None else None
            decides_convergence = beta is None and group_name is None
            yield AggregateMeanFScoreEvaluator(label_map, group_names, beta,
                                               decides_convergence=decides_convergence)
    yield MeanFScoreEvaluator(decides_convergence=False)


def get_label_weights(labels: Iterable[Label], dataset: Iterable[BioNerTags]) -> LabelWeights:
    labels = set(labels)
    label_counts = {label: 0 for label in labels}
    for bio_ner_tags in dataset:
        for label in bio_ner_tags.split():
            assert label in label_counts
            label_counts[label] += 1
    label_weights = {
        label: count**-1 if count > 0 else 0.0
        for label, count
        in label_counts.items()
    }
    return label_weights
