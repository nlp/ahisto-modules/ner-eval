from typing import Dict, Iterable, Optional, Set, List, Tuple
from functools import total_ordering

from adaptor.evaluators.token_classification import TokenClassificationEvaluator, MeanFScore
from adaptor.utils import AdaptationDataset
from more_itertools import zip_equal
import torch
from transformers import PreTrainedTokenizer

from ..config import CONFIG as _CONFIG
from ..search import BioNerTag as Label


Labels = Iterable[Label]
GroupName = str
GroupNames = Iterable[GroupName]
LabelId = int
Group = Set[Label]
LabelMap = Dict[Label, LabelId]
GroupMap = Dict[GroupName, Group]

Beta = float
FScore = float


@total_ordering
class MeanFScoreEvaluator(MeanFScore):
    def __hash__(self):
        return hash(None)

    def __eq__(self, other) -> bool:
        if isinstance(other, MeanFScoreEvaluator):
            return True
        return NotImplemented

    def __lt__(self, other) -> bool:
        if isinstance(other, MeanFScoreEvaluator):
            return False
        return NotImplemented

    def __repr__(self) -> str:
        return 'Macro-averaged non-aggregate F1-score'

    def __str__(self) -> str:
        return repr(self)


@total_ordering
class AggregateMeanFScoreEvaluator(TokenClassificationEvaluator):
    CONFIG = _CONFIG['recognition.AggregateMeanFScoreEvaluator']
    DEFAULT_GROUP_NAMES = tuple(CONFIG['default_group_names'].split('+'))
    BETA: Beta = CONFIG.getfloat('f_score_beta')
    BETAS = (0, float('inf'))
    GROUPS: GroupMap = {
        'PER': {'B-PER', 'I-PER', 'B-ORG', 'I-ORG'},
        'LOC': {'B-LOC', 'I-LOC'},
        'O': {'O', 'B-MISC', 'I-MISC'},
    }

    smaller_is_better: bool = False

    def __init__(self, label_map: LabelMap, group_names: Optional[GroupNames],
                 beta: Optional[Beta], *args, **kwargs):
        self.label_map = label_map
        self.group_names = self.DEFAULT_GROUP_NAMES if group_names is None else tuple(group_names)
        self.beta = beta if beta is not None else self.BETA
        super().__init__(*args, **kwargs)

    def __call__(self, model: torch.nn.Module, tokenizer: PreTrainedTokenizer,
                 dataset: AdaptationDataset, ignored_index: LabelId = -100) -> FScore:
        expected_labels, actual_labels = self._collect_token_predictions(model, dataset)

        mean_f_score, total_number_of_samples = 0, 0
        for group_name in self.group_names:
            number_of_samples, f_score = self.get_f_score(
                self.GROUPS[group_name], expected_labels, actual_labels, ignored_index)
            mean_f_score += number_of_samples * f_score
            total_number_of_samples += number_of_samples
        if total_number_of_samples > 0:
            mean_f_score /= total_number_of_samples

        return mean_f_score

    def get_f_score(self, group: Group, expected_labels: List[LabelId],
                    actual_labels: List[LabelId], ignored_index: LabelId) -> Tuple[int, FScore]:
        expected_categories: Set[LabelId] = {
            self.label_map[line_id]
            for line_id
            in group
            if line_id in self.label_map
        }

        true_positives, false_positives, false_negatives = 0, 0, 0
        for expected_label, actual_label in zip_equal(expected_labels, actual_labels):
            if expected_label == ignored_index:
                continue
            if expected_label in expected_categories and actual_label in expected_categories:
                true_positives += 1
            elif expected_label not in expected_categories and actual_label in expected_categories:
                false_positives += 1
            elif expected_label in expected_categories and actual_label not in expected_categories:
                false_negatives += 1

        if self.beta >= float('inf'):
            number_of_samples = true_positives + false_negatives
            recall = true_positives / number_of_samples if true_positives > 0 else 0.0
            f_score = recall
        elif self.beta <= 0:
            number_of_samples = true_positives + false_positives
            precision = true_positives / number_of_samples if true_positives > 0 else 0.0
            f_score = precision
        else:
            numerator = (1.0 + self.beta**2) * true_positives
            denominator = numerator + self.beta**2 * false_negatives + false_positives
            f_score = numerator / denominator if numerator > 0 else 0.0
            number_of_samples = true_positives + false_positives + false_negatives

        return number_of_samples, f_score

    def __hash__(self):
        return hash((self.group_names, self.beta))

    def __eq__(self, other) -> bool:
        if isinstance(other, AggregateMeanFScoreEvaluator):
            return self.group_names == other.group_names and self.beta == other.beta
        return NotImplemented

    def __lt__(self, other) -> bool:
        if isinstance(other, AggregateMeanFScoreEvaluator):
            return self.group_names < other.group_names and self.beta < other.beta
        return NotImplemented

    def __repr__(self) -> str:
        group_names = '+'.join(self.group_names)
        if self.beta >= float('inf'):
            return f'R ({group_names})'
        elif self.beta <= 0:
            return f'P ({group_names})'
        else:
            return f'F-{self.beta:g} ({group_names})'

    def __str__(self) -> str:
        return repr(self)
