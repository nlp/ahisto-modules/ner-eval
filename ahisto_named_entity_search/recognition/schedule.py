from typing import Iterable

from adaptor.objectives.objective_base import Objective
from adaptor.schedules import Schedule, SequentialSchedule, ParallelSchedule
from adaptor.utils import AdaptationArguments

from ..config import CONFIG as _CONFIG


ScheduleName = str


class FairSequentialSchedule(Schedule):
    CONFIG = _CONFIG['recognition.FairSequentialSchedule']
    MAX_NUM_TRAIN_EPOCHS = CONFIG.getint('maximum_number_of_training_epochs_per_objective')

    label = 'fair_sequential'

    def _sample_objectives(self, split: str) -> Iterable[Objective]:
        assert split == 'train'
        while True:
            for objective in self.objectives[split].values():
                starting_epoch = objective.epoch
                for _ in range(objective.dataset_length[split]):
                    if objective in self.converged_objectives and not self.args.log_converged_objectives:
                        continue
                    num_train_epochs = objective.epoch - starting_epoch
                    if num_train_epochs > self.MAX_NUM_TRAIN_EPOCHS:
                        continue
                    yield objective


class FineTuningSchedule(Schedule):
    CONFIG = _CONFIG['recognition.FineTuningSchedule']
    MAX_NUM_TRAIN_EPOCHS = CONFIG.getint('maximum_number_of_training_epochs_per_objective')

    label = 'fine_tuning'

    def _sample_objectives(self, split: str) -> Iterable[Objective]:
        assert split == 'train'
        objectives = self.objectives[split].values()
        remaining_objectives = list(objectives)
        for objective in objectives:
            remaining_objectives = remaining_objectives[1:]
            starting_epoch = objective.epoch
            while True:
                # Prevent future objectives from affecting early stopping
                for remaining_objective in remaining_objectives:
                    for evaluator, values in remaining_objective.evaluations_history["eval"].items():
                        remaining_objective.evaluations_history["eval"][evaluator] = []
                if objective in self.converged_objectives and not self.args.log_converged_objectives:
                    break
                num_train_epochs = objective.epoch - starting_epoch
                if num_train_epochs > self.MAX_NUM_TRAIN_EPOCHS:
                    break
                yield objective


def get_schedule(schedule_name: str, objectives: Iterable[Objective],
                 adaptation_arguments: AdaptationArguments) -> Schedule:
    objectives = list(objectives)
    if schedule_name == 'sequential':
        schedule = SequentialSchedule(objectives, adaptation_arguments)
    elif schedule_name == 'fair-sequential':
        schedule = FairSequentialSchedule(objectives, adaptation_arguments)
    elif schedule_name == 'fine-tuning':
        schedule = FineTuningSchedule(objectives, adaptation_arguments)
    elif schedule_name == 'parallel':
        schedule = ParallelSchedule(objectives, adaptation_arguments)
    elif schedule_name == 'just-last':
        schedule = SequentialSchedule(objectives[-1:], adaptation_arguments)
    else:
        raise ValueError(f'Unknown schedule "{schedule_name}"')
    return schedule
