from itertools import islice
from typing import Dict, Iterable, Optional, Union

from adaptor.objectives.classification import TokenClassification
import regex
import torch
from transformers import DataCollatorForTokenClassification, BatchEncoding

from .evaluator import Label, Labels, LabelId
from ..search import strip_prefix, strip_suffix


LabelWeight = float
LabelWeights = Dict[Label, LabelWeight]
Token = str


class BIOTokenWeightedClassification(TokenClassification):
    def __init__(self, label_weights: LabelWeights, *args, **kwargs):
        self.label_weights = label_weights
        super().__init__(*args, **kwargs)

    def _wordpiece_token_label_alignment(self,
                                         texts: Labels,
                                         labels: Labels,
                                         label_all_tokens: bool = True,
                                         ignore_label_idx: LabelId = -100) -> Iterable[Dict[str, torch.LongTensor]]:
        texts, labels = list(texts), list(labels)

        collator = DataCollatorForTokenClassification(self.tokenizer, pad_to_multiple_of=8)
        batch_features = []

        # special tokens identification: general heuristic
        ids1 = self.tokenizer("X").input_ids
        ids2 = self.tokenizer("Y").input_ids

        special_bos_tokens = []
        for i in range(len(ids1)):
            if ids1[i] == ids2[i]:
                special_bos_tokens.append(ids1[i])
            else:
                break

        special_eos_tokens = []
        for i in range(1, len(ids1)):
            if ids1[-i] == ids2[-i]:
                special_eos_tokens.append(ids1[-i])
            else:
                break
        special_eos_tokens = list(reversed(special_eos_tokens))

        # per-sample iteration
        for text, text_labels in zip(texts, labels):
            tokens = text.split()
            labels = text_labels.split()

            assert len(tokens) == len(labels), \
                "A number of tokens in the first line is different than a number of labels. " \
                "Text: %s \nLabels: %s" % (text, text_labels)

            tokens_ids = self.tokenizer(tokens, truncation=True, add_special_tokens=False).input_ids

            wpiece_ids = special_bos_tokens.copy()

            # labels of BoS and EoS are always "other"
            out_label_ids = [ignore_label_idx] * len(special_bos_tokens)

            def get_label_ids(head_label: Label) -> Iterable[LabelId]:
                head_label_id = self.labels_map[head_label]
                tail_label = f'I-{strip_prefix(head_label)}' if strip_suffix(head_label) == 'B-' else head_label
                tail_label_id = self.labels_map[tail_label]

                yield head_label_id
                while True:
                    yield tail_label_id

            def is_punctuation(token: Token) -> bool:
                punctuation_regex = r'^\W*$'
                punctuation_match = regex.match(punctuation_regex, token)
                is_punctuation = punctuation_match is not None
                return is_punctuation

            def strip_trailing_punctuation(label_ids: Iterable[LabelId],
                                           tokens: Iterable[Token]) -> Iterable[LabelId]:
                label_ids, tokens = list(label_ids), list(tokens)
                assert len(label_ids) == len(tokens)
                for token_index, token in reversed(list(enumerate(tokens))):
                    if is_punctuation(token):
                        label_ids[token_index] = self.labels_map['O']
                    else:
                        break
                return label_ids

            for label_index, (token_ids, label) in enumerate(zip(tokens_ids, labels)):
                # chain the wordpieces without the special symbols for each token
                wpiece_ids.extend(token_ids)
                if label_all_tokens:
                    # label all wordpieces
                    label_ids = get_label_ids(label)
                    label_ids = islice(label_ids, len(token_ids))
                    if label != 'O':
                        if label_index + 1 >= len(labels):
                            should_strip_punctuation = True
                        else:
                            label_type = strip_prefix(label)
                            next_label = labels[label_index + 1]
                            next_label_type = strip_prefix(next_label)
                            should_strip_punctuation = label_type != next_label_type
                        if should_strip_punctuation:
                            tokens = self.tokenizer.batch_decode(token_ids)
                            label_ids = strip_trailing_punctuation(label_ids, tokens)
                    out_label_ids.extend(label_ids)
                else:
                    # label only the first wordpiece
                    out_label_ids.append(self.labels_map[label])
                    # ignore the predictions over other token's wordpieces from the loss
                    out_label_ids.extend([ignore_label_idx] * (len(token_ids) - 1))

            out_label_ids.extend([ignore_label_idx] * len(special_eos_tokens))
            wpiece_ids.extend(special_eos_tokens.copy())

            assert len(out_label_ids) == len(wpiece_ids), \
                "We found misaligned labels in sample: '%s'" % text

            if self.tokenizer.model_max_length is None:
                truncated_size = len(out_label_ids)
            else:
                truncated_size = min(self.tokenizer.model_max_length, len(out_label_ids))

            batch_features.append({"input_ids": wpiece_ids[:truncated_size],
                                   "attention_mask": [1] * truncated_size,
                                   "labels": out_label_ids[:truncated_size]})
            # maybe yield a batch
            if len(batch_features) == self.batch_size:
                yield collator(batch_features)
                batch_features = []
        if batch_features:
            yield collator(batch_features)

        # check that the number of outputs of the selected compatible head matches the just-parsed data set
        num_outputs = list(self.compatible_head_model.parameters())[-1].shape[0]
        num_labels = len(self.labels_map)
        assert num_outputs == num_labels, "A number of the outputs for the selected %s head (%s) " \
                                          "does not match a number of token labels (%s)" \
                                          % (self.compatible_head, num_outputs, num_labels)

    def _compute_loss(self,
                      logit_outputs: torch.FloatTensor,
                      labels: torch.LongTensor,
                      inputs: Optional[Union[BatchEncoding, Dict[str, torch.Tensor]]] = None,
                      attention_mask: Optional[torch.LongTensor] = None) -> torch.FloatTensor:
        weights_dict = {
            self.labels_map[label]: weight
            for label, weight
            in self.label_weights.items()
        }
        weights = []
        for expected_label_id, (label_id, weight) in enumerate(sorted(weights_dict.items())):
            assert expected_label_id == label_id
            weights.append(weight)
        weight_tensor = torch.tensor(weights)
        weight_tensor = weight_tensor.to(labels.device)
        loss_fct = torch.nn.CrossEntropyLoss(weight=weight_tensor)

        # Only keep active parts of the loss
        if attention_mask is not None:
            active_loss = attention_mask.view(-1) == 1
            active_logits = logit_outputs.view(-1, len(self.labels_map))
            active_labels = torch.where(
                active_loss, labels.view(-1), torch.tensor(loss_fct.ignore_index).type_as(labels)
            )
            loss = loss_fct(active_logits, active_labels)
        else:
            loss = loss_fct(logit_outputs.view(-1, len(self.labels_map)), labels.view(-1))

        return loss
