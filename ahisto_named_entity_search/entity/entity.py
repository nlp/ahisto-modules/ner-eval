from __future__ import annotations

from abc import ABC, abstractmethod
from collections import defaultdict
from contextlib import contextmanager
from itertools import islice
from logging import getLogger
from functools import total_ordering
from pathlib import Path
import re
from typing import Set, Iterable, Tuple, Dict, Optional, TYPE_CHECKING
from urllib.parse import quote_plus

from IPython.display import display, Markdown
from lxml import etree
from more_itertools import zip_equal
import regex
from tqdm import tqdm

from ..config import CONFIG as _CONFIG
from ..util import cache, normalize_ocr

if TYPE_CHECKING:  # avoid circular dependency
    from ..document import Sentence
    from ..search import TaggedSentence, BioNerTag


CONFIG = _CONFIG['entity.Regest']
LOGGER = getLogger(__name__)

COMPOSITE_ENTITY = re.compile(r'(?P<before_parens>.*)\((?P<inside_parens>[^)]*)\)\s*')


NerTag = str

GuardedBioNerTag = str


@total_ordering
class Regest:
    ROOT_PATH = Path(CONFIG['root_path'])
    DOCUMENT_URL = CONFIG['document_url']

    def __init__(self, filename: str):
        self.filename = filename
        self.basename = str(Path(filename).with_suffix(''))
        self.document_url = self.DOCUMENT_URL.format(basename=quote_plus(self.basename))
        self.text_filename = self.ROOT_PATH / Path(self.filename).with_suffix('.xml')

    @contextmanager
    def _open(self) -> etree._ElementTree:
        parser = etree.XMLParser(huge_tree=True, encoding='utf-8')
        with self.text_filename.open('rb') as f:
            document = etree.parse(f, parser)
            yield document

    def _entities_and_elements(self, document: etree._Element) -> Iterable[Tuple[Iterable['Entity'], etree._Element]]:
        for entity_element in document.xpath('.//placeName'):
            if entity_element.attrib['reg']:
                entities = Place.from_text(entity_element.attrib['reg'])
                yield entities, entity_element
        for entity_element in document.xpath('.//persName'):
            if entity_element.attrib['reg']:
                entities = Person.from_text(entity_element.attrib['reg'])
                yield entities, entity_element

    @property
    @cache
    def entities(self) -> Set['Entity']:
        all_entities = set()
        with self._open() as document:
            for entities, _ in self._entities_and_elements(document):
                all_entities |= set(entities)
        return all_entities

    def get_tagged_sentences(self, entity_map: Optional[Dict['Entity', 'Entity']]) -> Iterable['TaggedSentence']:
        from ..search import TaggedSentence

        def guard_tag(ner_tag: 'BioNerTag') -> GuardedBioNerTag:
            guarded_ner_tag = f'___ner_tag___{ner_tag}'
            return guarded_ner_tag

        def unguard_tag(guarded_ner_tag: GuardedBioNerTag) -> 'BioNerTag':
            assert guarded_ner_tag.startswith('___ner_tag___')
            ner_tag = guarded_ner_tag[13:]
            return ner_tag

        def get_bio_ner_tags(ner_tag: NerTag) -> Iterable['BioNerTag']:
            assert ner_tag != 'O'
            assert not ner_tag.startswith('B-')
            assert not ner_tag.startswith('I-')

            yield f'B-{ner_tag}'
            while True:
                yield f'I-{ner_tag}'

        def replace_entity_element_with_ner_tags(entity_element: etree._Element,
                                                 ner_tag: NerTag) -> None:
            words = get_text_content(entity_element).split()
            number_of_tags = len(words)
            ner_tags = get_bio_ner_tags(ner_tag)
            ner_tags = islice(ner_tags, number_of_tags)
            ner_tags = map(guard_tag, ner_tags)
            ner_tags = list(ner_tags)

            replacement_text = ' '.join(ner_tags)
            replacement_text = f' {replacement_text} '

            replacement_element = etree.Element('replacement')
            replacement_element.text = replacement_text
            replacement_element.tail = entity_element.tail

            parent_element = entity_element.getparent()
            parent_element.replace(entity_element, replacement_element)

        def replace_with_spaces(element: etree._Element) -> None:
            replacement_element = etree.Element('replacement')
            replacement_element.text = ' '

            parent_element = element.getparent()
            parent_element.replace(element, replacement_element)

        def surround_with_spaces(element: etree._Element) -> None:
            if element.text is None:
                element.text = ' '
            else:
                element.text = f' {element.text}'

            if element.tail is None:
                element.tail = ' '
            else:
                element.tail = f' {element.tail}'

        def get_sentences(paragraph: str) -> Iterable['Sentence']:
            sentence_regex = r'.*?(\P{L}\p{Ll}+\.\s+|$)'
            for match in regex.finditer(sentence_regex, paragraph):
                sentence = match.group()
                sentence = sentence.strip()
                if sentence:
                    yield sentence

        with self._open() as document:
            for paragraph_element in document.xpath('//abstract/p'):
                for transcription_element in paragraph_element.xpath('.//transcription'):
                    replace_with_spaces(transcription_element)

                for _, entity_element in self._entities_and_elements(paragraph_element):
                    surround_with_spaces(entity_element)

                paragraph_text = get_text_content(paragraph_element)

                for entities, entity_element in self._entities_and_elements(paragraph_element):
                    ner_tags = set()
                    ner_tag: Optional[NerTag] = None
                    for entity in entities:
                        if entity_map is not None and entity in entity_map:
                            ner_tag = entity_map[entity].ner_tag
                            break
                        ner_tags.add(entity.ner_tag)
                    if ner_tag is None:
                        assert len(ner_tags) == 1
                        ner_tag, = ner_tags
                    replace_entity_element_with_ner_tags(entity_element, ner_tag)

                paragraph_tags = get_text_content(paragraph_element)
                paragraph_tags = [
                    'O' if word == ner_tag else unguard_tag(ner_tag)
                    for word, ner_tag
                    in zip_equal(paragraph_text.split(), paragraph_tags.split())
                ]

                paragraph_tags_iter = iter(paragraph_tags)
                for sentence in get_sentences(paragraph_text):
                    words = sentence.split()
                    number_of_tags = len(words)
                    ner_tags = islice(paragraph_tags_iter, number_of_tags)
                    ner_tags = list(ner_tags)
                    ner_tags = ' '.join(ner_tags)

                    tagged_sentence = TaggedSentence(sentence, ner_tags)

                    yield tagged_sentence

                assert next(paragraph_tags_iter, None) is None

    def __hash__(self):
        return hash(self.basename)

    def __eq__(self, other) -> bool:
        if isinstance(other, Regest):
            return self.basename == other.basename
        return NotImplemented

    def __lt__(self, other) -> bool:
        if isinstance(other, Regest):
            return self.basename < other.basename
        return NotImplemented

    @property
    @cache
    def paragraphs(self) -> Tuple[str]:
        paragraphs = tuple(self._paragraphs)
        return paragraphs

    @property
    def _paragraphs(self) -> Iterable[str]:
        with self._open() as document:
            for paragraph in document.xpath('//abstract/p'):
                text = get_text_content(paragraph)
                yield text

    @property
    @cache
    def text(self) -> str:
        text = '\n\n'.join(self.paragraphs)
        return text

    def __str__(self) -> str:
        return self.text

    def __repr__(self) -> str:
        return str(self)

    def _ipython_display_(self) -> None:
        summary = 'Regest `{}` with {} entities'.format(self.basename, len(self.entities))
        display(Markdown(summary))


@total_ordering
class Entity(ABC):
    def __init__(self, text: str):
        if not text:
            raise ValueError('Empty entity')
        self.text = normalize_ocr(text)

    @classmethod
    def from_text(entity_class: type['Entity'], text: str) -> Iterable['Entity']:
        match = re.fullmatch(COMPOSITE_ENTITY, text)
        if match is None:
            yield entity_class(text)
        else:
            yield entity_class(match.group('before_parens'))
            yield entity_class(match.group('inside_parens'))

    def __len__(self) -> int:
        return len(str(self))

    def __hash__(self):
        return hash((self.__class__.__name__, str(self)))

    def __eq__(self, other) -> bool:
        if isinstance(other, Entity):
            return repr(self) == repr(other)
        return NotImplemented

    def __lt__(self, other) -> bool:
        if isinstance(other, Entity):
            return repr(self) < repr(other)
        return NotImplemented

    def __str__(self) -> str:
        return self.text

    def __repr__(self) -> str:
        return '{}: {}'.format(self.__class__.__name__, self)

    @property
    @abstractmethod
    def ner_tag(self) -> NerTag:
        pass

    @staticmethod
    def from_repr(entity_repr: str) -> 'Entity':
        if entity_repr.startswith('Place: '):
            return Place(entity_repr[7:])
        else:
            assert entity_repr.startswith('Person: ')
            return Person(entity_repr[8:])

    def _ipython_display_(self) -> None:
        display(Markdown(str(self)))


class Place(Entity):
    ner_tag = 'LOC'

    def __init__(self, text):
        super().__init__(text)


class Person(Entity):
    ner_tag = 'PER'

    def __init__(self, text):
        super().__init__(text)


def get_text_content(element: etree._Element) -> str:
    text = ''.join(element.itertext())
    text = text.strip()
    text = re.sub(r'\s+', ' ', text)
    return text


def load_regests() -> Set[Regest]:
    filenames = sorted(Regest.ROOT_PATH.glob('*.xml'))
    regests = set()
    for filename in filenames:
        filename = str(Path(filename).relative_to(Regest.ROOT_PATH))
        regest = Regest(filename)
        regests.add(regest)
    LOGGER.info(f'Loaded {len(regests)} regests.')
    return regests


def load_entities() -> Dict[Entity, Set[Regest]]:
    regests = load_regests()
    places, persons = defaultdict(lambda: set()), defaultdict(lambda: set())
    all_entities = defaultdict(lambda: set())
    for regest in tqdm(regests, desc='Loading entities'):
        for entity in regest.entities:
            if isinstance(entity, Place):
                places[entity].add(regest)
                all_entities[entity].add(regest)
            else:
                assert isinstance(entity, Person)
                persons[entity].add(regest)
                all_entities[entity].add(regest)
    assert len(all_entities) == len(places) + len(persons)
    message = 'Loaded {} entities: {} places ({:.2f}%) and {} persons ({:.2f}%).'
    message = message.format(
        len(all_entities),
        len(places),
        100.0 * len(places) / len(all_entities),
        len(persons),
        100.0 * len(persons) / len(all_entities),
    )
    LOGGER.info(message)
    return all_entities
