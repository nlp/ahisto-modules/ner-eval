from collections import defaultdict
from enum import Enum
from logging import getLogger
from typing import Dict, Iterable, Optional, Set, Type, Callable
from pathlib import Path

from openpyxl import load_workbook
from openpyxl.cell.cell import Cell
from xlsxwriter import Workbook

from ..config import CONFIG as _CONFIG
from .entity import Entity, Place, Person, Regest


LOGGER = getLogger(__name__)


class EntityType(Enum):
    PERSON = (Person, 'O')
    PLACE = (Place, 'M')

    @classmethod
    def from_entity_class(cls, entity_class: Optional[Type[Entity]]) -> Optional['EntityType']:
        if entity_class is None:
            return None
        for value in cls:
            if value.to_entity_class() == entity_class:
                return value
        raise ValueError('Unexpected entity class: {}'.format(entity_class))

    @classmethod
    def from_entity(cls, entity: Optional[Entity]) -> Optional['EntityType']:
        if entity is None:
            return None
        return cls.from_entity_class(entity.__class__)

    @classmethod
    def from_symbol(cls, symbol: Optional[str]) -> Optional['EntityType']:
        if symbol is None:
            return None
        for value in cls:
            if str(value) == symbol:
                return value
        raise ValueError('Unexpected symbol: {}'.format(symbol))

    def __str__(self) -> str:
        return self.value[1]

    def to_entity_class(self) -> Type[Entity]:
        return self.value[0]

    @classmethod
    def list_symbols(cls) -> str:
        return ', '.join(str(value) for value in cls)


class Approval(Enum):
    APPROVED = 'A'
    DISAPPROVED = 'N'
    TOO_LONG = '>'
    TOO_SHORT = '<'

    @classmethod
    def from_symbol(cls, symbol: Optional[str]) -> Optional['Approval']:
        if symbol is None:
            return None
        for value in cls:
            if str(value) == symbol:
                return value
        raise ValueError('Unexpected symbol: {}'.format(symbol))

    def __str__(self) -> str:
        return self.value

    @classmethod
    def list_symbols(cls) -> str:
        return ', '.join(str(value) for value in cls)


class Patch:
    def __init__(self, entity: Entity, approval: Optional[Approval],
                 entity_type: Optional[EntityType], canonical_form: Optional[str],
                 note: Optional[str]):
        self.entity = entity
        self.approval = approval
        self.entity_type = entity_type
        self.canonical_form = canonical_form
        self.note = note

    def __eq__(self, other) -> bool:
        if isinstance(other, Patch):
            return all(
                (
                    self.entity == other.entity,
                    self.approval == other.approval,
                    self.entity_type == other.entity_type,
                    self.canonical_form == other.canonical_form,
                    self.note == other.note,
                )
            )
        return NotImplemented


EntityMap = Callable[[Patch], Entity]
EntityFilter = Callable[[Patch], bool]
RegestFilter = Callable[[Regest], bool]


def default_entity_map_persons(patch: Patch) -> Entity:
    entity_type = EntityType.PERSON
    entity = _default_entity_map(entity_type, patch)
    return entity


def default_entity_map_places(patch: Patch) -> Entity:
    entity_type = EntityType.PLACE
    entity = _default_entity_map(entity_type, patch)
    return entity


def _default_entity_map(default_entity_type: EntityType, patch: Patch) -> Entity:
    entity_type = patch.entity_type if patch.entity_type is not None else default_entity_type
    entity_class = entity_type.to_entity_class()
    text = patch.canonical_form if patch.canonical_form is not None else str(patch.entity)
    entity = entity_class(text)
    return entity


def default_entity_filter(patch: Patch) -> bool:
    if patch.approval is None or patch.approval == Approval.APPROVED:
        return True
    return False


def default_regest_filter(regest: Regest) -> bool:
    entity_classes: Set[Type[Entity]] = {entity.__class__ for entity in regest.entities}
    if len(regest.entities) <= 1 or len(entity_classes) > 1:
        return True
    return False


def _entity_to_letter(entity: Entity) -> str:
    letter, *_ = str(entity)
    letter = letter.upper()
    return letter


def _get_cell_value(cell: Cell) -> Optional[str]:
    if cell.value is None or not str(cell.value).strip():
        return None
    return str(cell.value)


class Patchset:
    CONFIG = _CONFIG['entity.Patchset']
    ROOT_PATH = Path(CONFIG['root_path'])
    MINIMUM_LENGTH = CONFIG.getint('minimum_length')
    MAXIMUM_LENGTH = CONFIG.getint('maximum_length')

    def __init__(self) -> None:
        self.patches: Dict[Entity, Patch] = dict()

    @classmethod
    def from_patches(cls, patches: Dict[Entity, Patch]) -> 'Patchset':
        patchset = cls()
        patchset.patches = dict(patches)
        return patchset

    @classmethod
    def from_patchset(cls, source_patchset: 'Patchset') -> 'Patchset':
        patchset = cls.from_patches(source_patchset.patches)
        return patchset

    @classmethod
    def from_file(cls, filename: str, entities: Iterable[Entity]) -> 'Patchset':
        entities = {str(entity): entity for entity in entities}
        letters = set(map(_entity_to_letter, entities.values()))

        patchset = cls()
        workbook = load_workbook(cls.ROOT_PATH / filename)
        for letter in letters:
            if letter not in workbook:
                continue
            worksheet = workbook[letter]
            rows = iter(worksheet.iter_rows())
            _ = next(rows)
            for row in rows:
                cell_values = map(_get_cell_value, row)
                entity_name, approval_symbol, entity_type_symbol, canonical_form, note, *_ = cell_values
                if entity_name not in entities:
                    continue
                entity = entities[entity_name]
                approval = Approval.from_symbol(approval_symbol)
                entity_type = EntityType.from_symbol(entity_type_symbol)
                patch = Patch(entity, approval, entity_type, canonical_form, note)
                patchset.patches[entity] = patch
        return patchset

    @classmethod
    def create_patchset_template(cls, filename: str, entities: Iterable[Entity],
                                 entities_to_regests: Dict[Entity, Set[Regest]]) -> None:

        entities = sorted(entities)

        # Create a plaintext file for power users
        with (cls.ROOT_PATH / filename).with_suffix('.txt').open('wt') as f:
            for entity in entities:
                print(entity, file=f)

        # Create a spreadsheet for standard users
        with Workbook((cls.ROOT_PATH / filename).with_suffix('.xlsx')) as workbook:

            # Declare formats
            default_format = workbook.add_format()

            bold_format = workbook.add_format({'bold': True})

            malformed_format = workbook.add_format({'bg_color': '#f4cccc'})
            well_formed_format = workbook.add_format({'bg_color': '#d9ead3'})

            disapproved_format = workbook.add_format({'bg_color': '#f4cccc'})
            approved_format = workbook.add_format({'bg_color': '#d9ead3'})
            too_short_or_long_format = workbook.add_format({'bg_color': '#fff2cc'})

            canonicalized_format = workbook.add_format({'bg_color': '#d9ead3'})

            # Sort entities by initial letters
            letters = defaultdict(lambda: list())
            for entity in entities:
                letter = _entity_to_letter(entity)
                letters[letter].append(entity)

            # Create worksheets for individual initial letters
            for letter, letter_entities in sorted(letters.items()):
                worksheet = workbook.add_worksheet(letter)

                # Set column width
                worksheet.set_column('A:A', 50)
                worksheet.set_column('B:B', 20)
                worksheet.set_column('C:C', 15)
                worksheet.set_column('D:D', 50)
                worksheet.set_column('E:E', 50)

                # Add headers
                worksheet.write('A1', 'Entita', bold_format)
                worksheet.write('B1', f'Použít? ({Approval.list_symbols()})', bold_format)
                worksheet.write_comment('B1', 'Uveďte, jestli si přejete entitu použít v '
                                        'trénovací datové sadě pro rozpoznávání jmenných '
                                        f'entit.\n\n{Approval.APPROVED}: Ano, přeji si entitu použít.\n\n'
                                        f'{Approval.DISAPPROVED}: Ne, přeji si entitu vynechat z datové sady.\n\n'
                                        f'{Approval.TOO_SHORT}: Entita je příliš krátká, pravděpodobně ji budeme '
                                        'chtít vynechat z datové sady, nebo ji budeme chtít '
                                        'přisoudit menší váhu.\n\n'
                                        f'{Approval.TOO_LONG}: Entita je příliš dlouhá, pravděpodobně ji budeme '
                                        'chtít vynechat z datové sady, nebo ji budeme chtít '
                                        'přisoudit menší váhu.')
                worksheet.write('C1', f'Typ entity ({EntityType.list_symbols()})', bold_format)
                worksheet.write_comment('C1', f'Uveďte typ entity:\n\n{EntityType.PERSON}: Osoba\n\n'
                                        f'{EntityType.PLACE}: Místo')
                worksheet.write('D1', 'Kanonická forma', bold_format)
                worksheet.write_comment('D1', 'Uveďte kanonickou formu entity, kterou '
                                        'budeme následně fulltextově hledat v dokumentech. '
                                        'Pokud lze kanonickou formu získat automatizovaně '
                                        '(např. odstraněním interpunkce zleva a zprava), '
                                        'použijte prosím poznámkový aparát.')
                worksheet.write('E1', 'Poznámky', bold_format)
                worksheet.write_comment('E1', 'Volitelně uveďte dodatečné informace ke své '
                                        'anotaci.')

                # List entities
                assert cls.MINIMUM_LENGTH <= cls.MAXIMUM_LENGTH
                for row_number, entity in enumerate(letter_entities):
                    row_number += 1
                    regest = sorted(entities_to_regests[entity])[0]

                    worksheet.write_url(row_number, 0, regest.document_url, string=str(entity))

                    if len(entity) < cls.MINIMUM_LENGTH:
                        worksheet.write(row_number, 1, str(Approval.TOO_SHORT))
                    elif len(entity) > cls.MAXIMUM_LENGTH:
                        worksheet.write(row_number, 1, str(Approval.TOO_LONG))

                # Add an autofilter, so that users can reorder the entities as they need
                worksheet.autofilter(0, 0, len(letter_entities), 4)

                # Set conditional formatting for approval
                blank_conditional_format = {'type': 'blanks', 'stop_if_true': True, 'format': default_format}
                disapproved_conditional_format = {'type': 'cell', 'criteria': '==',
                                                  'value': f'"{Approval.DISAPPROVED}"',
                                                  'format': disapproved_format}
                approved_conditional_format = {'type': 'cell', 'criteria': '==',
                                               'value': f'"{Approval.APPROVED}"',
                                               'format': approved_format}
                too_short_conditional_format = {'type': 'cell', 'criteria': '==',
                                                'value': f'"{Approval.TOO_SHORT}"',
                                                'format': too_short_or_long_format}
                too_long_conditional_format = {'type': 'cell', 'criteria': '==',
                                               'value': f'"{Approval.TOO_LONG}"',
                                               'format': too_short_or_long_format}

                approval_format_range = [1, 1, len(letter_entities), 1]

                worksheet.conditional_format(*approval_format_range, blank_conditional_format)
                worksheet.conditional_format(*approval_format_range, disapproved_conditional_format)
                worksheet.conditional_format(*approval_format_range, approved_conditional_format)
                worksheet.conditional_format(*approval_format_range, too_short_conditional_format)
                worksheet.conditional_format(*approval_format_range, too_long_conditional_format)

                # Set conditional formatting for entity types
                place_conditional_format = {'type': 'cell', 'criteria': '==',
                                            'value': f'"{EntityType.PLACE}"',
                                            'format': well_formed_format}
                person_conditional_format = {'type': 'cell', 'criteria': '==',
                                             'value': f'"{EntityType.PERSON}"',
                                             'format': well_formed_format}
                malformed_conditional_format = {
                    'type': 'formula',
                    'criteria': f'=AND($C2<>"{EntityType.PERSON}", $C2<>"{EntityType.PLACE}")',
                    'stop_if_true': True, 'format': malformed_format,
                }

                correct_entity_type_format_range = [1, 2, len(letter_entities), 2]

                worksheet.conditional_format(*correct_entity_type_format_range, blank_conditional_format)
                worksheet.conditional_format(*correct_entity_type_format_range, place_conditional_format)
                worksheet.conditional_format(*correct_entity_type_format_range, person_conditional_format)
                worksheet.conditional_format(*correct_entity_type_format_range, malformed_conditional_format)

                # Set conditional formatting for canonical form
                canonicalized_conditional_format = {'type': 'no_blanks', 'format': canonicalized_format}

                canonical_form_format_range = [1, 3, len(letter_entities), 3]

                worksheet.conditional_format(*canonical_form_format_range, blank_conditional_format)
                worksheet.conditional_format(*canonical_form_format_range, canonicalized_conditional_format)

    def get_canonical_form(self, entity: Entity) -> str:
        if entity not in self.patches:
            return str(entity)
        patch = self.patches[entity]
        if patch.canonical_form is None:
            return str(entity)
        return patch.canonical_form

    def apply(self, entities: Iterable[Entity], entity_map: EntityMap,
              entity_filter: EntityFilter = default_entity_filter) -> Dict[Entity, Entity]:
        patched_entities = dict()
        num_total, num_skipped, num_filtered_out, num_altered = 0, 0, 0, 0
        for entity in entities:
            num_total += 1
            if entity not in self.patches:
                num_skipped += 1
                continue
            patch = self.patches[entity]
            patched_entity = entity_map(patch)
            if entity_filter(patch):
                if patched_entity != entity:
                    num_altered += 1
                patched_entities[entity] = patched_entity
            else:
                num_filtered_out += 1
        message = (
            f'Skipped {num_skipped}, filtered out {num_filtered_out}, and altered {num_altered} '
            f'out of {num_total} entities'
        )
        LOGGER.info(message)
        return patched_entities


class PatchConfirmatory:
    def __init__(self, entity: Entity, approval: Optional[bool],
                 entity_type: Optional[EntityType]):
        self.entity = entity
        self.approval = approval
        self.entity_type = entity_type


PatchMap = Callable[[Patch, PatchConfirmatory], Optional[Patch]]


def default_patch_map(patch: Patch, patch_confirmatory: PatchConfirmatory) -> Optional[Patch]:
    assert patch.entity == patch_confirmatory.entity
    if patch_confirmatory.approval in (None, False):
        return None
    if patch_confirmatory.entity_type is not None:
        return Patch(patch.entity, patch.approval, patch_confirmatory.entity_type, patch.canonical_form, patch.note)
    else:
        return patch


class PatchsetConfirmatory:
    CONFIG = _CONFIG['entity.PatchsetConfirmatory']
    ROOT_PATH = Path(CONFIG['root_path'])

    def __init__(self, filename: str, patchset: Patchset, entities: Iterable[Entity]) -> None:
        self.patches: Dict[Entity, PatchConfirmatory] = dict()

        canonical_entities = defaultdict(lambda: set())
        for entity in entities:
            canonical_form = patchset.get_canonical_form(entity)
            canonical_entities[canonical_form].add(entity)

        workbook = load_workbook(self.ROOT_PATH / filename)
        worksheet = workbook['Entity']
        rows = iter(worksheet.iter_rows())
        _ = next(rows)
        for row in rows:
            cell_values = map(_get_cell_value, row)
            canonical_entity_name, is_person, is_place, disapprove, *_ = cell_values
            if canonical_entity_name not in canonical_entities:
                continue
            for entity in canonical_entities[canonical_entity_name]:

                def number_of_non_empty_values(cell_value: Optional[str]) -> int:
                    if cell_value is None:
                        number_of_non_empty_values = 0
                    else:
                        number_of_non_empty_values = 1
                    return number_of_non_empty_values

                num_checked_columns = sum(map(number_of_non_empty_values, (is_person, is_place, disapprove)))
                if num_checked_columns != 1:
                    approval = None
                    entity_type = None
                elif disapprove is not None:
                    assert disapprove in ('x', '?')
                    approval = False
                    entity_type = None
                else:
                    approval = True
                    if is_person is not None:
                        assert is_person in ('x', '?')
                        entity_type = EntityType.PERSON if is_person == 'x' else None
                    else:
                        assert is_place in ('x', '?')
                        entity_type = EntityType.PLACE if is_place == 'x' else None
                patch = PatchConfirmatory(entity, approval, entity_type)
                self.patches[entity] = patch

    @classmethod
    def create_confirmatory_template(cls, patchset: Patchset, filename: str,
                                     entities: Iterable[Entity],
                                     entities_to_regests: Dict[Entity, Set[Regest]],
                                     entity_filter: EntityFilter = default_entity_filter,
                                     regest_filter: RegestFilter = default_regest_filter) -> None:

        entities = sorted(entities, key=patchset.get_canonical_form)
        canonical_forms = set()

        with Workbook((cls.ROOT_PATH / filename).with_suffix('.xlsx')) as workbook:
            worksheet = workbook.add_worksheet('Entity')

            # Declare formats
            default_format = workbook.add_format()

            malformed_format = workbook.add_format({'bg_color': '#f4cccc'})
            certain_format = workbook.add_format({'bg_color': '#d9ead3'})
            uncertain_format = workbook.add_format({'bg_color': '#fce5cd'})

            certain_person_format = workbook.add_format({'bg_color': '#fff2cc'})
            uncertain_person_format = workbook.add_format({'bg_color': '#fff8e3'})
            certain_place_format = workbook.add_format({'bg_color': '#cfe2f3'})
            uncertain_place_format = workbook.add_format({'bg_color': '#e7f2fb'})
            certain_dont_know_format = workbook.add_format({'bg_color': '#ead1dc'})
            uncertain_dont_know_format = workbook.add_format({'bg_color': '#f2e3ea'})

            bold_format = workbook.add_format({'bold': True})
            bold_person_format = workbook.add_format({'bg_color': '#fff2cc', 'bold': True})
            bold_place_format = workbook.add_format({'bg_color': '#cfe2f3', 'bold': True})
            bold_dont_know_format = workbook.add_format({'bg_color': '#ead1dc', 'bold': True})

            # Set column width
            worksheet.set_column('A:A', 50)
            worksheet.set_column('B:B', 36)
            worksheet.set_column('C:C', 36)
            worksheet.set_column('D:D', 36)

            # Add headers
            worksheet.write('A1', 'Entita', bold_format)
            worksheet.write('B1', 'Osoba', bold_person_format)
            worksheet.write_comment('B1', 'Tento sloupec zatrhněte, pokud entita označuje osobu '
                                    '(např. „Filip III. Dobrý“).')
            worksheet.write('C1', 'Místo', bold_place_format)
            worksheet.write_comment('C1', 'Tento sloupec zatrhněte, pokud entita označuje místo '
                                    '(např. „Fürstenberg“).')
            worksheet.write('D1', 'Nelze snadno určit, raději vyřadíme', bold_dont_know_format)
            worksheet.write_comment('D1', 'Tento sloupec zatrhněte, pokud entita může označovat '
                                    'osobu tak místo (např. „Rožmberk“), neoznačuje ani osobu '
                                    'ani místo (např. „dominus“), nebo nedokážete určit, jestli '
                                    'označuje osobu, nebo místo.')

            row_number = 0
            for entity in entities:
                if entity not in patchset.patches:
                    continue

                patch = patchset.patches[entity]
                if not entity_filter(patch):
                    continue

                canonical_form = patchset.get_canonical_form(entity)
                if canonical_form in canonical_forms:
                    continue

                row_number += 1
                canonical_forms.add(canonical_form)

                regests = entities_to_regests[entity]
                regest, *_ = sorted(regests)
                worksheet.write_url(row_number, 0, regest.document_url, string=canonical_form)

                filtered_regests = filter(regest_filter, regests)
                filtered_regests = list(filtered_regests)
                original_entity_type = EntityType.from_entity(entity)
                patched_entity_type = patch.entity_type

                if not filtered_regests and original_entity_type == patched_entity_type:
                    certainty_symbol = '?'
                else:
                    certainty_symbol = 'x'

                if patch.entity_type is None:
                    raise ValueError('All patches in the patchset should have concrete entity types')
                elif patch.entity_type == EntityType.PERSON:
                    worksheet.write(row_number, 1, certainty_symbol)
                elif patch.entity_type == EntityType.PLACE:
                    worksheet.write(row_number, 2, certainty_symbol)

            # Add an autofilter, so that users can reorder the entities as they need
            worksheet.autofilter(0, 0, row_number, 3)

            # Set conditional formatting for checkboxes
            blank_conditional_format = {'type': 'blanks', 'stop_if_true': True, 'format': default_format}
            certain_conditional_format = {'type': 'cell', 'criteria': '==',
                                          'value': '"x"', 'format': certain_format}
            uncertain_conditional_format = {'type': 'cell', 'criteria': '==',
                                            'value': '"?"', 'format': uncertain_format}
            malformed_conditional_format = {'type': 'formula', 'criteria': '=AND(B2<>"x", B2<>"?")',
                                            'format': malformed_format}

            checkbox_format_range = [1, 1, row_number, 3]

            worksheet.conditional_format(*checkbox_format_range, blank_conditional_format)
            worksheet.conditional_format(*checkbox_format_range, certain_conditional_format)
            worksheet.conditional_format(*checkbox_format_range, uncertain_conditional_format)
            worksheet.conditional_format(*checkbox_format_range, malformed_conditional_format)

            # Set conditional formatting for entities
            certain_person_conditional_format = {'type': 'formula', 'criteria': '=$B2="x"',
                                                 'format': certain_person_format}
            uncertain_person_conditional_format = {'type': 'formula', 'criteria': '=$B2="?"',
                                                   'format': uncertain_person_format}
            certain_place_conditional_format = {'type': 'formula', 'criteria': '=$C2="x"',
                                                'format': certain_place_format}
            uncertain_place_conditional_format = {'type': 'formula', 'criteria': '=$C2="?"',
                                                  'format': uncertain_place_format}
            certain_dont_know_conditional_format = {'type': 'formula', 'criteria': '=$D2="x"',
                                                    'format': certain_dont_know_format}
            uncertain_dont_know_conditional_format = {'type': 'formula', 'criteria': '=$D2="?"',
                                                      'format': uncertain_dont_know_format}

            entity_format_range = [1, 0, row_number, 0]

            worksheet.conditional_format(*entity_format_range, certain_person_conditional_format)
            worksheet.conditional_format(*entity_format_range, uncertain_person_conditional_format)
            worksheet.conditional_format(*entity_format_range, certain_place_conditional_format)
            worksheet.conditional_format(*entity_format_range, uncertain_place_conditional_format)
            worksheet.conditional_format(*entity_format_range, certain_dont_know_conditional_format)
            worksheet.conditional_format(*entity_format_range, uncertain_dont_know_conditional_format)

    def apply(self, source_patchset: Patchset, entities: Iterable[Entity],
              patch_map: PatchMap = default_patch_map) -> Patchset:
        source_patches = source_patchset.patches
        patches = dict()
        num_total, num_skipped, num_filtered_out, num_altered = 0, 0, 0, 0
        for entity in entities:
            num_total += 1
            if entity not in self.patches:
                num_skipped += 1
                continue
            source_patch = source_patches[entity]
            patch_confirmatory = self.patches[entity]
            patch = patch_map(source_patch, patch_confirmatory)
            if patch is not None:
                if source_patch != patch:
                    num_altered += 1
                patches[entity] = patch
            else:
                num_filtered_out += 1
        message = (
            f'Skipped {num_skipped}, filtered out {num_filtered_out}, and altered {num_altered} '
            f'out of {num_total} patches'
        )
        patchset = Patchset.from_patches(patches)
        LOGGER.info(message)
        return patchset
