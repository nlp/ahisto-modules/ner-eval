FROM pytorch/pytorch:1.11.0-cuda11.3-cudnn8-runtime
RUN apt -qy update
RUN apt -qy --no-install-recommends install git
COPY setup.py requirements.txt MANIFEST.in /ahisto_named_entity_search/
COPY ahisto_named_entity_search /ahisto_named_entity_search/ahisto_named_entity_search
WORKDIR /ahisto_named_entity_search
RUN pip install . datasets
RUN transformers-cli download MU-NLPC/ahisto-ner-model-s
COPY scripts /ahisto_named_entity_search/scripts
ENTRYPOINT ["ahisto-ner"]
