from unittest import TestCase

from ahisto_named_entity_search.search.result import TaggedSentence
from ahisto_named_entity_search.search.evaluation import NerEvaluationResult


class TestNerEvaluationResult(TestCase):
    def setUp(self):
        ground_truth = TaggedSentence(
            'Jan   Žižka z Trocnova vyhlásil nepřátelství městu České Budějovice',
            'B-PER I-PER O B-LOC    O        O            O     B-LOC I-LOC',
        )
        self.ground_truth = ground_truth
        self.perfect_match = ground_truth
        self.no_predictions = TaggedSentence(ground_truth.sentence, 'O O O O O O O O O')
        self.misaligned_boundaries = TaggedSentence(
            ground_truth.sentence, 'B-PER B-PER B-LOC I-LOC O O B-LOC I-LOC O')
        self.first_mostly_complete_match = TaggedSentence(
            ground_truth.sentence, 'O O O B-LOC O O O B-LOC I-LOC')
        self.second_mostly_complete_match = TaggedSentence(
            ground_truth.sentence, 'B-PER I-PER O O O O O B-LOC I-LOC')
        self.third_mostly_complete_match = TaggedSentence(
            ground_truth.sentence, 'B-PER I-PER O B-LOC O O O O O')
        self.first_mostly_incomplete_match = TaggedSentence(
            ground_truth.sentence, 'O O O O O O O B-LOC I-LOC')
        self.second_mostly_incomplete_match = TaggedSentence(
            ground_truth.sentence, 'B-PER I-PER O O O O O O O')
        self.third_mostly_incomplete_match = TaggedSentence(
            ground_truth.sentence, 'O O O B-LOC O O O O O')
        self.overcomplete_match = TaggedSentence(
            ground_truth.sentence, 'B-PER I-PER B-PER B-LOC O B-PER O B-LOC I-LOC')
        self.mistyped_match = TaggedSentence(
            ground_truth.sentence, 'B-LOC I-LOC O B-PER O O O B-PER I-PER')

    def test_strict_precision(self):

        def get_precision(prediction: TaggedSentence):
            result = NerEvaluationResult.from_tagged_sentences([self.ground_truth], [prediction])
            return result.strict_result_for_precision.precision

        assert get_precision(self.perfect_match) == 1.0
        assert get_precision(self.no_predictions) == 1.0
        assert get_precision(self.misaligned_boundaries) == 0.0
        assert get_precision(self.first_mostly_complete_match) == 1.0
        assert get_precision(self.second_mostly_complete_match) == 1.0
        assert get_precision(self.third_mostly_complete_match) == 1.0
        assert get_precision(self.first_mostly_incomplete_match) == 1.0
        assert get_precision(self.second_mostly_incomplete_match) == 1.0
        assert get_precision(self.third_mostly_incomplete_match) == 1.0
        assert get_precision(self.overcomplete_match) == 3 / 5
        assert get_precision(self.mistyped_match) == 0.0

    def test_fuzzy_precision(self):

        def get_precision(prediction: TaggedSentence):
            result = NerEvaluationResult.from_tagged_sentences([self.ground_truth], [prediction])
            return result.fuzzy_result_for_precision.precision

        assert get_precision(self.perfect_match) == 1.0
        assert get_precision(self.no_predictions) == 1.0
        assert get_precision(self.misaligned_boundaries) == 1.0
        assert get_precision(self.first_mostly_complete_match) == 1.0
        assert get_precision(self.second_mostly_complete_match) == 1.0
        assert get_precision(self.third_mostly_complete_match) == 1.0
        assert get_precision(self.first_mostly_incomplete_match) == 1.0
        assert get_precision(self.second_mostly_incomplete_match) == 1.0
        assert get_precision(self.third_mostly_incomplete_match) == 1.0
        assert get_precision(self.overcomplete_match) == 3 / 5
        assert get_precision(self.mistyped_match) == 0.0

    def test_strict_recall(self):

        def get_recall(prediction: TaggedSentence):
            result = NerEvaluationResult.from_tagged_sentences([self.ground_truth], [prediction])
            return result.strict_result_for_recall.recall

        assert get_recall(self.perfect_match) == 1.0
        assert get_recall(self.no_predictions) == 0.0
        assert get_recall(self.misaligned_boundaries) == 0.0
        assert get_recall(self.first_mostly_complete_match) == 2 / 3
        assert get_recall(self.second_mostly_complete_match) == 2 / 3
        assert get_recall(self.third_mostly_complete_match) == 2 / 3
        assert get_recall(self.first_mostly_incomplete_match) == 1 / 3
        assert get_recall(self.second_mostly_incomplete_match) == 1 / 3
        assert get_recall(self.third_mostly_incomplete_match) == 1 / 3
        assert get_recall(self.overcomplete_match) == 1.0
        assert get_recall(self.mistyped_match) == 0.0

    def test_fuzzy_recall(self):

        def get_recall(prediction: TaggedSentence):
            result = NerEvaluationResult.from_tagged_sentences([self.ground_truth], [prediction])
            return result.fuzzy_result_for_recall.recall

        assert get_recall(self.perfect_match) == 1.0
        assert get_recall(self.no_predictions) == 0.0
        assert get_recall(self.misaligned_boundaries) == 1.0
        assert get_recall(self.first_mostly_complete_match) == 2 / 3
        assert get_recall(self.second_mostly_complete_match) == 2 / 3
        assert get_recall(self.third_mostly_complete_match) == 2 / 3
        assert get_recall(self.first_mostly_incomplete_match) == 1 / 3
        assert get_recall(self.second_mostly_incomplete_match) == 1 / 3
        assert get_recall(self.third_mostly_incomplete_match) == 1 / 3
        assert get_recall(self.overcomplete_match) == 1.0
        assert get_recall(self.mistyped_match) == 0.0
