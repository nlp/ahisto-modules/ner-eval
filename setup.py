#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pip._internal.req import parse_requirements
from setuptools import setup, find_packages


setup(
    name='ahisto-named-entity-search',
    version='1.0.1',
    author='Vít Novotný',
    author_email='witiko@mail.muni.cz',
    url='https://gitlab.fi.muni.cz/nlp/ahisto-modules/named-entity-search',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=True,
    entry_points={
        'console_scripts': [
            'ahisto-ner=ahisto_named_entity_search.cli:cli',
        ],
    },
    setup_requires=[
        'pip',
        'setuptools',
    ],
    install_requires=[
        str(r.requirement)
        for r
        in parse_requirements('requirements.txt', session='workaround')
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.8',
    ],
)
