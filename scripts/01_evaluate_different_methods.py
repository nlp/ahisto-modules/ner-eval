#!/usr/bin/env python
# coding: utf-8

import sys

assert len(sys.argv) == 2
hostname = sys.argv[1]

# # AHISTO Named Entity Search
# 
# Log some information about the current machine.

# In[1]:


# get_ipython().system(' hostname')


# In[2]:


# get_ipython().system(' python -V')


# Install the current version of the package and its dependencies.

# In[3]:


# get_ipython().run_cell_magic('capture', '', '! pip install .')


# Make sure numpy does not parallelize.

# In[4]:


import os


# In[5]:


os.environ["OMP_NUM_THREADS"] = "1"
os.environ["OPENBLAS_NUM_THREADS"] = "1"
os.environ["MKL_NUM_THREADS"] = "1"
os.environ["VECLIB_MAXIMUM_THREADS"] = "1"
os.environ["NUMEXPR_NUM_THREADS"] = "1"


# Pick the GPU that we will use.

# In[6]:


# get_ipython().system(' nvidia-smi -L')


# In[7]:


os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "16"


# Set up logging to display informational messages.

# In[8]:


import logging
import sys


# In[9]:


logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(message)s')


# ## Load documents and entities

# In[10]:


from ahisto_named_entity_search.entity import Entity, Person, Place, load_entities


# In[11]:


all_entities = load_entities()


# In[12]:


from ahisto_named_entity_search.document import Document, load_documents


# In[13]:


documents = load_documents()


# ## Take a subset of entities for evaluation

# First, we will find the shortest and longest entities.
# ``` python
# >>> def entity_length(entity): return len(str(entity))
# ```
# 
# We need to filter out too short entities, because they often contain only connectives, punctiation, numerals, etc.:
# ``` python
# >>> sorted(all_entities, key=entity_length)[:58][-3:]
# [Person: a, Person: (, Place: a, Person: hy, Person: XL]
# 
# >>> sorted(all_entities, key=entity_length)[:58][-3:]
# [Person: Iva, Place: Háj, Person: Ota]
# ```

# In[14]:


shortest_entities = [
    Person('Iva'),
    Place('Háj'),
    Person('Ota'),
]


# We also need to filter out too long entities, because they are often composed of several entities:
# 
# ```python
# >>> sorted(all_entities, key=entity_length)[-1]
# Person: Coln, Ach, Meincz, Worms, Straßburg, Basel, Hagnaw und den andern stetten in Ellseßen, Zurich, Luczern, Solottern, Mulhawsen, Northawsen, Frankfurt, Geylnhawsen, Fridberg, Winsheim, Sweinfurt, Ulme und die mit in in einung sein, Costencz und die mit in in einung sein, Freyburg in Uechtland, Freyburg in Preisgew, Preisach, Newemburg, Augspurg, Regenspurg, Eger, Heylprunnen, Wimpfen, Erffurd.
# 
# >>> sorted(all_entities, key=entity_length)[-146:][:3]
# [Person: Fridrichen marggrafen zu Brandemburg des heiligen Romischen reichs ercamrer und burggrafen zu Nuremberg,
#  Person: paní Kláře Zalcarové, měštce v Olomúci i její erbom i tomu, kdož by tento list měl s jejich dobrú vuolí,
#  Person: Markéta, vdova po Buškovi z Rýzmberku, Janem a Bohuslavem, jejími syny z Blažimi sezením na hradě Bubnu]
# ```

# In[15]:


longest_entities = [
    Person('Fridrichen marggrafen zu Brandemburg des heiligen Romischen reichs ercamrer und burggrafen zu Nuremberg'),
    Person('paní Kláře Zalcarové, měštce v Olomúci i její erbom i tomu, kdož by tento list měl s jejich dobrú vuolí'),
    Person('Markéta, vdova po Buškovi z Rýzmberku, Janem a Bohuslavem, jejími syny z Blažimi sezením na hradě Bubnu'),
]


# Next, we will find entities in different single languages:
# ``` python
# >>> ! pip install langdetect
# >>> from langdetect import detect_langs, LangDetectException
# >>> from collections import defaultdict
# >>> import random
# >>> entities_languages = defaultdict(lambda: list())
# >>> for entity in random.sample(all_entities, k=len(all_entities)):
# ...     try:
# ...         best_lang, *other_langs = detect_langs(str(entity))
# ...         if len(other_langs) == 0:
# ...             entities_languages[best_lang.lang].append(entity)
# ...     except LangDetectException:
# ...         continue
# 
# >>> entities_languages['de'][:3]
# [Person: Bischoff zu Wirtzpurg,
#  Place: in Trebicz,
#  Person: Weissenburg in Bayern]  # Notice the wrong designation as a Person instead of a Place.
# 
# >>> entities_languages['cs'][:3]
# [Place: kromě rybníka v velikého v Slatinie,
#  Person: Václava Králíka z Buřenic, tehdy správce olomouckého kostela a antiochijského patriarchy,
#  Person: Břeňka z Drštky]
# 
# >>> entities_languages['it'][:3]
# [Person: Imperatori Sigismundo,
#  Person: Wladislaus dei gratia rex Polonie et cetera,
#  Person: Brandou da Castiglione]
# ```

# In[16]:


german_entities = [
    Person('Bischoff zu Wirtzpurg'),
    Place('in Trebicz'),
    Person('Weissenburg in Bayern'),
]


# In[17]:


czech_entities = [
    Place('kromě rybníka v velikého v Slatinie'),
    Person('Václava Králíka z Buřenic, tehdy správce olomouckého kostela a antiochijského patriarchy'),
    Person('Břeňka z Drštky'),
]


# In[18]:


latin_entities = [
    Person('Imperatori Sigismundo'),
    Person('Wladislaus dei gratia rex Polonie et cetera'),
    Person('Brandou da Castiglione'),
]


# Next, we will find entities of different types:
# ``` python
# >>> import random
# >>> places = [entity for entity in sorted(all_entities) if isinstance(entity, Place)]
# >>> persons = [entity for entity in sorted(all_entities) if isinstance(entity, Person)]
# >>> random.choices(places, k=3)
# [
#     'Kutná Hora',
#     'pražského kostela',
#     'Těšeticích',
# ]
# >>> random.choices(persons, k=3)
# [
#     'Aleš z Vrahovic',
#     'králem Zikmundem',
#     'husité',
# ]
# ```

# In[19]:


place_entities = [
    Place('Kutná Hora'),
    Place('pražského kostela'),
    Place('Těšeticích'),
]


# In[20]:


person_entities = [
    Person('Aleš z Vrahovic'),
    Person('králem Zikmundem'),
    Person('husité'),
]


# We will combine the sampled entities into a single list:

# In[21]:


entities = shortest_entities + longest_entities + german_entities + czech_entities + latin_entities + place_entities + person_entities


# In[22]:


for entity in entities:
    assert entity in all_entities, entity


# In[23]:


print('We sampled {} entities.'.format(len(entities)))


# ## Compute and save search results
# 
# After we have selected a subset of entities, we will search for the entities in OCR texts. We will try a number of methods, some of which will be indexpensive and will serve to select candidates for expensive methods that will produce the final results. To enable later analysis, we will save the results of all methods.

# In[24]:


from json import JSONDecodeError


# In[25]:


from ahisto_named_entity_search.search import Search
from ahisto_named_entity_search.search import SearchResultList


# ### Inexpensive Methods
# 
# First, we will use a number of inexpensive and less accurate methods to select candidate results for the more expensive and accurate methods.

# #### Jaccard Similarity
# 
# First, we will slide a window across the OCR texts and we will exhaustively compute the Jaccard index between each entity and each window. Due to the low number of OCR texts and the constant time complexity of the Jaccard index, this is computationally feasible. This method accurately detects the position of an entity in a text, but the precision of the Jaccard index on the semantic text similarity task is low.

# In[26]:


from ahisto_named_entity_search.index import CharacterJaccardSimilarityIndex


# In[27]:

if hostname == 'aura.fi.muni.cz':
    try:
        jaccard_similarity_character_results = SearchResultList.load('character-jaccard-similarity', entities)
    except (IOError, JSONDecodeError):
        jaccard_similarity_character_results = Search(CharacterJaccardSimilarityIndex(documents.values())).search(entities)
        jaccard_similarity_character_results.save('character-jaccard-similarity')
    print(jaccard_similarity_character_results)


# In[28]:


from ahisto_named_entity_search.index import WordJaccardSimilarityIndex


# In[29]:

if hostname == 'aura.fi.muni.cz':
    try:
        jaccard_similarity_word_results = SearchResultList.load('word-jaccard-similarity', entities)
    except (IOError, JSONDecodeError):
        jaccard_similarity_word_results = Search(WordJaccardSimilarityIndex(documents.values())).search(entities)
        jaccard_similarity_word_results.save('word-jaccard-similarity')
    print(jaccard_similarity_word_results)


# #### Okapi BM25
# 
# Next, we will again slide a window across the OCR text and we will extract and index passages using an Okapi BM25 vector space model. Due to the low number of OCR texts and the constant time complexity of vector space models, this is computationally feasible. This method accurately detects the position of an entity in a text, but Okapi BM25 requires tokenization into words, which results in poor precision on OCR texts.

# In[30]:


from ahisto_named_entity_search.index import OkapiBM25Index


# In[31]:


if hostname == 'aura.fi.muni.cz':
    try:
        okapi_bm25_results = SearchResultList.load('okapi-bm25', entities)
    except (IOError, JSONDecodeError):
        okapi_bm25_results = Search(OkapiBM25Index(documents.values())).search(entities)
        okapi_bm25_results.save('okapi-bm25')
    print(okapi_bm25_results)


# #### Fuzzy Regexes
# 
# Finally, we will try to find exact and almost-exact matches of the entities in the OCR texts using [fuzzy regexes][1]. Fuzzy regexes offer high precision, but low recall, since only exact and almost-exact character-level matches will be found. Unlike the Jaccard index, fuzzy regexes don't use a sliding window: a properly-sized window with a match is automatically found in a full document. Almost-exact matches can be produced by allowing a small number of errors; a allowing a larger number of errors is not computationally feasible, because the time complexity is quadratic in the number of errors. To ensure that almost-exact matches can be found even for long entities, we divide them into shorter spans of text and search for them separately.
# 
#  [1]: https://pypi.org/project/regex/#approximate-fuzzy-matching-hg-issue-12-hg-issue-41-hg-issue-109

# In[32]:


from ahisto_named_entity_search.index import FuzzyRegexIndex


# In[33]:


if hostname == 'aura.fi.muni.cz':
    try:
        fuzzy_regex_results = SearchResultList.load('fuzzy-regex', entities)
    except (IOError, JSONDecodeError):
        fuzzy_regex_results = Search(FuzzyRegexIndex(documents.values())).search(entities)
        fuzzy_regex_results.save('fuzzy-regex')
    print(fuzzy_regex_results)


# ### Expensive Methods
# 
# Next, we will use the results produced by the inexpensive and less accurate methods as candidate results to be reranked by the expensive and accurate methods:

# In[34]:

from time import sleep

def wait_for_results(basename: str) -> SearchResultList:
    printed_message = False
    results = None
    while results is None:
        try:
            results = SearchResultList.load(basename, entities)
        except (IOError, JSONDecodeError):
            if not printed_message:
                printed_message = True
                print('Waiting for results of {}'.format(basename))
            sleep(60)
    return results

if hostname == 'apollo.fi.muni.cz':
    jaccard_similarity_character_results = wait_for_results('character-jaccard-similarity')
    jaccard_similarity_word_results = wait_for_results('word-jaccard-similarity')
    okapi_bm25_results = wait_for_results('okapi-bm25')
    fuzzy_regex_results = wait_for_results('fuzzy-regex')
else:
    sys.exit(0)


inexpensive_candidates = [
    jaccard_similarity_character_results,
    jaccard_similarity_word_results,
    fuzzy_regex_results,
    okapi_bm25_results,
]


# #### Edit Distance
# 
# First, we rerank the candidate results using the character and word error rate. This is necessary, because the edit distance has quadratic time complexity in the text size, which makes it extremely *expensive*. If at least some of the candidate results are representative, the edit distance offers moderate precision on the semantic text similarity task.

# In[35]:


from ahisto_named_entity_search.index import CharacterEditSimilarityIndex


# In[36]:


try:
    edit_similarity_character_results = SearchResultList.load('character-edit-similarity', entities)
except (IOError, JSONDecodeError):
    edit_similarity_character_results = Search(CharacterEditSimilarityIndex(inexpensive_candidates)).search(entities)
    edit_similarity_character_results.save('character-edit-similarity')
print(edit_similarity_character_results)


# In[37]:


from ahisto_named_entity_search.index import WordEditSimilarityIndex


# In[38]:


try:
    edit_similarity_word_results = SearchResultList.load('word-edit-similarity', entities)
except (IOError, JSONDecodeError):
    edit_similarity_word_results = Search(WordEditSimilarityIndex(inexpensive_candidates)).search(entities)
    edit_similarity_word_results.save('word-edit-similarity')
print(edit_similarity_word_results)


# #### BERTScore
# 
# Next, we will also rerank the candidate results using the [BERT F-Score][1]. Although originally designed for neural machine translation evaluation, the BERT F-Score is a symmetric similarity measure. Unlike the edit distance, which still measures mostly syntactic similarity, the BERT F-Score offers excellent precision on the semantic text similarity task.
# 
#  [1]: https://arxiv.org/abs/1904.09675

# In[39]:


from ahisto_named_entity_search.index import BERTScoreIndex


# In[40]:


try:
    bert_score_results = SearchResultList.load('bert-score', entities)
except (IOError, JSONDecodeError):
    import transformers
    transformers.logging.set_verbosity_error()
    bert_score_results = Search(BERTScoreIndex(inexpensive_candidates), num_workers=6).search(entities)
    bert_score_results.save('bert-score')
print(bert_score_results)


# #### SentenceBERT

# Finally, we will also rerank the candidate results using the cosine similarity between [siamese BERT][1] embeddings. Like the BERT F-Score, the siamese BERT embeddings offer excellent precision on the semantic text similarity task.
# 
#  [1]: https://arxiv.org/abs/1908.10084

# In[41]:


from ahisto_named_entity_search.index import SentenceBERTSimilarityIndex


# In[42]:


try:
    sentence_bert_similarity_results = SearchResultList.load('sbert-similarity', entities)
except (IOError, JSONDecodeError):
    logging.getLogger('sentence_transformers.SentenceTransformer').setLevel(logging.WARNING)
    sentence_bert_similarity_results = Search(SentenceBERTSimilarityIndex(inexpensive_candidates), num_workers=6).search(entities)
    sentence_bert_similarity_results.save('sbert-similarity')
print(sentence_bert_similarity_results)


# ### Rank Fusion

# Finally, we will use rank fusion to combine the results of all the above methods.

# In[43]:

expensive_candidates = [
    edit_similarity_character_results,
    edit_similarity_word_results,
    bert_score_results,
    sentence_bert_similarity_results,
]

# In[44]:

candidates = inexpensive_candidates + expensive_candidates

# In[45]:

from ahisto_named_entity_search.index import ReciprocalRankFusionIndex

# In[46]:

try:
    reciprocal_rank_fusion_results = SearchResultList.load('reciprocal-rank-fusion', entities)
except (IOError, JSONDecodeError):
    reciprocal_rank_fusion_results = Search(ReciprocalRankFusionIndex(candidates)).search(entities)
    reciprocal_rank_fusion_results.save('reciprocal-rank-fusion')
print(reciprocal_rank_fusion_results)

# In[45]:

from ahisto_named_entity_search.index import ConcatenatedIndex

# In[46]:

try:
    concatenated_index_results = SearchResultList.load('fuzzy-regex-and-reciprocal-rank-fusion', entities)
except (IOError, JSONDecodeError):
    concatenated_index_results = Search(ConcatenatedIndex([fuzzy_regex_results, reciprocal_rank_fusion_results])).search(entities)
    concatenated_index_results.save('fuzzy-regex-and-reciprocal-rank-fusion')
print(concatenated_index_results)

# In[46]:

all_results = candidates + [reciprocal_rank_fusion_results, concatenated_index_results]

# In[46]:

from ahisto_named_entity_search.search import Annotations

# In[46]:

Annotations.create_annotation_template('annotation-template.xlsx', entities, all_entities, all_results)
