from itertools import repeat, starmap
from regex import escape, fullmatch, match, split, sub, IGNORECASE
from sys import stdin, stdout, argv
from typing import Dict


Entity = str
EntityType = str
Regex = str


def starfilter(fn, iterable):
    return filter(lambda x: fn(*x), iterable)


def text_to_regex(text: str, separator: str = r'\s*[,/]\s*') -> Regex:
    items = (r'\b'+escape(item)+(r'\b' if item[-1].isalpha() else '') for item in split(separator, text))
    regex = '|'.join(sorted(items, key=len, reverse=True))
    regex = f'({regex})'
    return regex


def regexes_to_regex(regexes: str, separator: str = r'\s*\n\s*') -> Regex:
    items = split(separator, regexes)
    items = (sub(r"\s+", r"\s+", item) for item in items)
    items = (sub(r"(\([^()]*\))\?\\s\+", r"(?:\1\s+)?", item) for item in items)
    items = (sub(r"\\s\+(\([^()]*\))\?", r"(?:\s+\1)?", item) for item in items)
    items = (sub(r"\.", r"\\.", item) for item in items)
    regex = '|'.join(f'({item})' for item in items)
    regex = f'({regex})'
    return regex


CLUTTER = r'[\p{Punctuation}\p{Separator}\p{Other}]*'


PREFIXES = """
auf, aus, aus der, ausz, bei, bey, bey dem, bey der, by,
civitatem/civitatis/civitati/civitate/civium, czu/czum/czur,
d., das, de, dem, den, der, des, die, di, do, dye, dy, dicti,
famosi/famoso/famosorum/famos./famosus/famosum,
für, fur, gein, gen, gen dem, gen der,
honesta/honeste/honesti/honestorum,
honorab./honorabil./honorabilem/honorab. d./honorabil. d.,
in, im, in dem, in der, in di, in die, na,
nobil, nobilem, nobilis, nobil., nobil. d., nobil d., nobil. dd.,
nobil. dom, nobilem d., nobili, nobilis, nobilis d., nobilis. d.,
nobilis dominus, nobilibus, nobilium, nobili, nobilum,
quondam, strennui, strenuum, strenuo, strenuus,
umb, urozeného, urozené, urozený, urozenému, u, v,
vf der, vf dem, vmb, von, von dem, von der, vor, vor dem,
vor das, vor den, vor die, zcu, z, ze, zu, zu dem, zu der,
zur, zum
""".strip()
PREFIXES = text_to_regex(PREFIXES)

ELLIPSIS = r'(\.{3,}|…)'

QUESTION_MARK = r'\?'

ALLOWED_SHORT_ENTITIES: Dict[EntityType, Regex] = {
    'places': text_to_regex('Řím, Týn, Ulm'),
    'persons': text_to_regex('Iva, Jan, Lev, Ota/Otu/Oty, Vít'),
}

# ROMAN_NUMBER = r'(?<![MDCLXVI])(?=[MDCLXVI])M{0,3}(?:C[MD]|D?C{0,3})(?:X[CL]|L?X{0,3})(?:I[XV]|V?I{0,3})[^ ]\b'
ROMAN_NUMBER = r'\b[MDCLXVI]+\b'
NONEXISTANT_CHAR = '¤'
OTHER_NUMBER = r'\p{Number}'
NUMBER = f'({OTHER_NUMBER}|{ROMAN_NUMBER})'
LOWERCASE_LETTER = r'\p{Lowercase_Letter}'
UPPERCASE_LETTER = r'\p{Uppercase_Letter}'
SAINT = r'sv?\.'
INCORRECT_PREFIX = f'^({LOWERCASE_LETTER}|{NUMBER})'
CORRECT_PREFIX = r"""
(abatyše)
(\w+)? (biskup|biskupa|biskupem)
(bratr|bratra|bratru|bratry|bratři)
(bruder)
(\w+)? (klášter|kláštera) (v|na)?
(closter|conuentus|conventum|conventus|conventui)
(curia|curiam) (in )?
(císař|císaře|císařem)
(castri|castro|castrum|catro)
(český|českého|českým)? (král|krále|králem|králi)
(klaster|klastr|klostr)
(herzog|herczok|herczog|herczoge|herczogen|herczogens|herczogs|herczugen|herczug|hertzog|hertzoge|hertzogen|herzog|herzoge|herzogen)
(her|herr|hern|herre|herren|herrn)
(město|města)
(dům|domu)
(\w+)? (kostel|kostela|kostele|kostelem|kostelu) (v|na)?
(frater|fratres|fratrem|fratri|fratris|fri|fri.|fris|fris.)
(graf|graff)
(domina|domine|domini|domino|dominum)
(hrad|hradě|hradu) (v|ve|na)?
(regno|regnum)
(villa|villam|villis)
(kaple|kapli) (v|na)
(kněz|kněze|knězi|knězem|kněžnou|knížete|knížectví)
(\w+)? (kníže|knížete|knížetem)
(\w+)? (kanovník|kanovníka|kanovníkem)
(marggraf|marggrafe|marggraue|marggrauen|marggraven)
(meister|mistr|mistra|mistrem|mistru)
(magister|magistri|magistro|magistrum)
(mon.|monst.|montis|monasterii|monasteriorum|monasterie|monasteria|monasterium) (in)?
(\w+)? (opat|opata|opatu)
(otec|otce)
(opido|opidum)
(pan|pán|pana|páni|panem|paní|panu|pány|pánům) (z|ze|na)?
(papež|papeže|papežem|papeži)
(převor|převora|převorem|převoru)
(regni|regem|refni|rege|regis|regnum)
(rektor|rektora|rektorem)
(vévoda|vévody|vévodovi)
""".strip()
CORRECT_PREFIX = regexes_to_regex(CORRECT_PREFIX)
CORRECT_PREFIX = f'^{CORRECT_PREFIX}\\s+({SAINT}|{UPPERCASE_LETTER})'


def map_entity(entity: Entity, entity_type: EntityType) -> Entity:
    # 0. zachovat tečku za římským číslem
    assert '@' not in entity
    entity = sub(f'({ROMAN_NUMBER})\\.', fr'\1{NONEXISTANT_CHAR}', entity)

    # 1. odmazat interpunkci a prázdné znaky ze začátku a konce entity
    entity = sub(f'(^{CLUTTER}|{CLUTTER}$)', '', entity)

    # 2. smazat ze začátku entity předložky apod, mohou začínat malým
    # i velkým písmenem
    entity = sub(f'^{PREFIXES}{CLUTTER}', '', entity, flags=IGNORECASE)

    # 3. pokud entita obsahuje "," nebo ";"
    # smazat část od této interpunkce dál
    entity = sub(f'{CLUTTER}[,;].*$', '', entity)

    # 4. pokud entita obsahuje "..." (a více), "?", nebo "…"
    # smazat tuto interpunkci
    entity = sub(f'{CLUTTER}({ELLIPSIS}|{QUESTION_MARK}){CLUTTER}', ' ', entity)

    # ad 0. vrátit tečku za římským číslem
    entity = sub(f'({ROMAN_NUMBER}){NONEXISTANT_CHAR}', r'\1.', entity)

    return entity


def filter_entity(entity: Entity, entity_type: EntityType) -> bool:
    # 6. nepoužít entity, které mají 3 a méně znaků kromě [...]
    allowed_short_entities = ALLOWED_SHORT_ENTITIES[entity_type]
    if len(entity) <= 3 and not fullmatch(allowed_short_entities, entity):
        return False

    # 7. pokud potom entita začíná na slovo s počátečním malým písmenem,
    # číslici nebo na I, II, III, IIII, tak entitu nepoužít, kromě začátků, za
    # kterými musí následovat velké písmeno nebo s./sv.: [...]
    if match(INCORRECT_PREFIX, entity) and not match(CORRECT_PREFIX, entity):
        return False

    return True


def main(entity_type: EntityType) -> None:
    entities = (line.rstrip('\r\n') for line in stdin)
    entities = starmap(map_entity, zip(entities, repeat(entity_type)))
    entities = starfilter(filter_entity, zip(entities, repeat(entity_type)))
    for entity, _ in entities:
        print(entity, file=stdout)


if __name__ == '__main__':
    entity_type = argv[1]
    assert entity_type in {'places', 'persons'}
    main(entity_type)
