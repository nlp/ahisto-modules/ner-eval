#!/bin/bash
# Builds a Docker image with the Python package and uses it to train NER models at apollo.fi.muni.cz

set -e -o xtrace

HOSTNAME=docker.apollo.fi.muni.cz
IMAGE_NAME=ahisto/named-entity-search:latest
ROOT_PATH=/nlp/projekty/ahisto/public_html/named-entity-search/results/
ANNOTATION_PATH=/nlp/projekty/ahisto/annotations/
OCR_EVAL_PATH=/nlp/projekty/ahisto/ahisto-ocr-eval
NUMBER_OF_JOBS=6
GIT_COMMIT="$(git rev-parse --short HEAD)"
GPU_ID='$(nvidia-smi | grep -B 1 -- " [0-9]MiB / 15360MiB" | tail -n 2 | head -n 1 | awk "{ print \$2 }")'

DOCKER_BUILDKIT=1 docker build --build-arg UID="$(id -u)" --build-arg GID="$(id -g)" --build-arg UNAME="$(id -u -n)" . -f scripts/05_train_additional_ner_models_for_paper.Dockerfile -t "$IMAGE_NAME"

parallel --halt=soon,fail=100% --jobs="$NUMBER_OF_JOBS" --bar --delay 60 --resume-failed \
         --joblog scripts/05_train_additional_ner_models_for_paper.joblog \
         --colsep ' +' \
         -- \
           'GPU_ID='"$GPU_ID"'; '\
           'docker run --rm -u "$(id -u):$(id -g)" --hostname "'"$HOSTNAME"'" --runtime=nvidia -e CUDA_DEVICE_ORDER=PCI_BUS_ID -e NVIDIA_VISIBLE_DEVICES="$GPU_ID" -e TOKENIZERS_PARALLELISM=false -e COMET_API_KEY -v "$PWD"/..:/workdir:rw -w /workdir/"${PWD##*/}" -v "'"$ROOT_PATH"'":"'"$ROOT_PATH"'":rw -v "'"$ANNOTATION_PATH"'":"'"$ANNOTATION_PATH"'":ro -v "'"$OCR_EVAL_PATH"'":"'"$OCR_EVAL_PATH"'":ro "'"$IMAGE_NAME"'" nice -n 19 python scripts/05_train_additional_ner_models_for_paper.py {1} {2} {3} {4} '"$GIT_COMMIT" \
         :::: scripts/05_train_additional_ner_models_for_paper.tasks
