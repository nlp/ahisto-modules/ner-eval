#!/bin/bash
# Builds a Docker image with the Python package and uses it to complete NER datasets at apollo.fi.muni.cz

set -e -o xtrace

HOSTNAME=docker.apollo.fi.muni.cz
IMAGE_NAME=ahisto/named-entity-search:latest
ROOT_PATH=/nlp/projekty/ahisto/public_html/named-entity-search/results/
ANNOTATION_PATH=/nlp/projekty/ahisto/annotations/
OCR_EVAL_PATH=/nlp/projekty/ahisto/ahisto-ocr-eval/

DOCKER_BUILDKIT=1 docker build --build-arg UID="$(id -u)" --build-arg GID="$(id -g)" --build-arg UNAME="$(id -u -n)" . -f scripts/03b_complete_ner_datasets.Dockerfile -t "$IMAGE_NAME"

parallel --halt=soon,fail=100% --bar --delay 300 --resume-failed \
         --joblog scripts/03b_complete_ner_datasets.joblog \
         -- \
           'GPU_ID=$(nvidia-smi | grep -B 1 -- " [0-9]MiB / 15360MiB" | tail -n 2 | head -n 1 | awk "{ print \$2 }"); '\
           'docker run --rm -u "$(id -u):$(id -g)" --hostname "'"$HOSTNAME"'" --runtime=nvidia -e CUDA_DEVICE_ORDER=PCI_BUS_ID -e NVIDIA_VISIBLE_DEVICES="$GPU_ID" -e TOKENIZERS_PARALLELISM=false -v "$PWD"/..:/workdir:rw -w /workdir/"${PWD##*/}" -v "'"$ROOT_PATH"'":"'"$ROOT_PATH"'":rw -v "'"$ANNOTATION_PATH"'":"'"$ANNOTATION_PATH"'":ro -v "'"$OCR_EVAL_PATH"'":"'"$OCR_EVAL_PATH"'":ro "'"$IMAGE_NAME"'" nice -n 19 python scripts/03b_complete_ner_datasets.py {}' \
         :::: scripts/03b_complete_ner_datasets.tasks
