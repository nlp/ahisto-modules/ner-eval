import os
import sys

from ahisto_named_entity_search.recognition import NerModel, get_schedule


if __name__ == '__main__':
    assert len(sys.argv) == 6

    model_generation = sys.argv[1]
    input_corpus_mlm_name = sys.argv[2]
    input_corpus_tc_name = sys.argv[3]
    tc_loss_name = sys.argv[4]
    git_commit = sys.argv[5]

    model_basename = f'model_ner_{input_corpus_mlm_name}_{input_corpus_tc_name}_{tc_loss_name}_{model_generation}'
    model_checkpoint_basename = f'{model_basename}_checkpoints.{git_commit}'

    os.environ['COMET_PROJECT_NAME'] = f'ahisto-ner_{model_generation}'

    if '+regests_tiny' in input_corpus_tc_name:
        search_method = 'manatee+regests'
    else:
        search_method = 'manatee'

    cross_page_boundaries = 'non-crossing'
    only_relevant = 'only-relevant'
    schedule_name = 'fair-sequential'

    if input_corpus_mlm_name == 'books-medium':
        sentence_basename = f'dataset_mlm_{cross_page_boundaries}_{only_relevant}'
        training_sentence_basename = f'{sentence_basename}_training'
        validation_sentence_basename = f'{sentence_basename}_validation'
    else:
        raise ValueError(f'Unknown MLM corpus {input_corpus_mlm_name}')

    if 'books-small' in input_corpus_tc_name:
        tagged_sentence_basename = f'dataset_ner_{search_method}_{cross_page_boundaries}_{only_relevant}'
        training_tagged_sentence_basename = f'{tagged_sentence_basename}_training'
        validation_tagged_sentence_basename = f'{tagged_sentence_basename}_validation'
    elif 'books-medium' in input_corpus_tc_name:
        tagged_sentence_basename = f'dataset_ner_{search_method}_{cross_page_boundaries}_{only_relevant}'
        training_tagged_sentence_basename = f'{tagged_sentence_basename}_training_automatically_tagged'
        validation_tagged_sentence_basename = f'{tagged_sentence_basename}_validation_automatically_tagged'
    elif 'books-large' in input_corpus_tc_name:
        tagged_sentence_basename = f'dataset_mlm_{cross_page_boundaries}_{only_relevant}'
        training_tagged_sentence_basename = f'{tagged_sentence_basename}_training_automatically_tagged_007'
        validation_tagged_sentence_basename = f'{tagged_sentence_basename}_validation_automatically_tagged_007'
    else:
        raise ValueError(f'Unknown TC corpus {input_corpus_tc_name}')

    if tc_loss_name == 'unweighted':
        use_label_weighting = False
    elif tc_loss_name == 'weighted':
        use_label_weighting = True
    else:
        raise ValueError(f'Unknown TC loss {tc_loss_name}')

    base_model = 'xlm-roberta-base'
    batch_size = 4
    gradient_accumulation_steps = 4
    learning_rate = 0.00005
    number_of_training_epochs = 10
    max_steps = 32500
    warmup_ratio = 0.0

    try:
        ner_model = NerModel.load(model_basename)
        with ner_model.model as model:  # Try actually loading the NER model
            pass
    except EnvironmentError:
        NerModel.train_and_save(model_checkpoint_basename, model_basename,
                                training_sentence_basename, validation_sentence_basename,
                                training_tagged_sentence_basename,
                                validation_tagged_sentence_basename,
                                schedule_name=schedule_name,
                                base_model=base_model,
                                batch_size=batch_size,
                                gradient_accumulation_steps=gradient_accumulation_steps,
                                learning_rate=learning_rate,
                                number_of_training_epochs=number_of_training_epochs,
                                max_steps=max_steps,
                                warmup_ratio=warmup_ratio,
                                use_label_weighting=use_label_weighting)
