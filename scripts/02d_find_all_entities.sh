#!/bin/bash
set -e
export LC_ALL=C

NUMBER_OF_ENTITIES=15304
CHUNK_SIZE=100
NUMBER_OF_BATCHES=$((NUMBER_OF_ENTITIES / CHUNK_SIZE + 1))

# Compute the results.
parallel --bar --halt=soon,fail=100% --jobs=100% --resume-failed \
         --joblog scripts/02d_find_all_entities.joblog \
         --sshlogin 1/apollo \
         --sshlogin 1/aura \
         --sshlogin 1/adonis \
         --sshlogin 1/mir \
         --sshlogin 1/epimetheus1 \
         --sshlogin 1/epimetheus2 \
         --sshlogin 1/epimetheus3 \
         --sshlogin 1/epimetheus4 \
         --delay 10 \
         --workdir ahisto-named-entity-search \
         -- \
         scripts/02d_find_all_entities_worker.sh {} $NUMBER_OF_ENTITIES $CHUNK_SIZE \
         ::: $(seq 1 $NUMBER_OF_BATCHES | sort --random-sort --random-source=/dev/zero)
