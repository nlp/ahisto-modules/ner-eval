#!/bin/bash
set -e -o xtrace

# Set up a virtualenv.
VIRTUALENV=venv-$(hostname)
if [[ -e $VIRTUALENV ]]
then
  source $VIRTUALENV/bin/activate
else
  trap 'rm -rf $VIRTUALENV' EXIT
  python -m venv $VIRTUALENV
  source $VIRTUALENV/bin/activate
  pip install -U pip wheel
  trap '' EXIT
fi

# Log some information about the current machine.
hostname
python -V | grep 'Python 3\.[78]'

# Install the current version of the package and its dependencies.
pip install .

# Synchronize data.
if [[ $(hostname) = aura.fi.muni.cz ]]
then
  rsync --delete --progress -Cavz aurora:/nlp/projekty/ahisto/public_html/dokumenty/ /var/tmp/xnovot32/dokumenty_copy
  rsync --delete --progress -Cavz aurora:/nlp/projekty/ahisto/public_html/ocr-output/ /var/tmp/xnovot32/ocr-output_copy
fi

# Remove previous results.
if [[ $(hostname) = apollo.fi.muni.cz ]]
then
  rm -rf /nlp/projekty/ahisto/public_html/named-entity-search/results
  mkdir /nlp/projekty/ahisto/public_html/named-entity-search/results
  # rm -f /nlp/projekty/ahisto/public_html/named-entity-search/results/{bert-score,character-edit-similarity,sbert-similarity,word-edit-similarity}.{json,html}
  START_TIME=$(date --iso-8601=hours)
  HUMAN_READABLE_START_TIME="$(date +'%-d. %-m. %Y')"
  echo 'Now you can start computing the results on aura'
fi

# Compute the results.
if [[ $(hostname) = apollo.fi.muni.cz ]]
then
  nice -n 19 python scripts/01_evaluate_different_methods.py apollo.fi.muni.cz
elif [[ $(hostname) = aura.fi.muni.cz ]]
then
  systemd-run --user --scope -p MemoryMax=250M nice -n 19 python scripts/01_evaluate_different_methods.py aura.fi.muni.cz
fi

# Back up the results.
if [[ $(hostname) = apollo.fi.muni.cz ]]
then
  TAG=$(git describe --tags --always --long)
  sed -i 's#</a></h1>#</a>, '"${HUMAN_READABLE_START_TIME}"' (commit <a href="https://gitlab.fi.muni.cz/nlp/ahisto-modules/-/tree/'"${TAG}"'">'"${TAG}"'</a>)</h1>#' /nlp/projekty/ahisto/public_html/named-entity-search/results/*.html
  cp -rv /nlp/projekty/ahisto/public_html/named-entity-search/results{,_${START_TIME}_${TAG}}
fi

# Update the Jupyter notebook.
if [[ $(hostname) = apollo.fi.muni.cz ]]
then
  jupyter nbconvert --to notebook --execute 01_evaluate_different_methods.ipynb
  mv -v 01_evaluate_different_methods{.nbconvert,}.ipynb
  sed -r -i 's/\\r\\n"(,?)/\\n"\1/' 01_evaluate_different_methods.ipynb
fi
