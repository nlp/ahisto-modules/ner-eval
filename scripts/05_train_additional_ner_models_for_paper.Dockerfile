FROM pytorch/pytorch:latest
ARG UNAME=testuser
ARG UID=1000
ARG GID=1000
RUN apt -qy update
RUN apt -qy --no-install-recommends install git
COPY setup.py requirements.txt MANIFEST.in /ahisto_named_entity_search/
COPY scripts/ /ahisto_named_entity_search/scripts
COPY ahisto_named_entity_search /ahisto_named_entity_search/ahisto_named_entity_search
WORKDIR /ahisto_named_entity_search
RUN groupadd -g $GID -o $UNAME
RUN useradd -m -u $UID -g $GID -o -s /bin/bash $UNAME
USER $UNAME
ENV PATH="/home/$UNAME/.local/bin:${PATH}"
RUN pip install . datasets
RUN transformers-cli download xlm-roberta-base
