from importlib import import_module
from pathlib import Path
from typing import Iterable, Tuple, Set, List, Optional
import re
import sys
import logging

list_entities_in_vert_file = import_module('.06_list_entities_in_vert_file', package='scripts')
read_vert_file = list_entities_in_vert_file.read_vert_file
BookId = list_entities_in_vert_file.BookId
Page = list_entities_in_vert_file.Page
Word = list_entities_in_vert_file.Word
Other = list_entities_in_vert_file.Other

from more_itertools import zip_equal
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression


logger = logging.getLogger(__name__)


def read_ground_truth(input_file: Path) -> Iterable[Tuple[BookId, Iterable[Tuple[Page, Page]]]]:
    with input_file.open('r') as f:
        for line in f:
            book_and_page_ranges = re.match(r'(\d+)\s+(.*)', line)
            assert book_and_page_ranges is not None
            book = int(book_and_page_ranges.group(1))
            raw_page_ranges = book_and_page_ranges.group(2).strip()
            raw_page_ranges = [] if not raw_page_ranges else raw_page_ranges.split(',')
            page_ranges = []
            for page_range in raw_page_ranges:
                start_page, end_page = page_range.split('-')
                page_ranges.append((int(start_page), int(end_page)))
            yield book, page_ranges


def unroll_page_ranges(page_ranges: Iterable[Tuple[Page, Page]]) -> Set[Page]:
    return {page for start_page, end_page in page_ranges for page in range(start_page, end_page + 1)}


def get_paragraph_features(paragraph: List[Word]) -> Optional[List[float]]:
    num_all_tokens = len(paragraph)
    if num_all_tokens == 0:
        return None
    num_entity_tokens = sum(0 if isinstance(word, Other) else 1 for word in paragraph)
    num_nonentity_tokens = num_all_tokens - num_entity_tokens
    num_nonword_tokens = sum(1 if not str(word).isalpha() else 0 for word in paragraph)
    num_punctuation_tokens = sum(1 if not str(word).isalnum() else 0 for word in paragraph)
    num_numeric_tokens = sum(1 if str(word).isnumeric() else 0 for word in paragraph)

    sample = [
        num_entity_tokens,
        num_nonentity_tokens,
        num_all_tokens,
        num_entity_tokens / num_all_tokens,
        num_nonword_tokens / num_all_tokens,
        num_punctuation_tokens / num_all_tokens,
        num_numeric_tokens / num_all_tokens,
    ]
    return sample


def train_classifier(root_directory: Path, ground_truths: Iterable[Path]) -> Pipeline:
    X, y = [], []

    # Collect samples and features
    num_empty_paragraphs = 0
    for ground_truth in ground_truths:
        for book, page_ranges in read_ground_truth(ground_truth):
            index_pages = unroll_page_ranges(page_ranges)
            for _, page, paragraph in read_vert_file(root_directory / f'{book}.vert'):
                is_index = page in index_pages
                sample = get_paragraph_features(paragraph)
                if sample is None:
                    num_empty_paragraphs += 1
                    continue
                feature = 1 if is_index else -1
                X.append(sample)
                y.append(feature)

    # Compute sample weights
    num_all_paragraphs = len(y)
    num_index_paragraphs = sum(1 if feature == 1 else 0 for feature in y)
    num_non_index_paragraphs = num_all_paragraphs - num_index_paragraphs
    logger.info(f'Collected {num_all_paragraphs} training paragraphs (skipping {num_empty_paragraphs} empty paragraphs), {num_index_paragraphs} from index and {num_non_index_paragraphs} from main matter')
    assert num_index_paragraphs > 0
    assert num_non_index_paragraphs > 0
    sample_weight = [
        1 / (num_index_paragraphs if feature == 1 else num_non_index_paragraphs)
        for feature
        in y
    ]

    # Train classifier
    logger.debug('Training paragraph classifier')
    classifier = make_pipeline(StandardScaler(), LinearRegression())
    classifier.fit(X, y, linearregression__sample_weight=sample_weight)
    return classifier


def evaluate_classifier(root_directory: Path, ground_truth: Path, classifier: Pipeline) -> float:

    # Collect samples and features
    num_index_paragraphs, num_non_index_paragraphs, num_empty_paragraphs = 0, 0, 0
    samples, features = [], []
    for book, page_ranges in read_ground_truth(ground_truth):
        index_pages = unroll_page_ranges(page_ranges)
        for _, page, paragraph in read_vert_file(root_directory / f'{book}.vert'):
            is_index = page in index_pages
            if is_index:
                num_index_paragraphs += 1
            else:
                num_non_index_paragraphs += 1
            sample = get_paragraph_features(paragraph)
            if sample is None:
                num_empty_paragraphs += 1
                continue
            samples.append(sample)
            feature = 1 if is_index else -1
            features.append(feature)
    num_all_paragraphs = num_index_paragraphs + num_non_index_paragraphs
    logger.info(f'Collected {num_all_paragraphs} testing paragraphs (skipping {num_empty_paragraphs} empty paragraphs), {num_index_paragraphs} from index and {num_non_index_paragraphs} from main matter')

    # Evaluate classifier
    tp, tn, fp, fn = 0, 0, 0, 0
    predictions = classifier.predict(samples)
    for prediction, feature in zip_equal(predictions, features):
        predicted_index = prediction > 0
        is_index = feature > 0
        if is_index and predicted_index:
            tp += 1
        if not is_index and not predicted_index:
            tn += 1
        if not is_index and predicted_index:
            fp += 1
        if is_index and not predicted_index:
            fn += 1
    correct_predictions, incorrect_predictions = tp + tn, fp + fn
    assert correct_predictions + incorrect_predictions > 0
    precision = correct_predictions / (correct_predictions + incorrect_predictions)
    logger.info(f'Made {correct_predictions} correct predictions and {incorrect_predictions} incorrect predictions ({100 * precision:.2f}% precision, TP: {tp}, TN: {tn}, FP: {fp}, FN: {fn})')
    return precision


# Example usage:
# $ python3 -m scripts.07_paragraph_classification /mnt scripts/07_paragraph_classification.page-ranges.{train,test}
# 2023-04-24 11:29:56,228 Collected 31521 training paragraphs (skipping 58 empty paragraphs), 16640 from index and 14881 from main matter
# 2023-04-24 11:29:58,604 Collected 24798 testing paragraphs (skipping 39 empty paragraphs), 12486 from index and 12312 from main matter
# 2023-04-24 11:29:58,692 Made 18150 correct predictions and 6609 incorrect predictions (73.31% precision, TP: 9470, TN: 8680, FP: 3595, FN: 3014)
if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(asctime)s %(message)s')
    assert len(sys.argv) == 4
    root_directory = Path(sys.argv[1])
    ground_truth_train = Path(sys.argv[2])
    ground_truth_test = Path(sys.argv[3])
    classifier = train_classifier(root_directory, [ground_truth_train])
    evaluate_classifier(root_directory, ground_truth_test, classifier)
