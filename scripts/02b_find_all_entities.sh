#!/bin/bash
set -e
export LC_ALL=C

START_TIME=$(date --iso-8601=hours)
HUMAN_READABLE_START_TIME="$(date +'%-d. %-m. %Y')"
NUMBER_OF_ENTITIES=15304
CHUNK_SIZE=100
NUMBER_OF_BATCHES=$((NUMBER_OF_ENTITIES / CHUNK_SIZE + 1))

# Compute the results.
parallel --bar --halt=soon,fail=100% --jobs=100% --resume-failed \
         --joblog scripts/02b_find_all_entities.joblog \
         --sshlogin 1/apollo \
         --sshlogin 2/aura \
         --sshlogin 1/adonis \
         --sshlogin 1/mir \
         --sshlogin 1/epimetheus1 \
         --sshlogin 1/epimetheus2 \
         --sshlogin 1/epimetheus3 \
         --sshlogin 1/epimetheus4 \
         --delay 60 \
         --workdir ahisto-named-entity-search \
         -- \
         scripts/02b_find_all_entities_worker.sh {} $NUMBER_OF_ENTITIES $CHUNK_SIZE \
         ::: $(seq 1 $NUMBER_OF_BATCHES | sort --random-sort --random-source=/dev/zero)

# Back up the results.
TAG=$(git describe --tags --always --long)
sed -i 's#</a></h1>#</a>, '"${HUMAN_READABLE_START_TIME}"' (commit <a href="https://gitlab.fi.muni.cz/nlp/ahisto-modules/-/tree/'"${TAG}"'">'"${TAG}"'</a>)</h1>#' /nlp/projekty/ahisto/public_html/named-entity-search/results/*.html
cp -rv /nlp/projekty/ahisto/public_html/named-entity-search/results{,_${START_TIME}_${TAG}}
