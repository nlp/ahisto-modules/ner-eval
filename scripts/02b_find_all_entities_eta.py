import csv
from datetime import datetime, timedelta
from functools import lru_cache
import logging
from pathlib import Path
from sys import argv
from typing import Iterable, Tuple, Set, Callable, Optional


LOGGER = logging.getLogger(__name__)


ExitValue = bool
SequenceNumber = int
ContiguousSequence = Tuple[SequenceNumber, SequenceNumber]
StartTime = datetime
EndTime = datetime
JobRuntime = timedelta
Host = str
Line = Tuple[SequenceNumber, Host, StartTime, JobRuntime, ExitValue]


@lru_cache(maxsize=None)
def get_lines(input_joblog_file: Path) -> Iterable[Line]:
    lines = []
    with open(input_joblog_file, 'rt', newline='') as f:
        csv_reader = csv.reader(f, delimiter='\t')
        next(csv_reader)
        for raw_line in csv_reader:
            sequence_number, host, start_time, job_runtime, _, __, exit_value, *___ = raw_line
            sequence_number = int(sequence_number)
            start_time = datetime.fromtimestamp(float(start_time))
            job_runtime = timedelta(seconds = float(job_runtime))
            exit_value = exit_value == '0'
            line = (sequence_number, host, start_time, job_runtime, exit_value)
            lines.append(line)
    if not lines:
        raise ValueError(f'{input_joblog_file} is empty')
    return lines


@lru_cache(maxsize=None)
def get_start_time(input_joblog_file: Path, sequence_number: SequenceNumber) -> StartTime:
    for line in get_lines(input_joblog_file):
        current_sequence_number, _, start_time, *__ = line
        if current_sequence_number == sequence_number:
            return start_time
    raise ValueError(f'{sequence_number} does not exist in {input_joblog_file}')


@lru_cache(maxsize=None)
def get_job_runtime(input_joblog_file: Path, sequence_number: SequenceNumber) -> JobRuntime:
    for line in get_lines(input_joblog_file):
        current_sequence_number, _, __, job_runtime, *___ = line
        if current_sequence_number == sequence_number:
            return job_runtime
    raise ValueError(f'{sequence_number} does not exist in {input_joblog_file}')


def get_successful_sequence_numbers(input_joblog_file: Path) -> Iterable[SequenceNumber]:
    for line in get_lines(input_joblog_file):
        sequence_number, *_, exit_value = line
        if exit_value:
            yield sequence_number


def get_hosts(input_joblog_file: Path) -> Set[Host]:
    hosts = set()
    for line in get_lines(input_joblog_file):
        _, host, *_ = line
        hosts.add(host)
    return hosts


def get_contiguous_sequences(input_joblog_file: Path,
                             sequence_numbers: Iterable[SequenceNumber]) -> Iterable[ContiguousSequence]:
    sequence_numbers_list = list(sequence_numbers)
    sequence_numbers_set = set(sequence_numbers_list)
    for start_number in sequence_numbers_list:
        end_number = start_number
        start_start_time = get_start_time(input_joblog_file, start_number)
        while (end_number + 1) in sequence_numbers_set and \
              start_start_time < get_start_time(input_joblog_file, end_number + 1):
            end_number += 1
        if end_number > start_number:
            contiguous_sequence = (start_number, end_number)
            yield contiguous_sequence


def get_contiguous_sequence_length(contiguous_sequence: ContiguousSequence) -> int:
    start_number, end_number = contiguous_sequence
    assert start_number < end_number
    sequence_length = end_number - start_number + 1
    return sequence_length


def get_contiguous_sequence_filter(min_sequence_length: int,
                                   skip_to_sequence_number: Optional[int]) -> Callable[[ContiguousSequence], bool]:

    def contiguous_sentence_filter(contiguous_sequence: ContiguousSequence) -> bool:
        start_number, _ = contiguous_sequence
        contiguous_sequence_length = get_contiguous_sequence_length(contiguous_sequence)
        if all([
                    contiguous_sequence_length >= min_sequence_length,
                    skip_to_sequence_number is None or start_number >= skip_to_sequence_number,
                ]):
            return True
        return False

    return contiguous_sentence_filter


def get_contiguous_sequence(input_joblog_file: Path, skip_to_sequence_number: Optional[int]) -> ContiguousSequence:
    hosts = get_hosts(input_joblog_file)
    min_sequence_length = len(hosts) + 1
    message = [f'Looking for a contiguous sequence of at least {min_sequence_length} finished jobs']
    if skip_to_sequence_number is not None:
        message.append(f'starting with sequence number {skip_to_sequence_number}')
    LOGGER.info(' '.join(message))

    sequence_numbers = get_successful_sequence_numbers(input_joblog_file)
    sequence_numbers = sorted(sequence_numbers)

    contiguous_sequences = get_contiguous_sequences(input_joblog_file, sequence_numbers)
    contiguous_sentence_filter = get_contiguous_sequence_filter(min_sequence_length, skip_to_sequence_number)
    filtered_contiguous_sequences = filter(contiguous_sentence_filter, contiguous_sequences)
    sorted_contiguous_sequences = sorted(filtered_contiguous_sequences,
                                         key=get_contiguous_sequence_length, reverse=True)

    if not sorted_contiguous_sequences:
        raise ValueError(f'No contiguous sequences of {min_sequence_length} finished jobs exist')
    else:
        contiguous_sequence, *_ = sorted_contiguous_sequences
        start_number, end_number = contiguous_sequence
        contiguous_sequence_length = get_contiguous_sequence_length(contiguous_sequence)
        LOGGER.info(f'Found contiguous sequence {start_number}--{end_number} ({contiguous_sequence_length} jobs)')
        return contiguous_sequence


def get_mean_job_runtime(input_joblog_file: Path, contiguous_sequence: ContiguousSequence) -> JobRuntime:
    sequence_length = get_contiguous_sequence_length(contiguous_sequence)

    start_number, end_number = contiguous_sequence
    start_start_time = get_start_time(input_joblog_file, start_number)
    end_start_time = get_start_time(input_joblog_file, end_number)
    end_job_runtime = get_job_runtime(input_joblog_file, end_number)
    end_end_time = end_start_time + end_job_runtime

    duration = (end_end_time - start_start_time) / sequence_length
    return duration


def get_estimated_end_time(input_joblog_file: Path, last_sequence_number: int, contiguous_sequence: ContiguousSequence) -> EndTime:
    start_number, _ = contiguous_sequence
    complete_sequence = (start_number, last_sequence_number)
    complete_sequence_length = get_contiguous_sequence_length(complete_sequence)

    start_start_time = get_start_time(input_joblog_file, start_number)
    mean_job_runtime = get_mean_job_runtime(input_joblog_file, contiguous_sequence)

    last_sequence_number_end_time = start_start_time + complete_sequence_length * mean_job_runtime
    return last_sequence_number_end_time


def main(input_joblog_file: Path, last_sequence_number: int, skip_to_sequence_number: Optional[int]) -> None:
    contiguous_sequence = get_contiguous_sequence(input_joblog_file, skip_to_sequence_number)
    estimated_end_time = get_estimated_end_time(input_joblog_file, last_sequence_number, contiguous_sequence)
    print(f'The last job is estimated to finish on {estimated_end_time}.')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
    input_joblog_file = Path(argv[1])
    last_sequence_number = int(argv[2])
    skip_to_sequence_number = int(argv[3]) if len(argv) > 3 else None
    main(input_joblog_file, last_sequence_number, skip_to_sequence_number)
