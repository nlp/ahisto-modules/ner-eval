#!/bin/bash
set -e

# Set up a virtualenv.
VIRTUALENV=venv-$(hostname)
if [[ -e $VIRTUALENV ]]
then
  source $VIRTUALENV/bin/activate
else
  trap 'rm -rf $VIRTUALENV' EXIT
  python3 -m venv $VIRTUALENV
  source $VIRTUALENV/bin/activate
  pip install -U pip wheel
  trap '' EXIT
fi

python -V | grep -q 'Python 3\.[78]'
pip install --quiet --exists-action i .

# Synchronize data.
if [[ $(hostname) = aura.fi.muni.cz || $(hostname) = adonis.fi.muni.cz ]]
then
  rsync --quiet --delete -Cavz aurora:/nlp/projekty/ahisto/public_html/dokumenty-2022-06-05/ /var/tmp/xnovot32/dokumenty_copy
  rsync --quiet --delete -Cavz aurora:/nlp/projekty/ahisto/public_html/ocr-output/ /var/tmp/xnovot32/ocr-output_copy
elif [[ $(hostname) = mir ]]
then
  rsync --quiet --delete -Cavz aurora:/nlp/projekty/ahisto/public_html/dokumenty-2022-06-05/ /home/novotny/ahisto-named-entity-search-files/dokumenty_copy
  rsync --quiet --delete -Cavz aurora:/nlp/projekty/ahisto/public_html/ocr-output/ /home/novotny/ahisto-named-entity-search-files/ocr-output_copy
fi

# Compute the results.
LC_ALL=C nice -n 19 python scripts/02b_find_all_entities_worker.py "$@"
