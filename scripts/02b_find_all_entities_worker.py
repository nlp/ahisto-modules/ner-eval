import socket

hostname = socket.gethostname()

import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(message)s')

from typing import Iterable, TypeVar

T = TypeVar('T')

from ahisto_named_entity_search.entity import load_entities, Entity, Place, Person

all_entities = load_entities()
place_entities = [entity for entity in all_entities if isinstance(entity, Place)]
person_entities = [entity for entity in all_entities if isinstance(entity, Person)]

from ahisto_named_entity_search.entity import (Patchset, EntityType,
                                               default_entity_map_persons as entity_map_persons,
                                               default_entity_map_places as entity_map_places)

all_canonicalized_entities = {
    **Patchset.from_file('patchset-persons.xlsx', person_entities).apply(person_entities, entity_map_persons),
    **Patchset.from_file('patchset-places.xlsx', place_entities).apply(place_entities, entity_map_places),
}

def difficulty_sort_key(entity: Entity):
    difficulty = len(str(entity))
    return (difficulty, entity)

unique_canonical_entities = sorted(set(all_canonicalized_entities.values()), key=difficulty_sort_key)
expected_num_unique_canonical_entities = int(sys.argv[2])
assert len(unique_canonical_entities) == expected_num_unique_canonical_entities

from ahisto_named_entity_search.document import Document, load_documents

documents = load_documents()

from ahisto_named_entity_search.index import FuzzyRegexIndex

from more_itertools import chunked

chunk_size = int(sys.argv[3])

from json import JSONDecodeError

from ahisto_named_entity_search.search import Search
from ahisto_named_entity_search.search import SearchResultList

from math import ceil
import re

short_hostname = re.sub(r'\..*', '', hostname)

iterable = chunked(unique_canonical_entities, chunk_size)
iterable = list(iterable)

chunk_number = int(sys.argv[1])
entities = iterable[chunk_number - 1]
filename = f'fuzzy-regex_{chunk_number:03}_{short_hostname}'
try:
    _ = SearchResultList.load(filename, entities)
except (IOError, JSONDecodeError):
    fuzzy_regex_results = Search(FuzzyRegexIndex(documents.values())).search(entities)
    fuzzy_regex_results.save(filename)
