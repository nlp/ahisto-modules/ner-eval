import sys
from random import Random
from typing import Iterable

from more_itertools import zip_equal

from ahisto_named_entity_search.document import Document
from ahisto_named_entity_search.recognition import NerModel
from ahisto_named_entity_search.search import TaggedSentence


RANDOM_SEED = 42


def get_tagged_sentence_density(tagged_sentences: Iterable[TaggedSentence]) -> float:
    total_density, total_weight = 0.0, 0
    for tagged_sentence in tagged_sentences:
        density, weight = tagged_sentence.density, len(tagged_sentence)
        total_density += weight * density
        total_weight += weight
    return total_density / total_weight if total_weight > 0 else 0.0


if __name__ == '__main__':
    assert len(sys.argv) == 2

    input_sentence_basename = sys.argv[1]

    input_sentences = Document.load_sentences(input_sentence_basename)
    input_sentences = list(input_sentences)
    excluded_tagged_sentence_basename = f'dataset_ner_manatee_non-crossing_only-relevant_testing_401-500_tagged'
    output_tagged_sentence_basename = f'{input_sentence_basename}_automatically_tagged_007'
    try:
        output_tagged_sentences = TaggedSentence.load(output_tagged_sentence_basename)
        output_tagged_sentences = list(output_tagged_sentences)
    except FileNotFoundError:
        completion_models = [NerModel.load('model_ner_manatee_non-crossing_only-relevant_fair-sequential_007')]

        excluded_tagged_sentences = TaggedSentence.load(excluded_tagged_sentence_basename)
        excluded_tagged_sentences = set(excluded_tagged_sentences)

        model_tagged_sentences = NerModel.tag_sentences_with_multiple_models(completion_models, input_sentences)
        output_tagged_sentences = []
        for model_tagged_sentence in model_tagged_sentences:
            if model_tagged_sentence in excluded_tagged_sentences:
                continue
            partially_overlaps = False  # do extra legwork to prevent contamination with test dataset
            for excluded_tagged_sentence in excluded_tagged_sentences:
                if (
                            model_tagged_sentence.sentence in excluded_tagged_sentence.sentence or
                            excluded_tagged_sentence.sentence in model_tagged_sentence.sentence
                        ):
                    partially_overlaps = True
                    break
            if partially_overlaps:
                continue
            output_tagged_sentence = model_tagged_sentence
            output_tagged_sentences.append(output_tagged_sentence)

        Random(RANDOM_SEED).shuffle(output_tagged_sentences)
        TaggedSentence.save(output_tagged_sentence_basename, output_tagged_sentences)

    output_tagged_sentence_density = get_tagged_sentence_density(output_tagged_sentences)
    print(f'Improved tag density of "{input_sentence_basename}" from 0% '
          f'to {100.0 * output_tagged_sentence_density:.2f}%.')
