from collections import Counter
import csv
from functools import total_ordering
import gzip
from importlib import import_module
from itertools import chain, repeat
from io import TextIOWrapper
import json
from pathlib import Path
import random
import re
import sys
from typing import Iterable, List, Union, Callable, Any, Tuple, Optional
import logging
import pickle

from ahisto_named_entity_search.util import extract_sentences
from ahisto_named_entity_search.document import BookId, Page
from more_itertools import zip_equal
from openpyxl import Workbook
from openpyxl.styles import Font
from openpyxl.cell.text import InlineFont
from openpyxl.cell.rich_text import TextBlock, CellRichText
from sklearn.pipeline import Pipeline
from tqdm import tqdm


logger = logging.getLogger(__name__)


@total_ordering
class Word:
    def __init__(self, word: str):
        self.word = word

    def __str__(self) -> str:
        return self.word

    def __repr__(self) -> str:
        return str(self)

    @staticmethod
    def fingerprint(text: Iterable['Word']) -> str:
        return ''.join(word.stem for word in text)

    @property
    def stem(self) -> str:
        return re.sub(r'\W', '', self.word).lower()

    @property
    def formatted(self) -> Union[str, TextBlock]:
        return str(self)

    def __eq__(self, other) -> bool:
        if not isinstance(other, Word):
            return NotImplemented
        else:
            return str(self) == str(other)

    def __lt__(self, other) -> bool:
        if not isinstance(other, Word):
            return NotImplemented
        else:
            return str(self) < str(other)


class Entity(Word):
    def __init__(self, word: str, language: str):
        self.language = language
        super().__init__(word)


class Person(Entity):
    def __init__(self, word, language):
        super().__init__(word, language)

    def __repr__(self) -> str:
        return f'<PER: {self.word}>'

    @property
    def formatted(self) -> Union[str, TextBlock]:
        return TextBlock(InlineFont(b=True), str(self))


class BPerson(Person):
    def __init__(self, word, language):
        super().__init__(word, language)

    def __repr__(self) -> str:
        return f'<B-PER: {self.word}>'


class IPerson(Person):
    def __init__(self, word, language):
        super().__init__(word, language)

    def __repr__(self) -> str:
        return f'<I-PER: {self.word}>'


class Location(Entity):
    def __init__(self, word, language):
        super().__init__(word, language)

    def __repr__(self) -> str:
        return f'<LOC: {self.word}>'

    @property
    def formatted(self) -> Union[str, TextBlock]:
        return TextBlock(InlineFont(i=True), str(self))


class BLocation(Location):
    def __init__(self, word, language):
        super().__init__(word, language)

    def __repr__(self) -> str:
        return f'<B-LOC: {self.word}>'


class ILocation(Location):
    def __init__(self, word, language):
        super().__init__(word, language)

    def __repr__(self) -> str:
        return f'<I-LOC: {self.word}>'


class Other(Word):
    def __init__(self, word):
        super().__init__(word)


def read_vert_file(input_file: Path, num_input_lines: Optional[int] = None) -> Iterable[Tuple[BookId, Page, List[Word]]]:
    current_paragraph = []
    current_book = None
    current_page = None
    in_entity = False
    entity_words = []
    entity_word_types = []
    entity_languages = Counter()

    with input_file.open('rb') as rf:

        if input_file.suffix == '.gz':
            rf = gzip.open(rf, 'rt')
        else:
            rf = TextIOWrapper(rf)

        if num_input_lines is not None:
            rf = tqdm(rf, total=num_input_lines, desc=f'Reading lines from {input_file}')

        for line in rf:

            # Skip empty lines
            line = line.strip()
            if not line:
                continue

            if line.startswith('<') and line.endswith('>'):
                if line.startswith('<entity'):
                    match = re.fullmatch(r'<entity type="(?P<type>.*)" norm="(?P<norm>.*)" display="(?P<display>.*)">', line)
                    assert match, f'Failed to parse entity: {line}'
                    in_entity = True
                    assert len(entity_words) == 0
                    assert len(entity_word_types) == 0
                    if match.group('display') == 'true':
                        entity_words = match.group('norm').split()
                        entity_word_types = chain([f'B-{match.group("type")}'], repeat(f'I-{match.group("type")}'))
                        entity_word_types = list(entity_word_types)
                elif line == '</entity>':
                    in_entity = False
                    if len(entity_words) > 0:
                        (language, _), = entity_languages.most_common(1)
                        for word, current_word_type in zip_equal(entity_words, entity_word_types):
                            if current_word_type == 'B-PER':
                                word = BPerson(word, language)
                            elif current_word_type == 'I-PER':
                                word = IPerson(word, language)
                            elif current_word_type == 'B-LOC':
                                word = BLocation(word, language)
                            elif current_word_type == 'I-LOC':
                                word = ILocation(word, language)
                            else:
                                raise ValueError(f'Unknown word type: {current_word_type}')
                            current_paragraph.append(word)
                    entity_words.clear()
                    entity_word_types.clear()
                    entity_languages.clear()
                elif line.startswith('<doc '):
                    match = re.match(r'<doc[^>]* id="(\d+)"', line)
                    assert match is not None
                    current_book = int(match.group(1))
                elif line.startswith('<page '):
                    match = re.match(r'<page[^>]* n="s?(\d+)"', line)
                    assert match is not None
                    current_page = int(match.group(1))
                elif line == '</p>':
                    assert current_book is not None
                    assert current_page is not None
                    yield current_book, current_page, current_paragraph
                    current_paragraph = []
                continue

            # Handle the current word
            word, *_, language = line.split('\t')
            if in_entity:
                entity_languages[language] += 1
            else:
                word = Other(word)
                current_paragraph.append(word)


@total_ordering
class TableRow:
    CONTEXT_SIZE = 10
    HEADER = ('Left context', 'Entity', 'Right context',
              'Paragraph start distance', 'Sentence start distance',
              'Sentence end distance', 'Paragraph end distance',
              'Language', 'Absolute frequency on the collection (approximate)')

    def __init__(self, entities: Iterable[Entity], left_context: Iterable[Word], right_context: Iterable[Word],
                 sentence_start_distance: int, sentence_end_distance: int,
                 paragraph_start_distance: int, paragraph_end_distance: int,
                 get_frequency: Callable[['TableRow'], int]):
        self.entities = tuple(entities)
        assert len(self.entities) > 0
        assert all(isinstance(entity, Entity) for entity in self.entities)
        self.left_context = tuple(list(left_context)[-self.CONTEXT_SIZE:])
        self.right_context = tuple(list(right_context)[:self.CONTEXT_SIZE])
        self.sentence_start_distance = sentence_start_distance
        self.sentence_end_distance = sentence_end_distance
        self.paragraph_start_distance = paragraph_start_distance
        self.paragraph_end_distance = paragraph_end_distance
        self.get_frequency = get_frequency

    @property
    def frequency(self) -> int:
        return self.get_frequency(self)

    @property
    def formatted(self) -> List[Union[str, TextBlock]]:

        def format_words(words: Iterable[Word]) -> CellRichText:
            words = list(words)
            elements = []
            for index, word in enumerate(words):
                elements.append(word.formatted)
                if index + 1 < len(words):
                    elements.append(' ')
            return CellRichText(*elements)

        return self.format(format_words)

    @property
    def plain(self) -> List[str]:

        def format_words(words: Iterable[Word]) -> str:
            return ' '.join(repr(word) for word in words)

        return self.format(format_words)

    def format(self, format_words: Callable[[Iterable[Word]], Any]):
        row = [format_words(self.left_context), format_words(self.entities), format_words(self.right_context),
               str(self.paragraph_start_distance), str(self.sentence_start_distance),
               str(self.sentence_end_distance), str(self.paragraph_end_distance), self.entities[0].language,
               str(self.frequency)]
        return row

    def __str__(self):
        return f"{' '.join(map(str, self.left_context))} <{' '.join(map(str, self.entities))}> {' '.join(map(str, self.right_context))}"

    def __eq__(self, other) -> bool:
        if not isinstance(other, TableRow):
            return NotImplemented
        else:
            return self.entities == other.entities

    def __lt__(self, other) -> bool:
        if not isinstance(other, TableRow):
            return NotImplemented
        else:
            return self.entities < other.entities


def produce_table_rows(paragraphs: Iterable[List[Word]]) -> List[TableRow]:
    num_skipped_duplicates = 0
    seen_fingerprints = Counter()  # do not produce duplicate rows for entities

    def get_frequency(table_row: TableRow):
        entity_fingerprint = Word.fingerprint(table_row.entities)
        frequency = seen_fingerprints[entity_fingerprint]
        return frequency

    table_rows = []
    for paragraph in paragraphs:
        sentences = extract_sentences(' '.join(str(word) for word in paragraph), include_prefix=True, include_suffix=True)
        sentence_boundaries = [0]
        for sentence_number, (sentence_prefix, sentence) in enumerate(sentences):
            sentence_number = sentence_number + 1
            if sentence_prefix or sentence_number == 1:
                sentence_boundaries.append(sentence_boundaries[-1] + len(sentence_prefix.split()))
                sentence_boundaries.append(sentence_boundaries[-1] + len(sentence.split()))
            elif sentence:  # A word was broken to two sentences in the middle
                sentence_boundaries.append(sentence_boundaries[-1] + len(sentence.split()) - 1)
        sentence_boundaries = sorted(set(sentence_boundaries))  # Remove duplicates
        assert sentence_boundaries[-1] == len(paragraph), (sentence_boundaries, len(paragraph))
        previous_word = None
        for index, word in enumerate(paragraph):
            if isinstance(previous_word, Entity) and (isinstance(word, (BPerson, BLocation)) or not isinstance(word, Entity)):
                right_context = paragraph[index:]
                sentence_end_distance = sentence_boundaries[1] - index
                paragraph_end_distance = len(paragraph) - index
                assert sentence_end_distance <= paragraph_end_distance
                entity_fingerprint = Word.fingerprint(entities)
                if entity_fingerprint not in seen_fingerprints:
                    table_row = TableRow(entities, left_context, right_context, sentence_start_distance, sentence_end_distance,
                                         paragraph_start_distance, paragraph_end_distance, get_frequency)
                    table_rows.append(table_row)
                else:
                    num_skipped_duplicates += 1
                seen_fingerprints[entity_fingerprint] += 1

            while index >= sentence_boundaries[1]:
                sentence_boundaries.pop(0)
            assert index >= sentence_boundaries[0] and index < sentence_boundaries[1]

            if isinstance(word, (BPerson, BLocation)):
                left_context = paragraph[:index]
                sentence_start_distance = index - sentence_boundaries[0]
                paragraph_start_distance = index
                entities = []
            if isinstance(word, Entity):
                entities.append(word)
            previous_word = word
        if isinstance(previous_word, Entity):
            right_context = []
            sentence_end_distance = 0
            paragraph_end_distance = 0
            entity_fingerprint = Word.fingerprint(entities)
            if entity_fingerprint not in seen_fingerprints:
                table_row = TableRow(entities, left_context, right_context, sentence_start_distance, sentence_end_distance,
                                     paragraph_start_distance, paragraph_end_distance, get_frequency)
                table_rows.append(table_row)
            else:
                num_skipped_duplicates += 1
            seen_fingerprints[entity_fingerprint] += 1
    logger.info(f'Produced rows for {len(seen_fingerprints)} unique entities, skipping {num_skipped_duplicates} duplicates')
    return table_rows


def write_csv_file(output_csv_file: Path, table_rows: Iterable[TableRow]) -> None:
    with output_csv_file.open('wt', newline='') as csvfile:
        writer = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)

        writer.writerow(TableRow.HEADER)

        num_rows = 0
        for table_row in table_rows:
            writer.writerow(table_row.plain)
            num_rows += 1

    logger.info(f'Wrote {num_rows} table rows to {output_csv_file}')


def write_xlsx_file(output_xlsx_file: Path, table_rows: Iterable[TableRow]) -> None:
    workbook = Workbook()
    worksheet = workbook.active

    worksheet.append(TableRow.HEADER)
    for cell in worksheet[1]:
        cell.font = Font(bold=True)

    num_rows = 0
    for table_row in table_rows:
        worksheet.append(table_row.formatted)
        num_rows += 1

    workbook.save(output_xlsx_file)
    logger.info(f'Wrote {num_rows} table rows to {output_xlsx_file}')


def take_sample(table_rows: Iterable[TableRow], k: int, random_seed: int = 42) -> Iterable[TableRow]:
    table_rows = list(table_rows)
    if len(table_rows) >= k:
        random.seed(random_seed)
        table_rows = random.sample(table_rows, k)
    table_rows = sorted(table_rows)
    return table_rows


def remove_index_paragraphs(paragraphs: Iterable[List[Word]], classifier: Pipeline) -> Iterable[List[Word]]:
    paragraph_classification = import_module('.07_paragraph_classification', package='scripts')
    get_paragraph_features = paragraph_classification.get_paragraph_features
    nonempty_paragraphs = []
    samples = []
    for paragraph in paragraphs:
        sample = get_paragraph_features(paragraph)
        if sample is None:
            continue
        nonempty_paragraphs.append(paragraph)
        samples.append(sample)
    predictions = classifier.predict(samples)
    for paragraph, prediction in zip_equal(nonempty_paragraphs, predictions):
        predicted_index = prediction > 0
        if not predicted_index:
            yield paragraph


def load_classifier(root_directory: Path, ground_truths: Iterable[Path], classifier_file: Path, pickle_protocol: int = 0) -> Pipeline:
    try:
        with classifier_file.open('rb') as f:
            classifier = pickle.load(f)
    except IOError:
        paragraph_classification = import_module('.07_paragraph_classification', package='scripts')
        train_classifier = paragraph_classification.train_classifier
        classifier = train_classifier(root_directory, ground_truths)
        with classifier_file.open('wb') as f:
            pickle.dump(classifier, f, protocol=pickle_protocol)
    return classifier


def main(root_directory: Path, ground_truths: Iterable[Path], classifier_file: Path, input_vert_file: Path, num_input_lines: int, output_basename: Path, num_output_xlsx_rows: int, num_output_most_common_xlsx_rows: int) -> None:
    classifier = load_classifier(root_directory, ground_truths, classifier_file)
    paragraphs = read_vert_file(input_vert_file, num_input_lines)
    paragraphs = (paragraph for book, page, paragraph in paragraphs)
    paragraphs = remove_index_paragraphs(paragraphs, classifier)
    table_rows = produce_table_rows(paragraphs)
    write_csv_file(output_basename.with_suffix('.csv'), table_rows)
    write_xlsx_file(output_basename.with_suffix('.xlsx'),
                    take_sample(table_rows, num_output_xlsx_rows))
    write_xlsx_file(output_basename.with_suffix(f'.top_{num_output_most_common_xlsx_rows}.xlsx'),
                    sorted(table_rows, reverse=True, key=lambda row: (row.frequency, row))[:num_output_most_common_xlsx_rows])


# Example usage:
# $ python3 -m scripts.06_list_entities_in_vert_file /mnt1 '["scripts/07_paragraph_classification.page-ranges.train", "scripts/07_paragraph_classification.page-ranges.test"]' 'scripts/07_paragraph_classification.pickle' /mnt2/corpus.prim_vert_after_ner.gz `gzip -d < /mnt2/corpus.prim_vert_after_ner.gz | wc -l` /mnt2/corpus 100000 1000
if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(asctime)s %(message)s')
    assert len(sys.argv) == 9
    root_directory = Path(sys.argv[1])
    ground_truths = map(Path, json.loads(sys.argv[2]))
    classifier_file = Path(sys.argv[3])
    input_vert_file = Path(sys.argv[4])
    num_input_lines = int(sys.argv[5])
    output_basename = Path(sys.argv[6])
    num_output_xlsx_rows = int(sys.argv[7])
    num_output_most_common_xlsx_rows = int(sys.argv[8])
    main(root_directory, ground_truths, classifier_file, input_vert_file, num_input_lines, output_basename, num_output_xlsx_rows, num_output_most_common_xlsx_rows)
