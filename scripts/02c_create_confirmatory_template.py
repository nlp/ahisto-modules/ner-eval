from ahisto_named_entity_search.entity import load_entities, Place, Person

all_entities = load_entities()
place_entities = [entity for entity in all_entities if isinstance(entity, Place)]
person_entities = [entity for entity in all_entities if isinstance(entity, Person)]

from ahisto_named_entity_search.entity import Patchset, EntityType

person_patchset = Patchset.from_file('patchset-persons.xlsx', person_entities)
place_patchset = Patchset.from_file('patchset-places.xlsx', place_entities)

def concretize_entity_types(patchset: Patchset, default_entity_type: EntityType) -> None:
    for patch in patchset.patches.values():
        if patch.entity_type is None:
            patch.entity_type = default_entity_type

concretize_entity_types(person_patchset, EntityType.PERSON)
concretize_entity_types(place_patchset, EntityType.PLACE)

patches = {**person_patchset.patches, **place_patchset.patches}
patchset = Patchset.from_patches(patches)

from ahisto_named_entity_search.entity import PatchsetConfirmatory

PatchsetConfirmatory.create_confirmatory_template(
    patchset, 'patchset-confirmatory-template.xlsx', all_entities, all_entities)
