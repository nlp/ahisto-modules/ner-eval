import os
import sys

from ahisto_named_entity_search.recognition import NerModel, get_schedule


if __name__ == '__main__':
    assert len(sys.argv) == 7

    model_generation = sys.argv[1]
    search_method = sys.argv[2]
    cross_page_boundaries = sys.argv[3]
    only_relevant = sys.argv[4]
    schedule_name = sys.argv[5]
    git_commit = sys.argv[6]

    os.environ['COMET_PROJECT_NAME'] = f'ahisto-ner_{model_generation}'

    model_basename = f'model_ner_{search_method}_{cross_page_boundaries}_{only_relevant}_{schedule_name}_{model_generation}'
    model_checkpoint_basename = f'{model_basename}_checkpoints.{git_commit}'

    sentence_basename = f'dataset_mlm_{cross_page_boundaries}_{only_relevant}'
    training_sentence_basename = f'{sentence_basename}_training'
    validation_sentence_basename = f'{sentence_basename}_validation'

    tagged_sentence_basename = f'dataset_ner_{search_method}_{cross_page_boundaries}_{only_relevant}'
    training_tagged_sentence_basename = f'{tagged_sentence_basename}_training_automatically_tagged'
    validation_tagged_sentence_basename = f'{tagged_sentence_basename}_validation_automatically_tagged'

    try:
        ner_model = NerModel.load(model_basename)
        with ner_model.model as model:  # Try actually loading the NER model
            pass
    except EnvironmentError:
        NerModel.train_and_save(model_checkpoint_basename, model_basename,
                                training_sentence_basename, validation_sentence_basename,
                                training_tagged_sentence_basename,
                                validation_tagged_sentence_basename,
                                schedule_name=schedule_name)
