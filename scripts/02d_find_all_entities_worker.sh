#!/bin/bash
set -e

# Activate a virtualenv.
VIRTUALENV=venv-$(hostname)
source $VIRTUALENV/bin/activate

python -V | grep -q 'Python 3\.[78]'
pip install --quiet --exists-action i .

# Compute the results.
LC_ALL=C nice -n 19 python scripts/02d_find_all_entities_worker.py "$@"
